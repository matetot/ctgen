package transformation;

import configuration.Parameter;
import element.*;
import element.variable.PrimitiveInitializationElement;
import element.variable.ObjectInitializationElement;
import general.CStringUtils;
import general.DataTypes;
import general.ParameterUtils;
import input.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of abstract transformer {@link AbstractTransformer}.
 * This class overrides abstract methods from its parent
 * and builds final form using {@link element} objects.
 *
 * @author Tomáš Matějka
 */
public class JavaTransformer extends AbstractTransformer {

    private List<RootElement> classElements = new ArrayList<>();

    private TestClass activeTestClass;

    private TestMethod activeTestMethod;

    private ClassElement activeClassElement;

    private MethodElement activeMethodElement;

    private MethodCallElement activeAssertEqualsElement;

    private List<String> declaredVariables;

    private List<String> alreadyImported;

    private int testSuiteId;

    @Override
    protected List<RootElement> getRoots() {
        return classElements;
    }

    @Override
    protected void enteredTestBlock() {
        activeMethodElement = new MethodElement(activeTestMethod.getName() + "TestCase" + testSuiteId);
        activeClassElement.addMethodElement(activeMethodElement);

        activeMethodElement.addAnnotation(new AnnotationElement("Test"));

        String calledObject = "";
        if (activeTestMethod.isStaticMethod()) {
            calledObject = activeTestClass.getClassName();
        } else {
            calledObject = CStringUtils.createClassName(activeTestClass.getClassName());

            activeMethodElement.addBodyElement(new ObjectInitializationElement(
                    CStringUtils.createClassName(activeTestClass.getClassName()), activeTestClass.getClassName()));
            activeMethodElement.addBodyElement(new NewLineElement());
            activeMethodElement.addBodyElement(new NewLineElement());
        }
        activeAssertEqualsElement =
                new MethodCallElement(calledObject, activeTestMethod.getName());
    }

    @Override
    protected void leftTestBlock() {
        testSuiteId += 1;
        activeMethodElement.addBodyElement(activeAssertEqualsElement);
        activeMethodElement.addBodyElement(new NewLineElement());
        activeMethodElement.addBodyElement(new NewLineElement());
    }

    @Override
    protected void enteredPrimitiveParameter(TestPrimitive testPrimitive, List<Parameter> atlboParameters, Integer[] testValues) {
        activeAssertEqualsElement.addParameter(testPrimitive.getName());

        String value = getValueFor(testPrimitive.getName(), testPrimitive, atlboParameters, testValues);
        activeMethodElement.addBodyElement(new PrimitiveInitializationElement(testPrimitive.getName(), testPrimitive.getDataType(), value));
    }

    @Override
    protected void enteredObjectParameter(TestObject testObject) {
        addImport(testObject.getDataType());

        activeAssertEqualsElement.addParameter(testObject.getName());
        activeMethodElement.addBodyElement(new ObjectInitializationElement(testObject.getName(), testObject.getObjectType()));
    }

    @Override
    protected void enteredEnumParameter(TestEnum testEnum, List<Parameter> atlboParameters, Integer[] testValues) {
        activeAssertEqualsElement.addParameter(testEnum.getName());

        addImport(testEnum.getDataType());

        String value = getValueFor(testEnum.getName(), testEnum, atlboParameters, testValues);
        activeMethodElement.addBodyElement(new PrimitiveInitializationElement(testEnum.getName(), testEnum.getDataType(), value));
    }

    @Override
    protected void enteredPrimitiveAttribute(String currentPath, TestObject parentObject, TestPrimitive testPrimitive,
                                              List<Parameter> atlboParameters, Integer[] testValues) {
        String fullIdentifier = CStringUtils.append(currentPath, testPrimitive.getName());

        activeMethodElement.addBodyElement(new SetterElement(
                parentObject.getName(),
                CStringUtils.createSetter(testPrimitive.getName()),
                getValueFor(fullIdentifier, testPrimitive, atlboParameters, testValues)));
    }

    @Override
    protected void enteredEnumAttribute(String currentPath, TestObject parentObject, TestEnum testEnum, List<Parameter> atlboParameters, Integer[] testValues) {
        String fullIdentifier = CStringUtils.append(currentPath, testEnum.getName());

        addImport(testEnum.getDataType());
        activeMethodElement.addBodyElement(new SetterElement(
                parentObject.getName(),
                CStringUtils.createSetter(testEnum.getName()),
                getValueFor(fullIdentifier, testEnum, atlboParameters, testValues)));
    }

    @Override
    protected void enteredObjectAttributte(String currentPath, TestObject parentObject, TestObject testObject) {
        addImport(testObject.getDataType());

        activeMethodElement.addBodyElement(new ObjectInitializationElement(testObject.getName(), testObject.getObjectType()));
    }

    @Override
    protected void leftObjectAttributte(TestObject parentObject, TestObject testObject) {
        activeMethodElement.addBodyElement(new SetterElement(
                parentObject.getName(),
                CStringUtils.createSetter(testObject.getName()),
                testObject.getName()
        ));
    }

    @Override
    protected void enteredMethod(TestMethod testMethod) {
        testSuiteId = 1;
        activeTestMethod = testMethod;
    }

    @Override
    protected void enteredClass(TestClass testClass) {
        declaredVariables = new ArrayList<>();
        alreadyImported = new ArrayList<>();
        activeTestClass = testClass;
        activeClassElement = new ClassElement(testClass.getClassName() + "Test");
        addImport(testClass.getPackageName() + "." + testClass.getClassName());
        addImport("org.junit.jupiter.api.Test");
        classElements.add(activeClassElement);

    }

    private String getValueFor(String identifier, TestPrimitive testPrimitive, List<Parameter> atlboParmeters, Integer[] testValues) {
        Parameter atlboParameter = ParameterUtils.find(atlboParmeters, identifier);
        int valueIndex = testValues[atlboParmeters.indexOf(atlboParameter)];
        String decorator = testPrimitive.getDataType().equals(DataTypes.STRING_FULL)
                || testPrimitive.getDataType().equals(DataTypes.STRING_SHORT) ? "\"" : "";

        return decorator + testPrimitive.getValues().get(valueIndex) + decorator;
    }

    private String getValueFor(String identifier, TestEnum testEnum, List<Parameter> atlboParmeters, Integer[] testValues) {
        Parameter atlboParameter = ParameterUtils.find(atlboParmeters, identifier);
        int valueIndex = testValues[atlboParmeters.indexOf(atlboParameter)];

        return testEnum.getValues().get(valueIndex);
    }

    private boolean checkIfDeclared(String name) {
        return false;
//        if (!declaredVariables.contains(name)) {
//            declaredVariables.add(name);
//            return false;
//        }
//        return true;
    }

    private void addImport(String toImport) {
        if (!alreadyImported.contains(toImport)) {
            alreadyImported.add(toImport);
            this.activeClassElement.addImport(new ImportElement(toImport));
        }
    }

}
