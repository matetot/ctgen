package transformation;

import atlbo.Atlbo;
import configuration.MainConfiguration;
import configuration.Parameter;
import element.RootElement;
import general.CStringUtils;
import general.ParameterType;
import input.model.*;
import general.ParameterUtils;

import java.util.List;

/**
 * Abstract class implementing {@link Transformer} interface, which allows to transform input model into output form.
 * This class handles the walk through input model (its classes, methods, parameters etc.).
 * That means this class does not create concrete output forms, this responsibility is given to extending classes
 * through calling of abstract methods that must be overridden in extending class.
 *
 * @author Tomáš Matějka
 */
public abstract class AbstractTransformer implements Transformer {

    @Override
    public List<RootElement> transform(List<TestClass> testClasses, Atlbo atlbo, int populationSize, int atlboSuiteLimit) {
        for (TestClass testClass : testClasses) {
            enteredClass(testClass);
            for (TestMethod testMethod : testClass.getMethods()) {
                int t = testMethod.getInteractionStrength();
                if (t < 2 || t > testMethod.getParametersCount()) {
                    t = 2;
                }

                List<Parameter> atlboParameters = ParameterUtils.mapToAtlboParameters(testMethod.getParameters());
                MainConfiguration mainConfiguration = new MainConfiguration(t);
                mainConfiguration.addParameters(atlboParameters);
                List<Integer[]> atlboResults = atlbo.generate(mainConfiguration, populationSize, atlboSuiteLimit).getFinalTestSuite();

                enteredMethod(testMethod);

                for (Integer[] atlboResult : atlboResults) {
                    enteredTestBlock();
                    visitMethodBody(testClass, testMethod, testMethod.getParameters(), atlboParameters, atlboResult);
                    leftTestBlock();
                }
            }
        }

        return getRoots();
    }

    private void visitMethodBody(TestClass testClass, TestMethod testMethod, List<TestParameter> testParameters, List<Parameter> atlboParameters, Integer[] testValues) {
        for (TestParameter testParameter : testParameters) {
            if (testParameter.parameterType() == ParameterType.OBJECT) {
                TestObject testObject = (TestObject) testParameter;
                enteredObjectParameter(testObject);
                visitObject(testObject.getName(), testObject, atlboParameters, testValues);
            } else if (testParameter.parameterType() == ParameterType.PRIMITIVE) {
                enteredPrimitiveParameter((TestPrimitive) testParameter, atlboParameters, testValues);
            } else if (testParameter.parameterType() == ParameterType.ENUM) {
                enteredEnumParameter((TestEnum) testParameter, atlboParameters, testValues);
            }
        }
    }

    private void visitObject(String currentPath, TestObject parentObject, List<Parameter> atlboParameters, Integer[] testValues) {
        for (TestParameter attribute : parentObject.getAttributes()) {
            if (attribute.parameterType() == ParameterType.OBJECT) {
                TestObject testObject = (TestObject) attribute;

                enteredObjectAttributte(currentPath, parentObject, testObject);

                visitObject(CStringUtils.append(currentPath, attribute.getName()), testObject, atlboParameters, testValues);

                leftObjectAttributte(parentObject, testObject);
            } else if (attribute.parameterType() == ParameterType.PRIMITIVE) {
                enteredPrimitiveAttribute(currentPath, parentObject, (TestPrimitive) attribute, atlboParameters, testValues);
            } else if (attribute.parameterType() == ParameterType.ENUM) {
                enteredEnumAttribute(currentPath, parentObject, (TestEnum) attribute, atlboParameters, testValues);
            }
        }
    }

    /**
     * Call to extending class to get output element roots created during transformation.
     *
     * @return list of created output element roots
     */
    protected abstract List<RootElement> getRoots();

    /**
     * Call to extending class indicating that test block has been entered.
     * Test block is part of a method and generally contains preparation of variables
     * and some method call be it 'assertEquals' or another non-junit method.
     */
    protected abstract void enteredTestBlock();

    /**
     * Call to extending class indicating that test block has been left.
     */
    protected abstract void leftTestBlock();

    /**
     * Call to extending class indicating that primitive parameter of test method has been entered.
     *
     * @param testPrimitive processed primitive parameter
     * @param atlboParameters ATLBO parameters that were dug from input model
     * @param testValues values of currently processed test case
     */
    protected abstract void enteredPrimitiveParameter(TestPrimitive testPrimitive, List<Parameter> atlboParameters, Integer[] testValues);

    /**
     * Call to extending class indicating that object parameter of test method has been entered.
     *
     * @param testObject processed object parameters
     */
    protected abstract void enteredObjectParameter(TestObject testObject);

    protected abstract void enteredEnumParameter(TestEnum testEnum, List<Parameter> atlboParameters, Integer[] testValues);

    /**
     * Call to extending class indicating that primitive attribute of an object has been entered.
     *
     * @param currentPath current identifier of the object path e.g. (person.legs)
     * @param parentObject parent object - who owns processed primitive attribute
     * @param testPrimitive processed primitive attribute
     * @param atlboParameters ATLBO parameters that were dug from input model
     * @param testValues values of currently processed test case
     */
    protected abstract void enteredPrimitiveAttribute(String currentPath, TestObject parentObject, TestPrimitive testPrimitive,
                                                       List<Parameter> atlboParameters, Integer[] testValues);

    protected abstract void enteredEnumAttribute(String currentPath, TestObject parentObject, TestEnum testEnum,
                                                 List<Parameter> atlboParameters, Integer[] testValues);

    /**
     * Call to extending class indicating that object attribute of an object has been entered.
     *
     * @param currentPath current identifier of the object path e.g. (person.legs)
     * @param parentObject parent object - who owns processed object attribute
     * @param testObject processed object attribute
     */
    protected abstract void enteredObjectAttributte(String currentPath, TestObject parentObject, TestObject testObject);

    /**
     * Call to extending class indicating that objectAttribute of an object has been left.
     *
     * @param parentObject parent object - who owns processed object attribute
     * @param testObject processed object attribute
     */
    protected abstract void leftObjectAttributte(TestObject parentObject, TestObject testObject);

    /**
     * Call to extending class indicating that test method has been entered.
     *
     * @param testMethod processed test method
     */
    protected abstract void enteredMethod(TestMethod testMethod);

    /**
     * Call to extending class indicating that test class has been entered.
     *
     * @param testClass processed test class
     */
    protected abstract void enteredClass(TestClass testClass);

}
