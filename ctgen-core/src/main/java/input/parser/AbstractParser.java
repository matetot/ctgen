package input.parser;

import input.model.*;

import java.util.List;

/**
 * Abstract class for parsers of CTGen input files.
 * Input file is parsed and input model containing test classes, test methods and test parameters is created.
 *
 * @author Tomáš Matějka
 */
public abstract class AbstractParser {

    /**
     * Loads file with given name, parses it into an input model and return that model.
     *
     * @param file input file name
     * @return created model
     */
    public abstract List<TestClass> parse(String file);


}
