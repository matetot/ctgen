package input.parser;

import input.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of abstract parser {@link AbstractParser} for 'xml' file type.
 *
 * @author Tomáš Matějka
 */
public class XmlParser extends AbstractParser {

    @Override
    public List<TestClass> parse(String file) {
        List<TestClass> parsedClasses = new ArrayList<>();

        try {
            InputStream xmlInput = new FileInputStream(file);
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlInput);
            doc.getDocumentElement().normalize();

            Element classes = (Element) doc.getElementsByTagName("classes").item(0);
            NodeList classesList = classes.getElementsByTagName("class");
            for (int i = 0; i < classesList.getLength(); i++) {
                Element clazz = (Element) classesList.item(i);
                TestClass testClass = parseClass(clazz);
                parsedClasses.add(testClass);

//                System.out.println(testClass);
            }

            return parsedClasses;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return parsedClasses;
    }

    private TestClass parseClass(Element clazz) {
        TestClass.Builder builder = TestClass.builder();

        String packageName = clazz.getElementsByTagName("packageName").item(0).getTextContent();
        String className = clazz.getElementsByTagName("className").item(0).getTextContent();

        Element methods = (Element) clazz.getElementsByTagName("methods").item(0);
        NodeList methodsList = methods.getElementsByTagName("method");

        for (int i = 0; i < methodsList.getLength(); i++) {
            Element method = (Element) methodsList.item(i);
            TestMethod testMethod = parseMethod(method);
            builder.addMethod(testMethod);
        }

        return builder.
                packageName(packageName).
                className(className).
                build();
    }

    private TestMethod parseMethod(Element method) {
        TestMethod.Builder builder = TestMethod.builder();

        String name = getFirstDirectChildByTag(method, "name").getTextContent();
        int parameterCount = Integer.valueOf(getFirstDirectChildByTag(method, "parameterCount").getTextContent());
        String returnType = getFirstDirectChildByTag(method, "returnType").getTextContent();
        int interactionStrength = Integer.valueOf(getFirstDirectChildByTag(method, "interactionStrength").getTextContent());
        boolean isStatic = Boolean.valueOf(getFirstDirectChildByTag(method, "isStatic").getTextContent());

        Element methodParameters = (Element) method.getElementsByTagName("methodParameters").item(0);
        NodeList methodParametersList = methodParameters.getElementsByTagName("methodParameter");
        for (int i = 0; i < methodParametersList.getLength(); i++) {
            Element methodParameter = (Element) methodParametersList.item(i);
            TestParameter testParameter = parseMethodParameter(methodParameter);
            builder.addParameter(testParameter);
        }

        return builder.
                name(name).
                parametersCount(parameterCount).
                returnType(returnType).
                interactionStrength(interactionStrength).
                staticMethod(isStatic).
                build();
    }

    private TestParameter parseMethodParameter(Element methodParameter) {
        TestParameter testParameter;

        boolean primitive = Boolean.valueOf(getFirstDirectChildByTag(methodParameter, "primitive").getTextContent());
        if (primitive) {
            testParameter = parsePrimitive(methodParameter);
        } else {
            testParameter = parseObject(methodParameter);
        }

        return testParameter;
    }

    private TestEnum parseEnum(Element enumNode) {
        String name = getFirstDirectChildByTag(enumNode, "name").getTextContent();
        String dataType = getFirstDirectChildByTag(enumNode, "dataType").getTextContent();
        int index = Integer.valueOf(getFirstDirectChildByTag(enumNode, "index").getTextContent());

        TestEnum.Builder builder = TestEnum.builder().
                name(name).
                dataType(dataType).
                index(index);

        Element values = getFirstDirectChildByTag(enumNode, "enums");
        List<Element> valuesList = getDirectChildsByTag(values, "enum");

        for (int i = 0; i < valuesList.size(); i++) {
            String value = valuesList.get(i).getTextContent();
            builder.addValue(value);
        }

        return builder.build();
    }

    private TestPrimitive parsePrimitive(Element primitive) {
        String name = getFirstDirectChildByTag(primitive, "name").getTextContent();
        String dataType = getFirstDirectChildByTag(primitive, "dataType").getTextContent();
        int index = Integer.valueOf(getFirstDirectChildByTag(primitive, "index").getTextContent());

        TestPrimitive.Builder builder = TestPrimitive.builder().
                name(name).
                dataType(dataType).
                index(index);

        Element values = getFirstDirectChildByTag(primitive, "values");
        List<Element> valuesList = getDirectChildsByTag(values, "value");

        for (int i = 0; i < valuesList.size(); i++) {
            String value = valuesList.get(i).getTextContent();
            builder.addValue(value);
        }

        return builder.build();
    }

    private TestParameter parseObject(Element object) {
        boolean isEnum = Boolean.valueOf(getFirstDirectChildByTag(object, "isEnum").getTextContent());
        if (isEnum) {
            return parseEnum(object);
        }

        String name = getFirstDirectChildByTag(object, "name").getTextContent();
        String dataType = getFirstDirectChildByTag(object, "dataType").getTextContent();
        int index = Integer.valueOf(getFirstDirectChildByTag(object, "index").getTextContent());
        boolean isAbstract = Boolean.valueOf(getFirstDirectChildByTag(object, "isAbstract").getTextContent());
        boolean isInterface = Boolean.valueOf(getFirstDirectChildByTag(object, "isInterface").getTextContent());

        TestObject.Builder builder = TestObject.builder().
                name(name).
                dataType(dataType).
                index(index).
                isAbstract(isAbstract).
                isInterface(isInterface);

        // objects
        Element objects = getFirstDirectChildByTag(object, "objects");
        List<Element> objectList = getDirectChildsByTag(objects, "object");
        for (int i = 0; i < objectList.size(); i++) {
            TestParameter testParameter = parseObject(objectList.get(i));
            builder.addAttribute(testParameter);
        }

        // primitives
        Element primitiveFields = getFirstDirectChildByTag(object, "primitiveFields");
        List<Element> primitiveFieldList = getDirectChildsByTag(primitiveFields, "primitiveField");

        for (int i = 0; i < primitiveFieldList.size(); i++) {
            TestParameter testParameter = parsePrimitive(primitiveFieldList.get(i));
            builder.addAttribute(testParameter);
        }

        return builder.build();
    }

    private Element getFirstDirectChildByTag(Element el, String sTagName) {
        return getDirectChildsByTag(el, sTagName).get(0);
    }

    private List<Element> getDirectChildsByTag(Element el, String sTagName) {
        NodeList allChilds = el.getElementsByTagName(sTagName);
        List<Element> res = new ArrayList<>();

        for (int i = 0; i < allChilds.getLength(); i++) {
            if (allChilds.item(i).getParentNode().equals(el))
                res.add((Element) allChilds.item(i));
        }

        return res;
    }

}
