package input.parser;

import input.model.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of abstract parser {@link AbstractParser} for 'json' file type.
 *
 * @author Tomáš Matějka
 */
public class JsonParser extends AbstractParser {

    @Override
    public List<TestClass> parse(String file) {
        JSONParser jsonParser = new JSONParser();
        List<TestClass> parsedClasses = new ArrayList<>();

        try (FileReader reader = new FileReader(file))
        {
            JSONObject root = (JSONObject) jsonParser.parse(reader);
            JSONArray classes = (JSONArray) root.get("classes");

            for (Object clazz : classes) {
                TestClass testClass = parseClass((JSONObject) clazz);
                parsedClasses.add(testClass);
            }

            return parsedClasses;
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        return parsedClasses;
    }

    private TestClass parseClass(JSONObject clazz) {
        TestClass.Builder builder = TestClass.builder();

        String packageName = (String) clazz.get("packageName");
        String className = (String) clazz.get("className");

        JSONArray methods = (JSONArray) clazz.get("methods");
        for (Object method : methods) {
            TestMethod testMethod = parseMethod((JSONObject) method);
            builder.addMethod(testMethod);
        }

        return builder.
                packageName(packageName).
                className(className).
                build();
    }

    private TestMethod parseMethod(JSONObject method) {
        TestMethod.Builder builder = TestMethod.builder();

        String name = (String) method.get("name");
        int parameterCount = ((Long) method.get("parameterCount")).intValue();
        String returnType = (String) method.get("returnType");
        int interactionStrength = ((Long) method.get("interactionStrength")).intValue();
        boolean isStatic = (boolean) method.get("static");

        JSONArray methodParameters = (JSONArray) method.get("methodParameter");
        for (Object methodParameter : methodParameters) {
            TestParameter testParameter = parseMethodParameter((JSONObject) methodParameter);
            builder.addParameter(testParameter);
        }

        return builder.
                name(name).
                parametersCount(parameterCount).
                returnType(returnType).
                interactionStrength(interactionStrength).
                staticMethod(isStatic).
                build();
    }

    private TestParameter parseMethodParameter(JSONObject methodParameter) {
        TestParameter testParameter;

        boolean primitive = (boolean) methodParameter.get("primitive");
        if (primitive) {
            testParameter = parsePrimitive(methodParameter);
        } else {
            testParameter = parseObject(methodParameter);
        }

        return testParameter;
    }

    private TestEnum parseEnum(JSONObject enumNode) {
        String name = (String) enumNode.get("name");
        String dataType = (String) enumNode.get("dataType");
        int index = ((Long) enumNode.get("index")).intValue();

        TestEnum.Builder builder = TestEnum.builder().
                name(name).
                dataType(dataType).
                index(index);

        JSONArray values = (JSONArray) enumNode.get("possibleEnums");
        for (Object value : values) {
            builder.addValue((String) value);
        }

        return builder.build();
    }

    private TestPrimitive parsePrimitive(JSONObject primitive) {
        String name = (String) primitive.get("name");
        String dataType = (String) primitive.get("dataType");
        int index = ((Long) primitive.get("index")).intValue();

        TestPrimitive.Builder builder = TestPrimitive.builder().
                name(name).
                dataType(dataType).
                index(index);

        JSONArray values = (JSONArray) primitive.get("values");
        for (Object value : values) {
            builder.addValue((String) value);
        }

        return builder.build();
    }

    private TestParameter parseObject(JSONObject object) {
        boolean isEnum = (boolean) object.get("enum");
        if (isEnum) {
            return parseEnum(object);
        }

        String name = (String) object.get("name");
        String dataType = (String) object.get("dataType");
        int index = ((Long) object.get("index")).intValue();
        boolean isAbstract = (boolean) object.get("abstract");
        boolean isInterface = (boolean) object.get("interface");

        TestObject.Builder builder = TestObject.builder().
                name(name).
                dataType(dataType).
                index(index).
                isAbstract(isAbstract).
                isInterface(isInterface);

        // objects
        JSONArray objectAttributes = (JSONArray) object.get("objects");
        for (Object objectAttribute : objectAttributes) {
            TestParameter testParameter = parseObject((JSONObject) objectAttribute);
            builder.addAttribute(testParameter);
        }

        // primitives
        JSONArray primitiveAttributes = (JSONArray) object.get("primitiveFields");
        for (Object primitiveAttribute : primitiveAttributes) {
            TestParameter testParameter = parsePrimitive((JSONObject) primitiveAttribute);
            builder.addAttribute(testParameter);
        }

        return builder.build();
    }

}
