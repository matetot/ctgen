package input.model;

import general.ParameterType;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent object test parameter of test method.
 *
 * @author Tomáš Matějka
 */
public class TestObject extends TestParameter {

    /** Is parameter an abstract class. */
    private boolean isAbstract;

    /** Is parameter an interface. */
    private boolean isInterface;

    /** List of object´s attributes. */
    private List<TestParameter> attributes;

    /**
     * Private constructor.
     * Creates new instance for given values.
     *
     * @param name name of the parameter
     * @param dataType parameter data type
     * @param index parameter index in its test method
     * @param isAbstract is parameter an abstract class
     * @param isInterface is parameter an interface
     * @param attributes list of object´s attributes
     */
    private TestObject(String name, String dataType, int index,
                       boolean isAbstract, boolean isInterface,
                       List<TestParameter> attributes) {
        super(name, dataType, index);
        this.isAbstract = isAbstract;
        this.isInterface = isInterface;
        this.attributes = attributes;
    }

    public String getObjectType() {
        return dataType.substring(dataType.lastIndexOf(".") + 1);
    }

    @Override
    public ParameterType parameterType() {
        return ParameterType.OBJECT;
    }

    public List<TestParameter> getAttributes() {
        return attributes;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public boolean isInterface() {
        return isInterface;
    }

    @Override
    public String toString() {
        return "TestObject{" +
                "attributes=" + attributes +
                ", name='" + name + '\'' +
                ", dataType='" + dataType + '\'' +
                ", index=" + index +
                ", isAbstract=" + isAbstract +
                ", isInterface=" + isInterface +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String name;

        private String dataType;

        private int index;

        private boolean isAbstract;

        private boolean isInterface;

        private List<TestParameter> attributes = new ArrayList<>();

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder dataType(String dataType) {
            this.dataType = dataType;
            return this;
        }

        public Builder index(int index) {
            this.index = index;
            return this;
        }

        public Builder isAbstract(boolean isAbstract) {
            this.isAbstract = isAbstract;
            return this;
        }

        public Builder isInterface(boolean isInterface) {
            this.isInterface = isInterface;
            return this;
        }

        public Builder addAttribute(TestParameter attribute) {
            this.attributes.add(attribute);
            return this;
        }

        public TestObject build() {
            return new TestObject(name, dataType, index, isAbstract, isInterface, attributes);
        }

    }

}
