package input.model;

import general.ParameterType;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents enum parameter of the test method.
 *
 * @author Tomáš Matějka
 */
public class TestEnum extends TestParameter {

    /**
     * Concrete values of the parameter.
     */
    private List<String> values;

    /**
     * Constructor.
     * Creates new instance for given values.
     *
     * @param name name of the parameter
     * @param dataType parameter data type
     * @param index parameter index in its test method
     * @param values concrete values of the parameter
     */
    protected TestEnum(String name, String dataType, int index, List<String> values) {
        super(name, dataType, index);
        this.values = values;
    }

    public String dataTypeWithoutPackage() {
        return dataType.substring(dataType.lastIndexOf(".") + 1);
    }

    public List<String> getValues() {
        return values;
    }

    @Override
    public ParameterType parameterType() {
        return ParameterType.ENUM;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String name;

        private String dataType;

        private int index;

        private List<String> values = new ArrayList<>();

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder dataType(String dataType) {
            this.dataType = dataType;
            return this;
        }

        public Builder index(int index) {
            this.index = index;
            return this;
        }

        public Builder addValue(String value) {
            this.values.add(value.substring(dataType.lastIndexOf(".") + 1));
            return this;
        }

        public TestEnum build() {
            return new TestEnum(name, dataType, index, values);
        }
    }
}