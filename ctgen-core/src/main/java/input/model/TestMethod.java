package input.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents test method to be tested.
 *
 * @author Tomáš Matějka
 */
public class TestMethod {

    /** Method name. */
    private String name;

    /** Method parameters count. */
    private int parametersCount;

    /** Data type test method returns. */
    private String returnType;

    /** Is method static or not. */
    private boolean staticMethod;

    private int interactionStrength;

    /** List of parameters this method has. */
    private List<TestParameter> parameters;

    /**
     * Private constructor.
     * Creates new instance for given values.
     *
     * @param name name of the method
     * @param parametersCount parameters count
     * @param returnType returned data type
     * @param staticMethod is method static
     * @param parameters list of parameters this method has
     */
    private TestMethod(String name, int parametersCount, String returnType,
                       int interactionStrength, boolean staticMethod,
                       List<TestParameter> parameters) {
        this.name = name;
        this.parametersCount = parametersCount;
        this.returnType = returnType;
        this.interactionStrength = interactionStrength;
        this.staticMethod = staticMethod;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public int getParametersCount() {
        return parametersCount;
    }

    public List<TestParameter> getParameters() {
        return parameters;
    }

    public String getReturnType() {
        return returnType;
    }

    public boolean isStaticMethod() {
        return staticMethod;
    }

    public int getInteractionStrength() {
        return interactionStrength;
    }

    @Override
    public String toString() {
        return "TestMethod{" +
                "name='" + name + '\'' +
                ", parametersCount=" + parametersCount +
                ", returnType='" + returnType + '\'' +
                ", interactionStrength=" + interactionStrength +
                ", staticMethod=" + staticMethod +
                ", parameters=" + parameters +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String name;

        private int parametersCount;

        private String returnType;

        private int interactionStrength;

        private boolean staticMethod;

        private List<TestParameter> parameters = new ArrayList<>();

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder parametersCount(int parametersCount) {
            this.parametersCount = parametersCount;
            return this;
        }

        public Builder returnType(String returnType) {
            this.returnType = returnType;
            return this;
        }

        public Builder staticMethod(boolean staticMethod) {
            this.staticMethod = staticMethod;
            return this;
        }

        public Builder interactionStrength(int interactionStrength) {
            this.interactionStrength = interactionStrength;
            return this;
        }

        public Builder addParameter(TestParameter parameter) {
            this.parameters.add(parameter);
            return this;
        }

        public TestMethod build() {
            return new TestMethod(name, parametersCount, returnType,
                    interactionStrength, staticMethod, parameters);
        }

    }

}
