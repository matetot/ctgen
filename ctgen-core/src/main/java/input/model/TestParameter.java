package input.model;

import general.ParameterType;

/**
 * Represent test parameter of test method.
 *
 * @author Tomáš Matějka
 */
public abstract class TestParameter {

    /** Parameter name. */
    protected String name;

    /** Parameter data type. */
    protected String dataType;

    /** Parameter index in its test method. */
    protected int index;

    /**
     * Constructor.
     * Creates new instance for given values.
     *
     * @param name name of the parameter
     * @param dataType parameter data type
     * @param index parameter index in its test method
     */
    protected TestParameter(String name, String dataType, int index) {
        this.name = name;
        this.dataType = dataType;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public String getDataType() {
        return dataType;
    }

    public int getIndex() {
        return index;
    }

    public abstract ParameterType parameterType();

    @Override
    public String toString() {
        return "TestParameter{" +
                "name='" + name + '\'' +
                ", dataType='" + dataType + '\'' +
                ", index=" + index +
                '}';
    }

}
