package input.model;

import general.ParameterType;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent primitive test parameter of test method.
 *
 * @author Tomáš Matějka
 */
public class TestPrimitive extends TestParameter {

    /** Concrete values of the parameter. */
    private List<String> values;

    /**
     * Private constructor.
     * Creates new instance for given values.
     *
     * @param name name of the parameter
     * @param dataType parameter data type
     * @param index parameter index in its test method
     * @param values concrete values of the parameter
     */
    protected TestPrimitive(String name, String dataType, int index, List<String> values) {
        super(name, dataType, index);
        this.values = values;
    }

    @Override
    public ParameterType parameterType() {
        return ParameterType.PRIMITIVE;
    }

    public List<String> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "TestPrimitive{" +
                "values=" + values +
                ", name='" + name + '\'' +
                ", dataType='" + dataType + '\'' +
                ", index=" + index +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String name;

        private String dataType;

        private int index;

        private List<String> values = new ArrayList<>();

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder dataType(String dataType) {
            this.dataType = dataType;
            return this;
        }

        public Builder index(int index) {
            this.index = index;
            return this;
        }

        public Builder addValue(String value) {
            this.values.add(value);
            return this;
        }

        public TestPrimitive build() {
            return new TestPrimitive(name, dataType, index, values);
        }
    }

}
