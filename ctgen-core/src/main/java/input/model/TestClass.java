package input.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents class that contains methods to be tested.
 *
 * @author Tomáš Matějka
 */
public class TestClass {

    /** Class name */
    private String className;

    /** Package containing the class. */
    private String packageName;

    /** List of methods this class has. */
    private List<TestMethod> methods;

    /**
     * Private constructor.
     * Creates new instance for given values.
     *
     * @param className name of the class
     * @param packageName name of the package containing the class
     * @param methods list of methods this class has
     */
    private TestClass(String className, String packageName, List<TestMethod> methods) {
        this.className = className;
        this.packageName = packageName;
        this.methods = methods;
    }

    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }

    public List<TestMethod> getMethods() {
        return methods;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "TestClass{" +
                "className='" + className + '\'' +
                ", packageName='" + packageName + '\'' +
                ", methods=" + methods +
                '}';
    }

    public static class Builder {

        private String className;

        private String packageName;

        private List<TestMethod> methods = new ArrayList<>();

        public Builder className(String className) {
            this.className = className;
            return this;
        }

        public Builder packageName(String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder addMethod(TestMethod method) {
            this.methods.add(method);
            return this;
        }

        public TestClass build() {
            return new TestClass(className, packageName, methods);
        }

    }

}
