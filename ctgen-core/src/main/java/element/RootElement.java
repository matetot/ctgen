package element;

/**
 * Interface for root element of output model created by {@link transformation.Transformer}.
 * Root element must return its name because this name is used when saving the model to file.
 * This interface also implements interface {@link Element}.
 *
 * @author Tomáš Matějka
 */
public interface RootElement extends Element {

    /**
     * Get name of the output model this element is root of.
     * @return output model name
     */
    String getName();

}
