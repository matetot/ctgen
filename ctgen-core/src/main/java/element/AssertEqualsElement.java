package element;

import java.util.ArrayList;
import java.util.List;

public class AssertEqualsElement implements Element {

    private String calledObject;

    private String calledMethod;

    public List<String> parameters = new ArrayList<>();

    public AssertEqualsElement(String calledObject, String calledMethod) {
        this.calledObject = calledObject;
        this.calledMethod = calledMethod;
    }

    public void addParameter(String parameter) {
        this.parameters.add(parameter);
    }

    @Override
    public String write() {
        String out = "assertEquals(expected, " + calledObject + "." + calledMethod + "(";

        for (int i = 0; i < parameters.size(); i++) {
            out += parameters.get(i);
            if (i < parameters.size() - 1) {
                out += ",";
            }
        }
        out += "));";

        return out;
    }

}
