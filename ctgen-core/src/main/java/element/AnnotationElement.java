package element;

/**
 * Element representing single annotation.
 *
 * @author Tomáš Matějka
 */
public class AnnotationElement implements Element {

    /** Name of the annotation. */
    private String name;

    /**
     * Constructor.
     *
     * @param name name of the annotation
     */
    public AnnotationElement(String name) {
        this.name = name;
    }

    @Override
    public String write() {
        return "@" + name;
    }

}
