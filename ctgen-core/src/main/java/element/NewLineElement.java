package element;

/**
 * Element containing just the new line.
 *
 * @author Tomáš Matějka
 */
public class NewLineElement implements Element {

    @Override
    public String write() {
        return System.lineSeparator();
    }

}
