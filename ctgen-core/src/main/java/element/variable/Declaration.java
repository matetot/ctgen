package element.variable;

import element.Element;

/**
 * Element representing declaration of either primitive or object variable.
 *
 * @author Tomáš Matějka
 */
public class Declaration implements Element {

    /** Name of the declared variable. */
    private String name;

    /** Data type of the declared variable. */
    private String dataType;

    /**
     * Constructor.
     *
     * @param name name of the declared variable
     * @param dataType data type of the declared variable
     */
    public Declaration(String name, String dataType) {
        this.name = name;
        this.dataType = dataType;
    }

    @Override
    public String write() {
        return dataType + " " + name + ";";
    }

}
