package element.variable;

import element.Element;

/**
 * Element representing initialization of object variable.
 *
 * @author Tomáš Matějka
 */
public class ObjectInitializationElement implements Element {

    /** Name of the initialized object. */
    private String name;

    /** Data type of the initialized object. */
    private String dataType;

    /**
     * Constructor.
     *
     * @param name name of the initialized object
     * @param dataType data type of the initialized object
     */
    public ObjectInitializationElement(String name, String dataType) {
        this.name = name;
        this.dataType = dataType;
    }

    @Override
    public String write() {
        return dataType + " " + name + " = new " + dataType + "();";
    }
}
