package element.variable;

import element.Element;

/**
 * Element representing initialization of primitive variable.
 *
 * @author Tomáš Matějka
 */
public class PrimitiveInitializationElement implements Element {

    /** Name of the initialized primitive. */
    private String name;

    /** Data type of the initialized primitive. */
    private String dataType;

    /** Value of the initialized primitive. */
    private String value;

    /**
     * Constructor.
     *
     * @param name name of the initialized primitive
     * @param dataType data type of the initialized primitive
     * @param value value of the initialized primitive
     */
    public PrimitiveInitializationElement(String name, String dataType, String value) {
        this.name = name;
        this.dataType = dataType;
        this.value = value;
    }

    @Override
    public String write() {
        return dataType + " " + name + "=" + value + ";";
    }
}
