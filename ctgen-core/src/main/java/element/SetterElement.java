package element;

/**
 * Element representing setter on some object.
 *
 * @author Tomáš Matějka
 */
public class SetterElement implements Element {

    /** Name of the object that "owns" the setter. */
    private String calledObject;

    /** Name of the setter method. */
    private String setterName;

    /** Value passed to the setter. */
    private String value;

    /**
     * Constructor.
     *
     * @param calledObject name of the object that "owns" the setter
     * @param setterName name of the setter method
     * @param value value passed to the setter
     */
    public SetterElement(String calledObject, String setterName, String value) {
        this.calledObject = calledObject;
        this.setterName = setterName;
        this.value = value;
    }

    @Override
    public String write() {
        return calledObject + "." + setterName + "(" + value + ");" ;
    }

}
