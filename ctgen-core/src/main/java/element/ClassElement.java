package element;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of interface {@link RootElement}.
 * Root element of {@link transformation.JavaTransformer}.
 * Represents single output test class. Contains its name, list of imports and list of methods.
 *
 * @author Tomáš Matějka
 */
public class ClassElement implements RootElement {

    /** Name of the output class. */
    private String name;

    /** List of imported classes. */
    private List<ImportElement> imports = new ArrayList<>();

    /** List of tested methods. */
    private List<MethodElement> methods = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param name name of the output class
     */
    public ClassElement(String name) {
        this.name = name;
    }

    /**
     * Adds new import to the {@link #imports} list.
     *
     * @param importElement imported class
     */
    public void addImport(ImportElement importElement) {
        this.imports.add(importElement);
    }

    /**
     * Adds new method to the {@link #methods} list.
     *
     * @param methodElement added method
     */
    public void addMethodElement(MethodElement methodElement) {
        this.methods.add(methodElement);
    }

    @Override
    public String getName() {
        return name;
    }

    public List<MethodElement> getMethods() {
        return methods;
    }

    @Override
    public String write() {
        String out = "";
        for (ImportElement anImport : imports) {
            out += anImport.write();
        }
        out += "public class " + this.name + " {";

        for (MethodElement method : methods) {
            out += method.write();
        }

        out += "}";

        return out;
    }

}
