package element;

import java.util.ArrayList;
import java.util.List;

/**
 * Element representing single output test method.
 * Contains list of its @link{@link #annotations}, list of variable {@link #declarations} and list of other body elements ({@link #bodyElements}).
 *
 * @author Tomáš Matějka
 */
public class MethodElement implements Element {

    /** Name of the output method. */
    private String name;

    /** List of method annotations. */
    private List<AnnotationElement> annotations = new ArrayList<>();

    /** List of declared variables. */
    private List<Element> declarations = new ArrayList<>();

    /** List of any other body elements (variable initializations etc.). */
    private List<Element> bodyElements = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param name name of the output method
     */
    public MethodElement(String name) {
        this.name = name;
    }

    /**
     * Adds new variable declaration.
     *
     * @param element added declaration
     */
    public void addDeclaration(Element element) {
        this.declarations.add(element);
    }

    /**
     * Adds any element to method body.
     *
     * @param bodyElement added element
     */
    public void addBodyElement(Element bodyElement) {
        this.bodyElements.add(bodyElement);
    }

    /**
     * Adds list of elements to method body.
     *
     * @param bodyElements added list of elements
     */
    public void addBodyElements(List<Element> bodyElements) {
        this.bodyElements.addAll(bodyElements);
    }

    /**
     * Adds annotation to hte method.
     *
     * @param annotationElement added annotation
     */
    public void addAnnotation(AnnotationElement annotationElement) {
        this.annotations.add(annotationElement);
    }

    public String getName() {
        return name;
    }

    @Override
    public String write() {
        String out = "";

        for (AnnotationElement annotation : annotations) {
            out += annotation.write() + " ";
        }

        out += "public void " + this.name + "() {";

        for (Element declaration : declarations) {
            out += declaration.write();
        }

        for (Element bodyElement : bodyElements) {
            out += bodyElement.write();
        }

        out += "}";

        return out;
    }
}
