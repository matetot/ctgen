package element;

/**
 * Interface for element which is a smallest part forming the output model (for example used in {@link transformation.JavaTransformer}.
 *
 * @author Tomáš Matějka
 */
public interface Element {

    /**
     * Creates string representation of the element.
     * @return created string
     */
    String write();

}
