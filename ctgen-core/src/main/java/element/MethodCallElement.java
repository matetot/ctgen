package element;

import java.util.ArrayList;
import java.util.List;

/**
 * Element representing method call.
 *
 * @author Tomáš Matějka
 */
public class MethodCallElement implements Element {

    /** Object that "owns" the called method.*/
    private String calledObject;

    /** Name of the called method. */
    private String calledMethod;

    /** Parameter values of the method call. */
    public List<String> parameters = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param calledObject name of the object whose method is being called
     * @param calledMethod name of the called method
     */
    public MethodCallElement(String calledObject, String calledMethod) {
        this.calledObject = calledObject;
        this.calledMethod = calledMethod;
    }

    /**
     * Adds method parameter to @link {@link #parameters} list.
     *
     * @param parameter
     */
    public void addParameter(String parameter) {
        this.parameters.add(parameter);
    }

    @Override
    public String write() {
        String out = calledObject + "." + calledMethod + "(";

        for (int i = 0; i < parameters.size(); i++) {
            out += parameters.get(i);
            if (i < parameters.size() - 1) {
                out += ",";
            }
        }
        out += ");";

        return out;
    }

}
