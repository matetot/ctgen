package element;

/**
 * Element representing single class import.
 *
 * @author Tomáš Matějka
 */
public class ImportElement implements Element {

    /** Name of the imported class. */
    private String toImport;

    /**
     * Constructor.
     *
     * @param toImport name of the imported class
     */
    public ImportElement(String toImport) {
        this.toImport = toImport;
    }

    @Override
    public String write() {
        return "import " + toImport + ";";
    }
}
