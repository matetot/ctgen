package memorycompile;

import javax.tools.*;
import java.io.IOException;
import java.util.Arrays;

/**
 * Tool for compiling class in memory (no .class file is created on file system) and running method from the class.
 *
 * @author Tomáš Matějka
 */
public final class CodeRunner {

    /**
     * Creates class with given name and given source code, compiles it and runs its method for given name.
     *
     * @param className name of compiled class
     * @param methodName method to run
     * @param code code of compiled class
     * @throws Exception any exception that can happen during process
     */
    public static void run(String className, String methodName, String code) throws Exception {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        DiagnosticCollector<JavaFileObject> diagnostics =
                new DiagnosticCollector<>();

        final JavaByteObject byteObject = new JavaByteObject(className);
        StandardJavaFileManager standardFileManager =
                compiler.getStandardFileManager(diagnostics, null, null);

        JavaFileManager fileManager = createFileManager(standardFileManager,
                byteObject);

        JavaCompiler.CompilationTask task = compiler.getTask(null,
                fileManager, diagnostics, null, null, getCompilationUnits(className, code));
        if (!task.call()) {
            diagnostics.getDiagnostics().forEach(System.out::println);
        }
        fileManager.close();

        final ClassLoader inMemoryClassLoader = createClassLoader(byteObject);
        Class test = inMemoryClassLoader.loadClass(className);
        test.getMethod(methodName).invoke(test.newInstance());
    }

    private static JavaFileManager createFileManager(StandardJavaFileManager fileManager,
                                                     JavaByteObject byteObject) {
        return new ForwardingJavaFileManager<StandardJavaFileManager>(fileManager) {
            @Override
            public JavaFileObject getJavaFileForOutput(Location location,
                                                       String className, JavaFileObject.Kind kind,
                                                       FileObject sibling) throws IOException {
                return byteObject;
            }
        };
    }

    private static ClassLoader createClassLoader(final JavaByteObject byteObject) {
        return new ClassLoader() {
            @Override
            public Class<?> findClass(String name) throws ClassNotFoundException {
                byte[] bytes = byteObject.getBytes();
                return defineClass(name, bytes, 0, bytes.length);
            }
        };
    }

    private static Iterable<? extends JavaFileObject> getCompilationUnits(String className, String code) {
        JavaStringObject stringObject =
                new JavaStringObject(className, code);
        return Arrays.asList(stringObject);
    }

}
