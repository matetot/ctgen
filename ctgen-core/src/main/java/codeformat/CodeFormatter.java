package codeformat;

/**
 * Interface for any code formatter, that takes source code and formats it however it wants.
 *
 * @author Tomáš Matějka
 */
public interface CodeFormatter {

    /**
     * Method takes source code and return formatted code.
     *
     * @param code code to be formatted
     * @return formatted code
     * @throws CodeFormattingException any exception related to formatting (e.g. invalid source code)
     */
    String formatCode(String code) throws CodeFormattingException;

}
