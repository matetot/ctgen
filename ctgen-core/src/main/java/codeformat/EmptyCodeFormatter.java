package codeformat;

/**
 * Implementation of {@link CodeFormatter} interface.
 * This implementation does not do any formatting to input source code.
 *
 * @author Tomáš Matějka
 */
public class EmptyCodeFormatter implements CodeFormatter {

    @Override
    public String formatCode(String code) throws CodeFormattingException {
        return code;
    }

}
