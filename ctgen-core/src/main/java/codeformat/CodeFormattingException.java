package codeformat;

/**
 * Common exception for problems encountered during code formatting in any code formatter.
 *
 * @author Tomáš Matějka
 */
public final class CodeFormattingException extends Exception {

    /**
     * Constructor.
     * Creates new instance.
     *
     * @param errorMessage error message saying what happened
     */
    public CodeFormattingException(String errorMessage) {
        super(errorMessage);
    }

}
