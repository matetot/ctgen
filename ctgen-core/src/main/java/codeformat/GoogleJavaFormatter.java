package codeformat;

import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;

/**
 * Implementation of {@link CodeFormatter} interface.
 * This implementation uses <a href="https://github.com/google/google-java-format">google library</a>
 * that reformats Java source code to comply with
 * <a href="https://google.github.io/styleguide/javaguide.html">Google Java Style</a>.
 *
 * @author Tomáš Matějka
 */
public class GoogleJavaFormatter implements CodeFormatter {

    private Formatter formatter = new Formatter();

    @Override
    public String formatCode(String code) throws CodeFormattingException {
        try {
            return formatter.formatSource(code);
        } catch (FormatterException e) {
            throw new CodeFormattingException(e.getMessage());
        }
    }
}
