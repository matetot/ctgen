package ctgen;

import atlbo.Atlbo;

/**
 * Interface used in {@link CTGenBuilder} forcing implementing class to set {@link Atlbo}
 * before being able to move on with building process.
 *
 * @author Tomáš Matějka
 */
public interface AtlboConsumer {

    /**
     * Sets given atlbo and return instance of class implementing {@link TransformerConsumer}
     * which provides option to set {@link transformation.Transformer}.
     *
     * @param atlbo atlbo to be set
     * @return class implementing 'TransformerConsumer'
     */
    TransformerConsumer atlbo(Atlbo atlbo);

}

