package ctgen;

/**
 * Interface used in {@link CTGenBuilder} provides option to run {@link CTGen}.
 * Because it implements interface {@link CTGenOptionals} it also provides methods to set optional parameters of {@link CTGen}.
 *
 * @author Tomáš Matějka
 */
public interface RunCTGen extends CTGenOptionals {

    /**
     * Runs CTGen with parameters set by {@link AtlboConsumer}, {@link TransformerConsumer}, {@link RunCTGen},
     * {@link CTGenOptionals} interfaces implemented by {@link CTGenBuilder}.
     */
    void go();

}
