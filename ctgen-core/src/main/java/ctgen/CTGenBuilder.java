package ctgen;

import atlbo.Atlbo;
import codeformat.CodeFormatter;
import codeformat.EmptyCodeFormatter;
import input.parser.AbstractParser;
import transformation.Transformer;

/**
 * Builder to set mandatory and optional parameters of {@link CTGen}.
 *
 * @author Tomáš Matějka
 */
public class CTGenBuilder implements AtlboConsumer, TransformerConsumer, RunCTGen {

    /**
     * Mandatory parameter. Set from constructor when creating instance of builder.
     * Path for input file containing description of test classes.
     */
    private String inputFile;

    /**
     * Mandatory parameter. Set from constructor when creating instance of builder.
     * Parser which will be used to parse input file and create input model.
     */
    private AbstractParser parser;

    /**
     * Mandatory parameter. Set through interface {@link AtlboConsumer}.
     * Instance of {@link Atlbo} that will provide test sets when creating output.
     */
    private Atlbo atlbo;

    /**
     * Mandatory parameter. Set through interface {@link TransformerConsumer}.
     * Instance of {@link Transformer} that will transform input model, test sets provided by {@link Atlbo} into the output form.
     */
    private Transformer transformer;

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * File extension used when saving generated files by {@link CTGen}.
     * Default extension is 'java'.
     */
    private String filesExtension = "java";

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * Output folder into which generated files will be saved.
     * Default folder is './'.
     */
    private String outputFolder = "./";

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * Interactions strength for used {@link atlbo.Atlbo}.
     * Default t is '2'.
     */
    private int t = 2;

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * Size of the population generated for used {@link atlbo.Atlbo}.
     * Default population size is '40'.
     */
    private int populationSize = 40;

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * Maximum size of output test suite from {@link atlbo.Atlbo}.
     * Default limit is 'Integer.MAX_VALUE'.
     */
    private int atlboSuiteLimit = Integer.MAX_VALUE;

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * Flag indicating if generated classes should be compiled in memory and run immediately.
     * Default value is 'false'.
     */
    private boolean compileAndRunImmediately = false;

    /**
     * Optional parameter. Set through interface {@link CTGenOptionals}.
     * Formatter to format generated source codes.
     * Default formatter is {@link EmptyCodeFormatter}.
     */
    private CodeFormatter codeFormatter = new EmptyCodeFormatter();

    /**
     * Constructor. Creates new instance for given file and parser.
     *
     * @param file   input file containing description of test classes
     * @param parser parser which will be used to parse input file and create input model
     */
    public CTGenBuilder(String file, AbstractParser parser) {
        this.inputFile = file;
        this.parser = parser;
    }

    @Override
    public TransformerConsumer atlbo(Atlbo atlbo) {
        this.atlbo = atlbo;
        return this;
    }

    @Override
    public RunCTGen transformer(Transformer transformer) {
        this.transformer = transformer;
        return this;
    }

    @Override
    public RunCTGen filesExtension(String filesExtension) {
        this.filesExtension = filesExtension;
        return this;
    }

    @Override
    public RunCTGen outputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
        return this;
    }

    @Override
    public RunCTGen populationSize(int populationSize) {
        this.populationSize = populationSize;
        return this;
    }

    @Override
    public RunCTGen atlboSuiteLimit(int atlboSuiteLimit) {
        this.atlboSuiteLimit = atlboSuiteLimit;
        return this;
    }

    @Override
    public RunCTGen compileAndRunImmediately(boolean compileAndRunImmediately) {
        this.compileAndRunImmediately = compileAndRunImmediately;
        return this;
    }

    @Override
    public RunCTGen codeFormatter(CodeFormatter codeFormatter) {
        this.codeFormatter = codeFormatter;
        return this;
    }

    @Override
    public void go() {
        CTGen ctGen = new CTGen(parser, atlbo, transformer, codeFormatter,
                inputFile, outputFolder, filesExtension, populationSize, atlboSuiteLimit, compileAndRunImmediately);
        try {
            ctGen.generate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
