package ctgen;

import atlbo.Atlbo;
import codeformat.CodeFormatter;
import element.ClassElement;
import element.MethodElement;
import element.RootElement;
import input.model.TestClass;
import input.parser.AbstractParser;
import input.parser.JsonParser;
import general.FileUtils;
import input.parser.XmlParser;
import memorycompile.CodeRunner;
import transformation.Transformer;

import java.util.List;

/**
 * Entry point into CTGen program.
 * Static methods {@link #fromXml(String)} and {@link #fromJson(String)} provide instance of builder {@link CTGenBuilder}
 * to set all mandatory and optional parameters and to run CTGen.
 * During a run, CTGen loads input file using given {@link AbstractParser} into the input model {@link input.model},
 * then using given {@link Transformer}, {@link Atlbo} and ATLBO parameters (t, population size, final test suite limit)
 * creates output forms of test classes that are saved to files.
 * <p>
 * To see all possible parameters check:
 * {@link AtlboConsumer}
 * {@link TransformerConsumer}
 * {@link CTGenOptionals}
 *
 * @author Tomáš Matějka
 */
public final class CTGen {

    /** Parser which will be used to parse input file and create input model. */
    private AbstractParser parser;

    /** Instance of {@link Atlbo} that will provide test sets when creating output. */
    private Atlbo atlbo;

    /** Instance of {@link Transformer} that will transform input model, test sets provided by {@link Atlbo} into the output form. */
    private Transformer transformer;

    /** Formatter to format generated source codes. */
    private CodeFormatter codeFormatter;

    /** Path for input file containing description of test classes. */
    private String inputFile;

    /** Output folder into which generated files will be saved. */
    private String outputFolder;

    /** File extension used when saving generated files by {@link CTGen}. */
    private String filesExtension;

    /** Size of the population generated for used {@link atlbo.Atlbo}. */
    private int populationSize;

    /** Maximum size of output test suite from {@link atlbo.Atlbo}. */
    private int atlboSuiteLimit;

    /** Flag indicating if generated classes should be compiled in memory and run immediately. */
    private boolean compileAndRunImmediately;

    /**
     * Constructor creates new instance of CTGen with all parameters set.
     *
     * @param parser                   parser which will be used to parse input file and create input model
     * @param atlbo                    instance of {@link Atlbo} that will provide test sets when creating output
     * @param transformer              instance of {@link Transformer} that will transform input model, test sets provided by {@link Atlbo} into the output form
     * @param codeFormatter            formatter to format generated source codes
     * @param inputFile                path for input file containing description of test classes
     * @param outputFolder             output folder into which generated files will be saved
     * @param filesExtension           file extension used when saving generated files by {@link CTGen}
     * @param populationSize           size of the population generated for used {@link atlbo.Atlbo}
     * @param atlboSuiteLimit          maximum size of output test suite from {@link atlbo.Atlbo}
     * @param compileAndRunImmediately flag indicating if generated classes should be compiled in memory and run immediately
     */
    CTGen(AbstractParser parser, Atlbo atlbo, Transformer transformer, CodeFormatter codeFormatter,
          String inputFile, String outputFolder, String filesExtension, int populationSize, int atlboSuiteLimit,
          boolean compileAndRunImmediately) {
        this.parser = parser;
        this.atlbo = atlbo;
        this.transformer = transformer;
        this.codeFormatter = codeFormatter;

        this.inputFile = inputFile;
        this.outputFolder = outputFolder;
        this.filesExtension = filesExtension;
        this.populationSize = populationSize;
        this.atlboSuiteLimit = atlboSuiteLimit;
        this.compileAndRunImmediately = compileAndRunImmediately;
    }

    /**
     * Generates output for parameters set through constructor gathered from builder.
     * Parses input file, transform input and ATLBO results to output form and saves these output forms as string into the output folder
     * and when flag 'compileAndRunImmediately' is set to true, immediately compiles (in memory) and runs generated classes.
     *
     * @throws Exception any exception that can occur during parsing, transforming, saving or compiling
     */
    public void generate() throws Exception {
        List<TestClass> testClasses = parser.parse(inputFile);
        List<RootElement> generatedClasses = transformer.transform(testClasses, atlbo, populationSize, atlboSuiteLimit);

        for (RootElement root : generatedClasses) {
            ClassElement generatedClass = (ClassElement) root;
            String code = generatedClass.write();
            String formatted = codeFormatter.formatCode(code);

            FileUtils.save(outputFolder, generatedClass.getName(), filesExtension, formatted);

            if (compileAndRunImmediately) {
                for (MethodElement method : generatedClass.getMethods()) {
                    CodeRunner.run(generatedClass.getName(), method.getName(), code);
                }
            }
        }
    }

    /**
     * Creates new {@link CTGenBuilder} for given input file and {@link XmlParser}.
     *
     * @param file input file containing description of test classes
     * @return new instance of CTGenBuilder
     */
    public static AtlboConsumer fromXml(String file) {
        return new CTGenBuilder(file, new XmlParser());
    }

    /**
     * reates new {@link CTGenBuilder} for given input file and {@link JsonParser}.
     *
     * @param file input file containing description of test classes
     * @return new instance of CTGenBuilder
     */
    public static AtlboConsumer fromJson(String file) {
        return new CTGenBuilder(file, new JsonParser());
    }

    public static AtlboConsumer fromFile(String file) throws UnsupportedFileFormatException {
        String extension = file.substring(file.lastIndexOf(".") + 1).toLowerCase();
        if (extension.equals("xml")) {
            return fromXml(file);
        } else if (extension.equals("json")) {
            return fromJson(file);
        } else {
            throw new UnsupportedFileFormatException("'" + extension + "' is not supported extension.");
        }
    }

}
