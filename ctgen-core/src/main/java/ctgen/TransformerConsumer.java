package ctgen;

import transformation.Transformer;

/**
 * Interface used in {@link CTGenBuilder} forcing implementing class to set {@link Transformer}
 * before being able to create instance or move on with building process.
 *
 * @author Tomáš Matějka
 */
public interface TransformerConsumer {

    /**
     * Sets given transformer and return instance of class implementing {@link RunCTGen}
     * which provides more builder options and option to run {@link CTGen}.
     *
     * @param transformer transformer to be set
     * @return class implementing 'RunCTGen'
     */
    RunCTGen transformer(Transformer transformer);

}