package ctgen;

/**
 * Exception thrown when input file for {@link CTGen#fromFile(String)} is neither 'xml' or 'json'.
 *
 * @author Tomáš Matějka
 */
public class UnsupportedFileFormatException extends Exception {

    /**
     * Constructor.
     * Creates new instance.
     *
     * @param message error message saying what happened
     */
    public UnsupportedFileFormatException(String message) {
        super(message);
    }

}
