package ctgen;

import codeformat.CodeFormatter;

/**
 * Interface used in {@link CTGenBuilder} provides options to set optionaal parameters for {@link CTGen}.
 *
 * @author Tomáš Matějka
 */
public interface CTGenOptionals {

    /**
     * Sets file extension used when saving generated files by {@link CTGen}.
     *
     * @param filesExtension extension of files
     * @return instance of builder for future use
     */
    RunCTGen filesExtension(String filesExtension);

    /**
     * Sets output folder into which generated files will be saved.
     *
     * @param outputFolder folder for generated files
     * @return instance of builder for future use
     */
    RunCTGen outputFolder(String outputFolder);


    /**
     * Sets size of the population generated for used {@link atlbo.Atlbo}
     *
     * @param populationSize population size
     * @return instance of builder for future use
     */
    RunCTGen populationSize(int populationSize);

    /**
     * Sets the maximum size of output test suite from {@link atlbo.Atlbo}
     *
     * @param atlboSuiteLimit maximum size of output test suite
     * @return instance of builder for future use
     */
    RunCTGen atlboSuiteLimit(int atlboSuiteLimit);

    /**
     * Sets flag indicating if generated classes should be compiled in memory and run immediately.
     *
     * @param compileAndRunImmediately boolean flag for immediate run
     * @return instance of builder for future use
     */
    RunCTGen compileAndRunImmediately(boolean compileAndRunImmediately);

    /**
     * Sets formatter to format generated source codes.
     *
     * @param codeFormatter formatter used to format code
     * @return instance of builder for future use
     */
    RunCTGen codeFormatter(CodeFormatter codeFormatter);

}
