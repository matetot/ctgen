package general;

/**
 * Provides utility for working with strings in CTGen project, hence the 'C' in class name.
 *
 * @author Tomáš Matějka
 */
public final class CStringUtils {

    /**
     * Appends one string (appendedPart) to another (currentIdentifier) with '.' as separator between them.
     * If 'currentIDentifier' is empty string, '.'is not added to final string.
     * E. g. for empty currentIdentifier and appended part 'age', output string is just 'age'
     * but for currentIdentifier 'person' and appended part 'age', output string is 'person.age'.
     *
     * @param currentIdentifier base string to which another string is appended
     * @param appendedPart string that is being appended
     * @return final concatenated string
     */
    public static String append(String currentIdentifier, String appendedPart) {
        return currentIdentifier + (currentIdentifier.isEmpty() ? "" : ".") + appendedPart;
    }

    /**
     * For given attribute name creates name of setter method.
     * This method is just assuming that correct setter name is created by adding 'set' in front ot attribute name and
     * setting first attribute character to upper case.
     *
     * @param attributeName attribute name setter is being created for
     * @return created setter
     */
    public static String createSetter(String attributeName) {
        return "set" + (attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1));
    }

    /**
     * For given class type creates name of a variable of that class type.
     * E. g. for class type 'TestObjectEasy' created variable name is  'testObjectEasy'.
     *
     * @param classType class type that is used for variable name
     * @return created class variable name
     */
    public static String createClassName(String classType) {
        return classType.substring(0, 1).toLowerCase() + classType.substring(1);
    }

}
