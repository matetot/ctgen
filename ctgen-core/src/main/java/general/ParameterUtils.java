package general;

import configuration.Parameter;
import input.model.TestEnum;
import input.model.TestObject;
import input.model.TestParameter;
import input.model.TestPrimitive;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides functionality to dig ATLBO parameters {@link Parameter} from test parameters {@link TestParameter}
 * and to find ATLBO parameter by identifier.
 *
 * @author Tomáš Matějka
 */
public final class ParameterUtils {

    /**
     * From given test parameters create list of corresponding ATLBO parameters with identifiers.
     * One test parameters does not have to equal one ATLBO parameter. If one object parameter has three primitive attributes,
     * each of these attributes is ATLBO parameter etc.
     * Identifier is created from names of test parameters (e.g. for primitive attribute named 'age' in object 'person', identifier would be 'person.ge')
     * Thanks to identifier, any ATLBO parameter can be found for given valid identifier in the future.
     *
     * @param testParameters test parameters
     * @return list of ATLBO parameters with corresponding identifier
     */
    public static List<Parameter> mapToAtlboParameters(List<TestParameter> testParameters) {
        List<Parameter> mappedParameters = new ArrayList<>();
        int [] position = {0};
        map("", position, testParameters, mappedParameters);
        return mappedParameters;
    }

    /**
     * In given list of ATLBO parameters finds parameter corresponding to given identifier.
     *
     * @param atlboParameters searched ATLBO parameters
     * @param identifier searched identifier
     * @return found ATLBO parameter or 'null'
     */
    public static Parameter find(List<Parameter> atlboParameters, String identifier) {
        return atlboParameters.stream().filter(p -> p.getIdentifier().equals(identifier)).findFirst().orElse(null);
    }

    private static void map(String identifier, int[] position, List<TestParameter> testParameters, List<Parameter> mappedParameters) {
        for (TestParameter parameter : testParameters) {
            if (parameter.parameterType() == ParameterType.OBJECT) {
                TestObject testObject = (TestObject) parameter;
                map(CStringUtils.append(identifier, testObject.getName()), position, testObject.getAttributes(), mappedParameters);
            } else if (parameter.parameterType() == ParameterType.ENUM) {
                TestEnum testEnum = (TestEnum) parameter;
                Parameter p = new Parameter(position[0], testEnum.getValues().size(), CStringUtils.append(identifier, testEnum.getName()));
                mappedParameters.add(p);
                position[0] += 1;
            } else {
                TestPrimitive testPrimitive = (TestPrimitive) parameter;
                Parameter p = new Parameter(position[0], testPrimitive.getValues().size(), CStringUtils.append(identifier, testPrimitive.getName()));
                mappedParameters.add(p);
                position[0] += 1;
            }
        }
    }

}
