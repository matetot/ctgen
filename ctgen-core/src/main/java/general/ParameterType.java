package general;

/**
 * Enum for possible types of test parameter.
 *
 * @author Tomáš Matějka
 */
public enum ParameterType {

    PRIMITIVE,

    OBJECT,

    ARRAY,

    ENUM

}
