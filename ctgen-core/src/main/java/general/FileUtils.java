package general;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Provides methods for saving files and creating folders.
 *
 * @author Tomáš Matějka
 */
public final class FileUtils {

    /**
     * Saves a file of given name with given extension and content to given folder.
     * Also checks existence of given folder path and if needed, creates the folder.
     *
     * @param folderPath folder that file will be saved to
     * @param fileName file name
     * @param fileExtension file extension
     * @param content file content
     */
    public static void save(String folderPath, String fileName, String fileExtension, String content) {
        createFolder(folderPath);
        save(folderPath + System.getProperty("file.separator") + fileName + "." + fileExtension, content);
    }

    /**
     * Save a file with given name and content on file system.
     *
     * @param fileName saved file name
     * @param content saved file content
     */
    public static void save(String fileName, String content) {
        try {
            PrintStream printStream = new PrintStream(new FileOutputStream(fileName, false));
            printStream.print(content);
            printStream.flush();
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if folders on given path already exists and if it does not, creates the folders.
     *
     * @param folderPath folder/s to create
     */
    public static void createFolder(String folderPath) {
        Path path = Paths.get(folderPath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
