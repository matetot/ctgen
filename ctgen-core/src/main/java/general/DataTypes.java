package general;

public final class DataTypes {

    public static final String INTEGER = "java.lang.Integer";

    public static final String LONG = "java.lang.Long";

    public static final String BOOLEAN = "java.lang.Boolean";

    public static final String CHARARACTER = "java.lang.Character";

    public static final String DOUBLE = "java.lang.Double";

    public static final String FLOAT = "java.lang.Float";

    public static final String STRING_SHORT = "String";

    public static final String STRING_FULL = "java.lang.String";

}
