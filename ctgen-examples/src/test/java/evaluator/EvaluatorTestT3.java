package evaluator;

import ctgen.example02.Evaluator;
import org.junit.jupiter.api.Test;

public class EvaluatorTestT3 {
  @Test
  public void evaluateTestCase1() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase2() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase3() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase4() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase5() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase6() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase7() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase8() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase9() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase10() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase11() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase12() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase13() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase14() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase15() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase16() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase17() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase18() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase19() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase20() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase21() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase22() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase23() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase24() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase25() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase26() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase27() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase28() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase29() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase30() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase31() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase32() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase33() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase34() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase35() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase36() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase37() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase38() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase39() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase40() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase41() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase42() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase43() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase44() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase45() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase46() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase47() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase48() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase49() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase50() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase51() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase52() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase53() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase54() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase55() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase56() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase57() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase58() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase59() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase60() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase61() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase62() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase63() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase64() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase65() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase66() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase67() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase68() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase69() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase70() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase71() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase72() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase73() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase74() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase75() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase76() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase77() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase78() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase79() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase80() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase81() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase82() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase83() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase84() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase85() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase86() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase87() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase88() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase89() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase90() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase91() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase92() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase93() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase94() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase95() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase96() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase97() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase98() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase99() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase100() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase101() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase102() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase103() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase104() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase105() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase106() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase107() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase108() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase109() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase110() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase111() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase112() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase113() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase114() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase115() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase116() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase117() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase118() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase119() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase120() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase121() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase122() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase123() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase124() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase125() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase126() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase127() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase128() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase129() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase130() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase131() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase132() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase133() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase134() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase135() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase136() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase137() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase138() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase139() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase140() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase141() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase142() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase143() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase144() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase145() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase146() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase147() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase148() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase149() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase150() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase151() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase152() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase153() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase154() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase155() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase156() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase157() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase158() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase159() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase160() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase161() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase162() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase163() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase164() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase165() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase166() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase167() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }
}
