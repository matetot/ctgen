package evaluator;

import ctgen.example02.Evaluator;
import org.junit.jupiter.api.Test;

public class EvaluatorTestT4 {
  @Test
  public void evaluateTestCase1() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase2() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase3() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase4() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase5() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase6() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase7() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase8() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase9() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase10() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase11() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase12() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase13() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase14() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase15() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase16() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase17() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase18() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase19() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase20() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase21() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase22() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase23() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase24() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase25() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase26() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase27() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase28() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase29() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase30() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase31() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase32() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase33() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase34() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase35() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase36() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase37() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase38() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase39() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase40() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase41() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase42() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase43() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase44() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase45() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase46() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase47() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase48() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase49() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase50() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase51() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase52() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase53() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase54() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase55() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase56() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase57() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase58() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase59() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase60() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase61() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase62() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase63() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase64() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase65() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase66() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase67() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase68() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase69() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase70() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase71() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase72() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase73() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase74() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase75() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase76() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase77() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase78() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase79() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase80() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase81() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase82() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase83() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase84() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase85() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase86() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase87() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase88() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase89() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase90() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase91() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase92() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase93() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase94() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase95() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase96() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase97() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase98() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase99() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase100() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase101() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase102() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase103() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase104() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase105() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase106() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase107() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase108() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase109() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase110() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase111() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase112() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase113() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase114() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase115() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase116() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase117() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase118() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase119() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase120() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase121() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase122() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase123() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase124() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase125() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase126() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase127() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase128() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase129() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase130() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase131() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase132() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase133() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase134() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase135() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase136() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase137() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase138() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase139() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase140() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase141() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase142() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase143() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase144() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase145() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase146() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase147() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase148() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase149() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase150() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase151() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase152() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase153() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase154() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase155() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase156() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase157() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase158() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase159() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase160() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase161() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase162() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase163() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase164() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase165() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase166() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase167() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase168() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase169() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase170() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase171() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase172() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase173() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase174() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase175() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase176() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase177() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase178() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase179() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase180() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase181() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase182() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase183() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase184() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase185() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase186() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase187() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase188() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase189() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase190() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase191() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase192() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase193() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase194() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase195() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase196() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase197() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase198() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase199() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase200() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase201() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase202() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase203() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase204() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase205() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase206() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase207() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase208() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase209() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase210() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase211() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase212() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase213() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase214() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase215() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase216() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase217() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase218() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase219() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase220() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase221() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase222() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase223() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase224() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase225() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase226() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase227() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase228() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase229() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase230() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase231() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase232() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase233() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase234() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase235() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase236() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase237() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase238() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase239() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase240() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase241() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase242() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase243() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase244() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase245() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase246() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase247() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase248() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase249() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase250() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase251() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase252() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase253() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase254() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase255() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase256() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase257() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase258() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase259() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase260() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase261() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase262() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase263() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase264() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase265() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase266() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase267() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase268() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase269() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase270() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase271() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase272() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase273() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase274() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase275() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase276() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase277() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase278() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase279() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase280() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase281() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase282() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase283() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase284() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase285() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase286() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase287() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase288() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase289() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase290() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase291() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase292() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase293() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase294() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase295() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase296() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase297() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase298() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase299() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase300() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase301() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase302() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase303() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase304() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase305() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase306() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase307() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase308() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase309() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase310() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase311() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase312() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase313() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase314() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase315() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase316() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase317() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase318() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase319() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase320() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase321() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase322() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase323() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase324() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase325() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase326() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase327() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase328() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase329() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase330() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase331() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase332() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase333() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase334() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase335() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase336() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase337() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase338() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase339() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase340() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase341() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase342() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase343() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase344() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase345() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase346() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase347() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase348() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase349() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase350() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase351() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase352() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase353() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase354() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase355() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase356() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase357() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase358() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase359() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase360() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase361() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase362() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase363() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase364() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase365() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase366() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase367() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase368() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase369() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase370() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase371() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase372() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase373() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase374() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase375() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase376() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase377() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase378() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase379() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase380() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase381() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase382() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase383() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase384() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase385() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase386() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase387() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase388() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase389() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase390() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase391() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase392() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase393() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase394() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase395() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase396() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase397() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase398() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase399() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase400() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase401() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase402() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase403() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase404() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase405() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase406() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase407() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase408() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase409() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase410() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase411() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase412() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase413() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase414() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase415() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase416() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase417() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase418() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase419() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase420() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase421() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase422() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase423() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase424() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase425() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase426() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase427() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase428() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase429() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase430() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase431() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase432() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase433() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase434() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase435() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase436() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase437() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase438() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase439() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase440() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase441() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase442() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase443() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase444() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase445() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase446() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase447() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase448() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase449() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase450() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase451() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase452() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase453() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase454() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase455() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase456() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase457() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase458() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase459() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase460() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase461() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase462() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase463() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase464() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase465() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase466() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase467() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase468() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase469() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase470() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase471() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase472() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase473() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase474() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase475() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase476() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase477() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase478() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase479() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase480() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase481() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase482() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase483() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase484() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase485() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase486() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase487() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase488() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase489() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase490() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase491() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase492() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase493() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase494() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase495() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase496() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase497() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase498() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase499() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase500() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase501() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase502() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase503() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase504() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase505() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase506() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase507() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase508() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase509() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase510() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase511() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase512() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase513() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase514() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase515() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase516() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase517() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase518() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase519() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase520() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase521() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase522() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }
}
