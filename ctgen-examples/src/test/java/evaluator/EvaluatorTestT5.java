package evaluator;

import ctgen.example02.Evaluator;
import org.junit.jupiter.api.Test;

public class EvaluatorTestT5 {
  @Test
  public void evaluateTestCase1() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase2() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase3() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase4() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase5() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase6() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase7() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase8() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase9() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase10() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase11() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase12() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase13() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase14() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase15() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase16() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase17() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase18() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase19() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase20() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase21() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase22() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase23() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase24() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase25() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase26() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase27() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase28() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase29() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase30() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase31() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase32() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase33() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase34() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase35() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase36() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase37() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase38() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase39() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase40() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase41() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase42() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase43() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase44() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase45() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase46() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase47() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase48() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase49() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase50() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase51() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase52() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase53() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase54() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase55() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase56() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase57() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase58() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase59() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase60() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase61() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase62() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase63() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase64() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase65() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase66() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase67() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase68() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase69() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase70() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase71() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase72() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase73() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase74() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase75() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase76() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase77() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase78() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase79() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase80() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase81() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase82() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase83() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase84() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase85() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase86() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase87() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase88() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase89() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase90() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase91() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase92() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase93() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase94() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase95() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase96() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase97() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase98() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase99() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase100() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase101() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase102() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase103() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase104() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase105() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase106() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase107() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase108() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase109() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase110() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase111() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase112() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase113() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase114() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase115() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase116() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase117() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase118() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase119() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase120() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase121() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase122() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase123() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase124() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase125() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase126() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase127() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase128() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase129() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase130() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase131() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase132() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase133() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase134() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase135() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase136() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase137() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase138() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase139() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase140() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase141() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase142() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase143() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase144() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase145() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase146() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase147() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase148() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase149() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase150() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase151() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase152() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase153() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase154() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase155() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase156() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase157() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase158() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase159() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase160() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase161() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase162() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase163() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase164() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase165() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase166() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase167() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase168() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase169() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase170() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase171() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase172() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase173() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase174() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase175() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase176() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase177() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase178() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase179() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase180() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase181() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase182() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase183() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase184() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase185() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase186() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase187() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase188() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase189() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase190() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase191() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase192() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase193() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase194() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase195() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase196() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase197() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase198() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase199() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase200() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase201() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase202() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase203() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase204() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase205() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase206() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase207() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase208() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase209() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase210() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase211() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase212() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase213() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase214() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase215() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase216() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase217() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase218() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase219() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase220() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase221() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase222() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase223() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase224() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase225() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase226() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase227() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase228() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase229() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase230() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase231() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase232() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase233() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase234() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase235() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase236() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase237() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase238() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase239() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase240() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase241() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase242() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase243() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase244() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase245() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase246() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase247() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase248() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase249() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase250() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase251() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase252() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase253() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase254() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase255() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase256() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase257() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase258() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase259() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase260() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase261() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase262() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase263() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase264() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase265() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase266() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase267() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase268() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase269() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase270() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase271() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase272() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase273() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase274() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase275() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase276() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase277() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase278() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase279() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase280() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase281() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase282() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase283() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase284() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase285() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase286() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase287() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase288() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase289() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase290() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase291() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase292() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase293() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase294() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase295() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase296() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase297() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase298() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase299() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase300() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase301() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase302() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase303() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase304() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase305() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase306() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase307() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase308() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase309() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase310() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase311() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase312() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase313() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase314() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase315() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase316() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase317() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase318() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase319() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase320() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase321() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase322() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase323() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase324() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase325() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase326() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase327() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase328() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase329() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase330() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase331() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase332() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase333() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase334() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase335() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase336() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase337() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase338() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase339() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase340() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase341() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase342() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase343() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase344() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase345() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase346() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase347() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase348() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase349() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase350() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase351() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase352() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase353() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase354() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase355() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase356() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase357() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase358() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase359() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase360() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase361() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase362() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase363() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase364() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase365() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase366() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase367() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase368() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase369() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase370() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase371() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase372() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase373() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase374() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase375() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase376() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase377() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase378() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase379() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase380() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase381() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase382() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase383() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase384() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase385() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase386() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase387() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase388() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase389() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase390() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase391() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase392() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase393() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase394() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase395() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase396() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase397() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase398() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase399() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase400() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase401() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase402() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase403() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase404() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase405() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase406() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase407() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase408() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase409() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase410() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase411() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase412() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase413() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase414() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase415() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase416() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase417() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase418() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase419() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase420() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase421() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase422() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase423() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase424() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase425() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase426() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase427() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase428() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase429() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase430() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase431() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase432() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase433() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase434() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase435() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase436() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase437() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase438() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase439() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase440() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase441() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase442() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase443() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase444() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase445() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase446() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase447() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase448() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase449() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase450() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase451() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase452() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase453() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase454() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase455() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase456() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase457() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase458() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase459() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase460() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase461() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase462() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase463() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase464() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase465() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase466() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase467() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase468() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase469() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase470() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase471() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase472() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase473() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase474() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase475() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase476() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase477() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase478() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase479() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase480() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase481() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase482() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase483() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase484() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase485() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase486() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase487() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase488() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase489() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase490() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase491() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase492() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase493() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase494() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase495() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase496() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase497() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase498() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase499() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase500() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase501() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase502() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase503() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase504() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase505() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase506() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase507() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase508() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase509() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase510() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase511() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase512() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase513() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase514() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase515() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase516() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase517() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase518() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase519() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase520() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase521() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase522() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase523() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase524() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase525() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase526() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase527() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase528() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase529() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase530() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase531() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase532() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase533() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase534() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase535() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase536() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase537() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase538() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase539() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase540() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase541() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase542() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase543() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase544() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase545() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase546() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase547() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase548() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase549() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase550() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase551() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase552() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase553() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase554() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase555() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase556() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase557() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase558() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase559() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase560() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase561() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase562() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase563() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase564() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase565() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase566() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase567() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase568() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase569() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase570() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase571() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase572() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase573() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase574() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase575() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase576() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase577() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase578() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase579() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase580() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase581() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase582() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase583() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase584() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase585() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase586() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase587() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase588() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase589() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase590() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase591() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase592() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase593() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase594() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase595() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase596() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase597() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase598() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase599() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase600() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase601() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase602() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase603() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase604() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase605() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase606() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase607() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase608() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase609() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase610() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase611() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase612() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase613() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase614() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase615() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase616() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase617() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase618() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase619() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase620() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase621() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase622() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase623() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase624() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase625() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase626() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase627() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase628() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase629() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase630() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase631() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase632() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase633() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase634() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase635() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase636() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase637() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase638() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase639() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase640() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase641() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase642() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase643() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase644() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase645() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase646() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase647() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase648() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase649() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase650() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase651() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase652() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase653() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase654() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase655() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase656() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase657() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase658() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase659() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase660() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase661() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase662() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase663() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase664() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase665() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase666() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase667() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase668() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase669() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase670() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase671() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase672() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase673() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase674() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase675() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase676() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase677() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase678() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase679() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase680() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase681() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase682() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase683() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase684() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase685() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase686() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase687() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase688() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase689() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase690() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase691() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase692() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase693() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase694() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase695() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase696() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase697() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase698() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase699() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase700() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase701() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase702() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase703() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase704() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase705() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase706() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase707() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase708() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase709() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase710() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase711() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase712() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase713() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase714() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase715() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase716() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase717() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase718() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase719() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase720() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase721() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase722() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase723() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase724() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase725() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase726() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase727() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase728() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase729() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase730() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase731() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase732() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase733() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase734() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase735() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase736() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase737() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase738() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase739() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase740() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase741() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase742() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase743() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase744() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase745() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase746() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase747() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase748() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase749() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase750() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase751() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase752() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase753() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase754() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase755() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase756() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase757() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase758() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase759() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase760() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase761() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase762() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase763() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase764() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase765() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase766() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase767() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase768() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase769() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase770() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase771() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase772() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase773() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase774() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase775() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase776() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase777() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase778() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase779() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase780() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase781() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase782() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase783() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase784() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase785() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase786() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase787() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase788() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase789() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase790() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase791() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase792() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase793() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase794() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase795() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase796() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase797() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase798() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase799() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase800() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase801() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase802() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase803() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase804() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase805() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase806() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase807() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase808() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase809() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase810() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase811() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase812() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase813() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase814() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase815() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase816() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase817() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase818() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase819() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase820() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase821() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase822() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase823() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase824() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase825() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase826() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase827() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase828() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase829() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase830() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase831() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase832() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase833() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase834() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase835() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase836() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase837() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase838() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase839() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase840() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase841() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase842() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase843() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase844() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase845() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase846() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase847() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase848() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase849() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase850() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase851() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase852() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase853() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase854() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase855() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase856() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase857() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase858() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase859() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase860() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase861() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase862() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase863() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase864() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase865() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase866() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase867() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase868() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase869() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase870() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase871() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase872() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase873() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase874() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase875() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase876() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase877() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase878() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase879() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase880() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase881() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase882() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase883() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase884() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase885() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase886() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase887() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase888() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase889() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase890() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase891() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase892() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase893() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase894() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase895() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase896() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase897() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase898() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase899() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase900() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase901() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase902() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase903() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase904() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase905() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase906() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase907() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase908() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase909() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase910() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase911() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase912() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase913() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase914() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase915() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase916() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase917() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase918() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase919() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase920() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase921() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase922() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase923() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase924() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase925() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase926() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase927() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase928() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase929() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase930() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase931() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase932() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase933() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase934() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase935() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase936() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase937() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase938() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase939() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase940() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase941() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase942() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase943() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase944() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase945() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase946() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase947() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase948() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase949() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase950() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase951() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase952() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase953() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase954() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase955() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase956() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase957() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase958() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase959() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase960() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase961() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase962() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase963() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase964() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase965() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase966() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase967() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase968() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase969() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase970() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase971() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase972() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase973() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase974() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase975() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase976() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase977() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase978() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase979() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase980() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase981() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase982() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase983() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase984() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase985() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase986() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase987() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase988() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase989() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase990() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase991() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase992() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase993() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase994() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase995() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase996() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase997() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase998() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase999() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1000() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1001() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1002() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1003() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1004() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1005() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1006() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1007() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1008() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1009() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1010() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1011() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1012() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1013() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1014() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1015() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1016() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1017() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1018() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1019() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1020() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1021() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1022() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1023() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1024() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1025() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1026() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1027() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1028() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1029() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1030() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1031() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1032() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1033() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1034() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1035() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1036() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1037() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1038() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1039() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1040() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1041() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1042() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1043() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1044() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1045() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1046() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1047() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1048() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1049() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1050() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1051() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1052() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1053() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1054() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1055() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1056() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1057() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1058() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1059() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1060() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1061() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1062() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1063() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1064() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1065() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1066() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1067() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1068() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1069() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1070() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1071() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1072() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1073() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1074() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1075() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1076() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1077() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1078() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1079() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1080() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1081() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1082() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1083() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1084() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1085() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1086() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1087() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1088() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1089() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1090() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1091() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1092() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1093() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1094() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1095() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1096() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1097() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1098() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1099() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1100() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1101() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1102() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1103() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1104() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1105() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1106() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1107() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1108() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1109() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1110() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1111() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1112() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1113() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1114() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1115() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1116() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1117() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1118() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1119() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1120() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1121() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1122() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1123() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1124() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1125() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1126() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1127() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1128() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1129() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1130() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1131() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1132() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1133() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1134() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1135() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1136() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1137() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1138() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1139() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1140() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1141() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1142() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1143() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1144() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1145() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1146() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1147() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1148() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1149() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1150() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1151() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1152() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1153() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1154() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1155() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1156() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1157() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1158() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1159() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1160() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1161() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1162() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1163() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1164() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1165() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1166() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1167() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1168() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1169() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1170() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1171() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1172() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1173() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1174() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1175() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1176() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1177() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1178() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1179() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1180() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1181() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1182() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1183() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1184() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1185() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1186() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1187() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1188() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1189() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1190() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1191() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1192() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1193() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1194() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1195() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1196() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1197() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1198() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1199() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1200() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1201() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1202() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1203() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1204() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1205() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1206() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1207() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1208() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1209() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1210() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1211() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1212() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1213() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1214() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1215() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1216() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1217() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1218() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1219() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1220() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1221() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1222() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1223() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1224() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1225() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1226() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1227() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1228() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1229() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1230() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1231() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1232() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1233() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1234() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1235() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1236() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1237() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1238() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1239() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1240() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1241() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1242() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1243() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1244() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1245() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1246() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1247() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1248() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1249() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1250() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1251() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1252() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1253() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1254() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1255() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1256() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1257() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1258() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1259() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1260() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1261() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1262() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1263() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1264() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1265() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1266() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1267() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1268() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1269() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1270() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1271() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1272() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1273() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1274() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1275() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1276() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1277() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1278() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1279() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1280() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1281() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1282() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1283() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1284() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1285() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1286() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1287() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1288() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1289() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1290() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1291() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1292() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1293() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1294() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1295() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1296() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1297() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1298() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1299() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1300() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1301() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1302() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1303() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1304() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1305() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1306() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1307() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1308() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1309() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1310() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1311() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1312() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1313() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1314() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1315() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1316() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1317() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1318() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1319() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1320() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1321() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1322() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1323() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1324() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1325() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1326() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1327() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1328() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1329() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1330() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1331() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1332() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1333() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1334() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1335() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1336() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1337() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1338() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1339() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1340() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1341() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1342() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1343() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1344() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1345() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1346() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1347() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1348() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1349() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1350() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1351() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1352() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1353() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1354() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1355() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1356() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1357() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1358() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1359() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1360() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1361() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1362() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1363() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1364() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1365() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1366() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1367() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1368() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1369() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1370() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1371() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1372() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1373() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1374() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1375() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1376() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1377() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1378() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1379() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1380() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1381() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1382() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1383() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1384() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1385() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1386() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1387() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1388() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1389() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1390() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1391() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1392() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1393() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1394() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1395() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1396() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1397() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1398() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1399() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1400() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1401() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1402() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1403() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1404() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1405() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1406() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1407() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1408() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1409() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1410() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1411() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase1412() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }
}
