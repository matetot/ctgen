package evaluator;

import ctgen.example02.Evaluator;
import org.junit.jupiter.api.Test;

public class EvaluatorTestT2 {
  @Test
  public void evaluateTestCase1() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase2() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase3() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase4() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase5() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 0;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase6() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase7() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase8() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 4;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase9() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase10() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase11() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase12() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase13() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase14() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase15() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase16() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase17() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase18() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 2;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase19() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 1;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase20() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase21() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase22() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase23() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase24() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 5;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase25() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase26() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase27() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 3;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase28() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 5;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase29() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase30() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 1;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase31() {
    Evaluator evaluator = new Evaluator();

    int degree = 4;
    int children = 3;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase32() {
    Evaluator evaluator = new Evaluator();

    int degree = 0;
    int children = 5;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase33() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 1;
    boolean read = true;
    boolean write = false;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase34() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 1;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase35() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 0;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase36() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 2;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase37() {
    Evaluator evaluator = new Evaluator();

    int degree = 2;
    int children = 3;
    boolean read = false;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase38() {
    Evaluator evaluator = new Evaluator();

    int degree = 5;
    int children = 4;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = false;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = false;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase39() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 3;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase40() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 2;
    boolean read = true;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = true;
    boolean experience = true;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase41() {
    Evaluator evaluator = new Evaluator();

    int degree = 3;
    int children = 5;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 1;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase42() {
    Evaluator evaluator = new Evaluator();

    int degree = 6;
    int children = 0;
    boolean read = true;
    boolean write = false;
    boolean speak = false;
    boolean understand = false;
    boolean newGrad = false;
    boolean experience = true;
    boolean english = true;
    boolean disability = true;
    int maritalStatus = 0;
    int resident = 1;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase43() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 0;
    boolean read = false;
    boolean write = true;
    boolean speak = false;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = true;
    boolean disability = false;
    int maritalStatus = 1;
    int resident = 0;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }

  @Test
  public void evaluateTestCase44() {
    Evaluator evaluator = new Evaluator();

    int degree = 1;
    int children = 4;
    boolean read = false;
    boolean write = true;
    boolean speak = true;
    boolean understand = true;
    boolean newGrad = false;
    boolean experience = false;
    boolean english = false;
    boolean disability = false;
    int maritalStatus = 2;
    int resident = 2;
    evaluator.evaluate(
        degree,
        children,
        read,
        write,
        speak,
        understand,
        newGrad,
        experience,
        english,
        disability,
        maritalStatus,
        resident);
  }
}
