package atlbo;

import atlbo.configurations.Table4;
import fuzzy.FuzzySystemImpl;
import interaction.InteractionsGeneratorImpl;
import population.PopulationGeneratorImpl;
import report.ConsoleReporter;
import report.FileReporter;
import report.Reporter;
import run.Execution;
import run.Executor;

import java.io.FileNotFoundException;

public class RunTable4 {

    public static void main(String[] args) throws FileNotFoundException {
        int iterations = Enviroment.ITERATIONS;
        int runs = Enviroment.RUNS;
        int populationSize = Enviroment.POPULATION_SIZE;

        Atlbo atlbo = new Atlbo(new PopulationGeneratorImpl(), new InteractionsGeneratorImpl(),
                new FuzzySystemImpl(), new AbsorbingWalls());

        ConsoleReporter consoleReporter = Reporter.getConsoleReporter();
        FileReporter fileReporter = Reporter.getFileReporter("Table4" + Enviroment.TXT);
        consoleReporter.setUsedHeader(Reporter.defaultHeader3);
        fileReporter.setUsedHeader(Reporter.defaultHeader3);

        consoleReporter.reportHeader();
        fileReporter.reportHeader();
        Table4.getAllForT(2).forEach(k -> {
            Execution e = Executor.execute(iterations, runs, populationSize, atlbo, k);
            consoleReporter.reportExecutionLikeTable4(e);
            fileReporter.reportExecutionLikeTable4(e);
        });

        consoleReporter.reportln("");
        fileReporter.reportln("");

        consoleReporter.reportHeader();
        fileReporter.reportHeader();
        Table4.getAllForT(3).forEach(k -> {
            Execution e = Executor.execute(iterations, runs, populationSize, atlbo, k);
            consoleReporter.reportExecutionLikeTable4(e);
            fileReporter.reportExecutionLikeTable4(e);
        });

        consoleReporter.reportln("");
        fileReporter.reportln("");

        consoleReporter.reportHeader();
        fileReporter.reportHeader();
        Table4.getAllForT(4).forEach(k -> {
            Execution e = Executor.execute(iterations, runs, populationSize, atlbo, k);
            consoleReporter.reportExecutionLikeTable4(e);
            fileReporter.reportExecutionLikeTable4(e);
        });

//        Table4.getAll().forEach((k, v) -> {
//            Map<String, Execution> executions = Executor.execute(iterations, runs, populationSize, atlbo, v);
//
//            consoleReporter.reportExecutionsLikeTable4(new ArrayList<>(executions.values()));
//            fileReporter.reportExecutionsLikeTable4(new ArrayList<>(executions.values()));
//        });
    }

}
