package atlbo;

import atlbo.configurations.Table7;
import fuzzy.FuzzySystemImpl;
import interaction.InteractionsGeneratorImpl;
import population.PopulationGeneratorImpl;
import report.ConsoleReporter;
import report.FileReporter;
import report.Reporter;
import run.Execution;
import run.Executor;

import java.io.FileNotFoundException;

public class RunTable7 {

    public static void main(String[] args) throws FileNotFoundException {
        int iterations = Enviroment.ITERATIONS;
        int runs = Enviroment.RUNS;
        int populationSize = Enviroment.POPULATION_SIZE;

        Atlbo atlbo = new Atlbo(new PopulationGeneratorImpl(), new InteractionsGeneratorImpl(),
                new FuzzySystemImpl(), new AbsorbingWalls());

        ConsoleReporter consoleReporter = Reporter.getConsoleReporter();
        FileReporter fileReporter = Reporter.getFileReporter("Table7" + Enviroment.TXT);
        consoleReporter.setUsedHeader(Reporter.defaultHeader1);
        fileReporter.setUsedHeader(Reporter.defaultHeader1);

        consoleReporter.reportHeader();
        fileReporter.reportHeader();

        Execution e;
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA1);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);
//
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA2);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);
//
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA3);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);
//
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA4);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);
//
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA5);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);
//
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA6);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);
//
//        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA7);
//        consoleReporter.reportExecution(e);
//        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA8);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA9);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA10);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table7.VCA11);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);
    }

}
