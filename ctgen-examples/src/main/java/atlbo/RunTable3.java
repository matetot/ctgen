package atlbo;

import atlbo.configurations.Table3;
import configuration.MainConfiguration;
import fuzzy.FuzzySystemImpl;
import interaction.InteractionsGeneratorImpl;
import population.PopulationGeneratorImpl;
import report.ConsoleReporter;
import report.FileReporter;
import report.Reporter;
import run.Execution;
import run.Executor;
import run.Run;
import run.Runner;

import java.io.FileNotFoundException;


public class RunTable3 {

    public static void main(String[] args) throws FileNotFoundException {
        int iterations = Enviroment.ITERATIONS;
        int runs = Enviroment.RUNS;
        int populationSize = Enviroment.POPULATION_SIZE;

        Atlbo atlbo = new Atlbo(new PopulationGeneratorImpl(), new InteractionsGeneratorImpl(),
                new FuzzySystemImpl(), new AbsorbingWalls());

        ConsoleReporter consoleReporter = Reporter.getConsoleReporter();
        FileReporter fileReporter = Reporter.getFileReporter("Table3" + Enviroment.TXT);
        consoleReporter.setUsedHeader(Reporter.defaultHeader1);
        fileReporter.setUsedHeader(Reporter.defaultHeader1);

        consoleReporter.reportHeader();
        fileReporter.reportHeader();

//        MainConfiguration mainConfiguration = Table3.CA1;
//        Run run = Runner.run(30, atlbo, mainConfiguration,
//                atlbo.getPopulationGenerator().spawn(mainConfiguration.getParameters(), 40),
//                atlbo.getInteractionsGenerator().generate(mainConfiguration));
//
//        consoleReporter.reportRun(run);
//        fileReporter.reportRun(run);

        Execution e;
        e = Executor.execute(10, 10, populationSize, atlbo, Table3.CA1);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table3.CA2);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table3.CA3);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table3.VCA1);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table3.VCA2);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);

        e = Executor.execute(iterations, runs, populationSize, atlbo, Table3.VCA3);
        consoleReporter.reportExecution(e);
        fileReporter.reportExecution(e);
    }

}
