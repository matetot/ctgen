package atlbo;

/**
 * Konstanty pro prostredi behu a reporotvani vysledku ATLBO algoritmu.
 *
 * @author Tomáš Matějka
 */
public final class Enviroment {

    /** Kolikrat ATLBO pobezi. Pro kazdy beh bude pouzita nove vygenerovana populace. X Runu tvori 'Execetuion'.  */
    public static final int RUNS = 10;

    /** Kolikrat ATLBO pobezi nad stejnou populaci. X Iteraci tvori 'Run'. */
    public static final int ITERATIONS = 1;

    /** Velikost generovane populace pro ATLBO algortimus. */
    public static final int POPULATION_SIZE = 40;

    /** Pripona souboru pro report vysledku. */
    public static final String TXT = ".txt";

}
