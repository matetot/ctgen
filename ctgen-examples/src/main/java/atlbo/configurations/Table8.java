package atlbo.configurations;

import configuration.MainConfiguration;
import configuration.Parameter;
import configuration.SubConfiguration;

import java.util.Arrays;
import java.util.List;

public class Table8 extends ConfigurationRepository {

    public static final MainConfiguration VCA1 = createVca1();

    public static final MainConfiguration VCA2 = createVca2();

    public static final MainConfiguration VCA3 = createVca3();

    public static final MainConfiguration VCA4 = createVca4();

    public static final MainConfiguration VCA5 = createVca5();

    public static final MainConfiguration VCA6 = createVca6();

    public static final MainConfiguration VCA7 = createVca7();

    public static List<MainConfiguration> getAll() {
        return Arrays.asList(VCA1, VCA2, VCA3, VCA4, VCA5, VCA6, VCA7);
    }

    private static MainConfiguration createVca1() {
        return createConfiguration("Table8.VCA1", "VCA1", 3, 15, 3);
    }

    private static MainConfiguration createVca2() {
        MainConfiguration vca = createConfiguration("Table8.VCA2", "VCA2", 3, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca3() {
        MainConfiguration vca = createConfiguration("Table8.VCA3", "VCA3", 3, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3));

        SubConfiguration subConfiguration2 = new SubConfiguration(4);
        subConfiguration2.addParameters(parameters.get(4), parameters.get(5), parameters.get(6), parameters.get(7));

        vca.addSubconfiguration(subConfiguration);
        vca.addSubconfiguration(subConfiguration2);

        return vca;
    }

    private static MainConfiguration createVca4() {
        MainConfiguration vca = createConfiguration("Table8.VCA4", "VCA4", 3, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3),
                parameters.get(4));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca5() {
        MainConfiguration vca = createConfiguration("Table8.VCA5", "VCA5", 3, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3),
                parameters.get(4), parameters.get(6));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca6() {
        MainConfiguration vca = createConfiguration("Table8.VCA6", "VCA6", 3, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3),
                parameters.get(4), parameters.get(5), parameters.get(6), parameters.get(7), parameters.get(8));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca7() {
        MainConfiguration vca = createConfiguration("Table8.VCA7", "VCA7", 3, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3),
                parameters.get(4), parameters.get(5), parameters.get(6), parameters.get(7), parameters.get(8), parameters.get(9), parameters.get(10));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

}
