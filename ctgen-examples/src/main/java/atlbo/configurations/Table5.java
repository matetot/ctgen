package atlbo.configurations;

import configuration.MainConfiguration;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Table5 extends ConfigurationRepository {

    private static final int p = 7;

    private static final Map<Integer, List<MainConfiguration>> configurations = new LinkedHashMap<>();

    static {
        configurations.put(2, new ArrayList<>());
        configurations.put(3, new ArrayList<>());
        configurations.put(4, new ArrayList<>());

        int caIndex = 1;
        for (int t = 2; t <= 4; t++) {
            for (int v = 2; v <= 5; v++) {
                configurations.get(t).add(createConfiguration("Table5.CA" + caIndex, "CA" + caIndex, t, p, v));
                caIndex += 1;
            }
        }
    }

    public static final MainConfiguration CA1 = get(2, 2);
    public static final MainConfiguration CA2 = get(2, 3);
    public static final MainConfiguration CA3 = get(2, 4);
    public static final MainConfiguration CA4 = get(2, 5);

    public static final MainConfiguration CA5 = get(3, 2);
    public static final MainConfiguration CA6 = get(3, 3);
    public static final MainConfiguration CA7 = get(3, 4);
    public static final MainConfiguration CA8 = get(3, 5);

    public static final MainConfiguration CA9 = get(4, 2);
    public static final MainConfiguration CA10 = get(4, 3);
    public static final MainConfiguration CA11 = get(4, 4);
    public static final MainConfiguration CA12 = get(4, 5);


    public static MainConfiguration get(int t, int v) {
        return configurations.get(t).stream().filter(k -> k.getParameters().get(0).getValues() == v).findFirst().orElse(null);
    }

    public static List<MainConfiguration> getAllForT(int t) {
        return configurations.get(t);
    }


}
