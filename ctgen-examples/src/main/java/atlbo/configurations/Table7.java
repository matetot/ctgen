package atlbo.configurations;

import configuration.MainConfiguration;
import configuration.Parameter;
import configuration.SubConfiguration;

import java.util.Arrays;
import java.util.List;

public class Table7 extends ConfigurationRepository {

    public static final MainConfiguration VCA1 = createVca1();

    public static final MainConfiguration VCA2 = createVca2();

    public static final MainConfiguration VCA3 = createVca3();

    public static final MainConfiguration VCA4 = createVca4();

    public static final MainConfiguration VCA5 = createVca5();

    public static final MainConfiguration VCA6 = createVca6();

    public static final MainConfiguration VCA7 = createVca7();

    public static final MainConfiguration VCA8 = createVca8();

    public static final MainConfiguration VCA9 = createVca9();

    public static final MainConfiguration VCA10 = createVca10();

    public static final MainConfiguration VCA11 = createVca11();


    public static List<MainConfiguration> getAll() {
        return Arrays.asList(VCA1, VCA2, VCA3, VCA4, VCA5, VCA6, VCA7, VCA8, VCA9, VCA10, VCA11);
    }

    private static MainConfiguration createVca1() {
        return createConfiguration("Table7.VCA1", "VCA1",2, 15, 3);
    }

    private static MainConfiguration createVca2() {
        MainConfiguration vca =createConfiguration("Table7.VCA2", "VCA2",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca3() {
        MainConfiguration vca = createConfiguration("Table7.VCA3", "VCA3",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2));

        SubConfiguration subConfiguration2 = new SubConfiguration(3);
        subConfiguration2.addParameters(parameters.get(3), parameters.get(4), parameters.get(5));

        vca.addSubconfiguration(subConfiguration);
        vca.addSubconfiguration(subConfiguration2);

        return vca;
    }

    private static MainConfiguration createVca4() {
        MainConfiguration vca = createConfiguration("Table7.VCA4", "VCA4",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2));

        SubConfiguration subConfiguration2 = new SubConfiguration(3);
        subConfiguration2.addParameters(parameters.get(3), parameters.get(4), parameters.get(5));

        SubConfiguration subConfiguration3 = new SubConfiguration(3);
        subConfiguration3.addParameters(parameters.get(6), parameters.get(7), parameters.get(8));

        vca.addSubconfiguration(subConfiguration);
        vca.addSubconfiguration(subConfiguration2);
        vca.addSubconfiguration(subConfiguration3);

        return vca;
    }

    private static MainConfiguration createVca5() {
        MainConfiguration vca = createConfiguration("Table7.VCA5", "VCA5",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca6() {
        MainConfiguration vca = createConfiguration("Table7.VCA6", "VCA6",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca7() {
        MainConfiguration vca = createConfiguration("Table7.VCA7", "VCA7",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();
        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1),
                parameters.get(2), parameters.get(3), parameters.get(4), parameters.get(5));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca8() {
        MainConfiguration vca = createConfiguration("Table7.VCA8", "VCA8",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();
        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1),
                parameters.get(2), parameters.get(3), parameters.get(4), parameters.get(5), parameters.get(6));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca9() {
        MainConfiguration vca = createConfiguration("Table7.VCA9", "VCA9",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();
        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1),
                parameters.get(2), parameters.get(3));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca10() {
        MainConfiguration vca = createConfiguration("Table7.VCA10", "VCA10",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();
        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1),
                parameters.get(2), parameters.get(3), parameters.get(4));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca11() {
        MainConfiguration vca = createConfiguration("Table7.VCA11", "VCA11",2, 15, 3);

        List<Parameter> parameters = vca.getParameters();
        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1),
                parameters.get(2), parameters.get(3), parameters.get(4), parameters.get(5), parameters.get(6));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

}
