package atlbo.configurations;

import configuration.MainConfiguration;

public class ConfigurationRepository {

    protected static MainConfiguration createConfiguration(String fullName, String shortName, int t, int p, int v) {
        MainConfiguration mainConfiguration = new MainConfiguration(fullName, shortName, t);

        for (int i = 0; i < p; i++) {
            mainConfiguration.addParameter(i, v);
        }

        return mainConfiguration;
    }

    protected static MainConfiguration addToConfiguration(MainConfiguration mainConfiguration, int p, int v) {
        int paramCount = mainConfiguration.getParameters().size();
        for (int i = paramCount; i < paramCount + p; i++) {
            mainConfiguration.addParameter(i, v);
        }
        return mainConfiguration;
    }

}
