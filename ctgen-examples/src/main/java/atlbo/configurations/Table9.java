package atlbo.configurations;

import configuration.MainConfiguration;
import configuration.Parameter;
import configuration.SubConfiguration;

import java.util.Arrays;
import java.util.List;

public class Table9 extends ConfigurationRepository {

    public static final MainConfiguration VCA1 = createVca1();

    public static final MainConfiguration VCA2 = createVca2();

    public static final MainConfiguration VCA3 = createVca3();

    public static final MainConfiguration VCA4 = createVca4();

    public static final MainConfiguration VCA5 = createVca5();

    public static final MainConfiguration VCA6 = createVca6();

    public static final MainConfiguration VCA7 = createVca7();

    public static List<MainConfiguration> getAll() {
        return Arrays.asList(VCA1, VCA2, VCA3, VCA4, VCA5, VCA6, VCA7);
    }

    private static MainConfiguration createVca1() {
        MainConfiguration vca = createConfiguration("Table9.VCA1", "VCA1", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        return vca;
    }

    private static MainConfiguration createVca2() {
        MainConfiguration vca = createConfiguration("Table9.VCA2", "VCA2", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca3() {
        MainConfiguration vca = createConfiguration("Table9.VCA3", "VCA3", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        List<Parameter> parameters = vca.getParameters();
        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2), parameters.get(3), parameters.get(4));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca4() {
        MainConfiguration vca = createConfiguration("Table9.VCA4", "VCA4", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2));


        SubConfiguration subConfiguration2 = new SubConfiguration(3);
        subConfiguration2.addParameters(parameters.get(3), parameters.get(4), parameters.get(5));

        vca.addSubconfiguration(subConfiguration);
        vca.addSubconfiguration(subConfiguration2);

        return vca;
    }

    private static MainConfiguration createVca5() {
        MainConfiguration vca = createConfiguration("Table9.VCA5", "VCA5", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2),
                parameters.get(3), parameters.get(4), parameters.get(5),
                parameters.get(6));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

    private static MainConfiguration createVca6() {
        MainConfiguration vca = createConfiguration("Table9.VCA6", "VCA6", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2));


        SubConfiguration subConfiguration2 = new SubConfiguration(4);
        subConfiguration2.addParameters(parameters.get(3), parameters.get(4), parameters.get(5), parameters.get(6));

        vca.addSubconfiguration(subConfiguration);
        vca.addSubconfiguration(subConfiguration2);

        return vca;
    }

    private static MainConfiguration createVca7() {
        MainConfiguration vca = createConfiguration("Table9.VCA7", "VCA7", 2, 3, 4);
        addToConfiguration(vca, 3, 5);
        addToConfiguration(vca, 2, 6);

        List<Parameter> parameters = vca.getParameters();

        SubConfiguration subConfiguration = new SubConfiguration(4);
        subConfiguration.addParameters(parameters.get(0), parameters.get(1), parameters.get(2),
                parameters.get(3), parameters.get(4));

        vca.addSubconfiguration(subConfiguration);

        return vca;
    }

}
