package atlbo.configurations;

import configuration.MainConfiguration;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Table4 extends ConfigurationRepository {

    private static final int v = 3;

    private static final Map<Integer, List<MainConfiguration>> configurations = new LinkedHashMap<>();

    static {
        configurations.put(2, new ArrayList<>());
        configurations.put(3, new ArrayList<>());
        configurations.put(4, new ArrayList<>());

        int caIndex = 1;
        for (int t = 2; t <= 4; t++) {
            for (int p = 2 + t; p <= 12; p++) {
                configurations.get(t).add(createConfiguration("Table4.CA" + caIndex, "CA" + caIndex, t, p, v));
                caIndex += 1;
            }
        }
    }

    public static Map<Integer, List<MainConfiguration>> getAll() {
        return configurations;
    }

    public static final MainConfiguration CA1 = get(2, 4);
    public static final MainConfiguration CA2 = get(2, 5);
    public static final MainConfiguration CA3 = get(2, 6);
    public static final MainConfiguration CA4 = get(2, 7);
    public static final MainConfiguration CA5 = get(2, 8);
    public static final MainConfiguration CA6 = get(2, 9);
    public static final MainConfiguration CA7 = get(2, 10);
    public static final MainConfiguration CA8 = get(2, 11);
    public static final MainConfiguration CA9 = get(2, 12);

    public static final MainConfiguration CA10 = get(3, 5);
    public static final MainConfiguration CA11 = get(3, 6);
    public static final MainConfiguration CA12 = get(3, 7);
    public static final MainConfiguration CA13 = get(3, 8);
    public static final MainConfiguration CA14 = get(3, 9);
    public static final MainConfiguration CA15 = get(3, 10);
    public static final MainConfiguration CA16 = get(3, 11);
    public static final MainConfiguration CA17 = get(3, 12);

    public static final MainConfiguration CA18 = get(3, 6);
    public static final MainConfiguration CA19 = get(3, 7);
    public static final MainConfiguration CA20 = get(3, 8);
    public static final MainConfiguration CA21 = get(3, 9);
    public static final MainConfiguration CA22 = get(3, 10);
    public static final MainConfiguration CA23 = get(3, 11);
    public static final MainConfiguration CA24 = get(3, 12);

    public static MainConfiguration get(int t, int p) {
        return configurations.get(t).stream().filter(k -> k.getParameters().size() == p).findFirst().orElse(null);
    }

    public static List<MainConfiguration> getAllForT(int t) {
        return configurations.get(t);
    }

}
