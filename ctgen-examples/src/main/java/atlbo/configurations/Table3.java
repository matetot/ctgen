package atlbo.configurations;

import configuration.MainConfiguration;
import configuration.Parameter;
import configuration.SubConfiguration;

import java.util.Arrays;
import java.util.List;

public class Table3 extends ConfigurationRepository {

    public static final MainConfiguration CA1 = ca1();

    public static final MainConfiguration CA2 = ca2();

    public static final MainConfiguration CA3 = ca3();

    public static final MainConfiguration VCA1 = vca1();

    public static final MainConfiguration VCA2 = vca2();

    public static final MainConfiguration VCA3 = vca3();

    public static List<MainConfiguration> getAll() {
        return Arrays.asList(CA1, CA2, CA3, VCA1, VCA2, VCA3);
    }

    private static MainConfiguration ca1() {
        return createConfiguration("Table3.CA1", "CA1", 2, 5, 10);
    }

    private static MainConfiguration ca2() {
        MainConfiguration mainConfiguration = createConfiguration("Table3.CA2", "CA2", 2, 2, 4);
        return addToConfiguration(mainConfiguration, 5, 5);
    }

    private static MainConfiguration ca3() {
        MainConfiguration mainConfiguration = createConfiguration("Table3.CA3", "CA3", 2, 3, 2);
        return addToConfiguration(mainConfiguration, 5, 3);
    }

    private static MainConfiguration vca1() {
        MainConfiguration vca1 = new MainConfiguration("Table3.VCA1", "VCA1", 2);
        vca1.addParameter(0, 5);
        vca1.addParameter(1, 5);

        Parameter p3 = vca1.addParameter(2, 4);
        Parameter p4 = vca1.addParameter(3, 4);

        Parameter p5 = vca1.addParameter(4, 3);
        Parameter p6 = vca1.addParameter(5, 3);

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(p3, p4, p5, p6);

        vca1.addSubconfiguration(subConfiguration);

        return vca1;
    }

    private static MainConfiguration vca2() {
        MainConfiguration vca2 = new MainConfiguration("Table3.VCA2", "VCA2", 2);
        Parameter p1 = vca2.addParameter(0, 5);
        Parameter p2 = vca2.addParameter(1, 5);
        Parameter p3 = vca2.addParameter(2, 5);

        vca2.addParameter(3, 5);
        vca2.addParameter(4, 5);
        vca2.addParameter(5, 5);
        vca2.addParameter(6, 5);

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(p1, p2, p3);

        vca2.addSubconfiguration(subConfiguration);

        return vca2;
    }

    private static MainConfiguration vca3() {
        MainConfiguration vca3 = new MainConfiguration("Table3.VCA3", "VCA3", 2);
        Parameter p1 = vca3.addParameter(0, 3);
        Parameter p2 = vca3.addParameter(1, 3);
        Parameter p3 = vca3.addParameter(2, 3);

        vca3.addParameter(3, 3);
        vca3.addParameter(4, 3);
        vca3.addParameter(5, 3);
        vca3.addParameter(6, 3);
        vca3.addParameter(7, 3);
        vca3.addParameter(8, 3);
        vca3.addParameter(9, 3);
        vca3.addParameter(10, 3);
        vca3.addParameter(11, 3);
        vca3.addParameter(12, 3);

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(p1, p2, p3);

        vca3.addSubconfiguration(subConfiguration);

        return vca3;
    }

}
