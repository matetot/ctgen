package atlbo;

import atlbo.configurations.Table5;
import fuzzy.FuzzySystemImpl;
import interaction.InteractionsGeneratorImpl;
import population.PopulationGeneratorImpl;
import report.ConsoleReporter;
import report.FileReporter;
import report.Reporter;
import run.Execution;
import run.Executor;

import java.io.FileNotFoundException;

public class RunTable5 {

    public static void main(String[] args) throws FileNotFoundException {
        int iterations = Enviroment.ITERATIONS;
        int runs = Enviroment.RUNS;
        int populationSize = Enviroment.POPULATION_SIZE;

        Atlbo atlbo = new Atlbo(new PopulationGeneratorImpl(), new InteractionsGeneratorImpl(),
                new FuzzySystemImpl(), new AbsorbingWalls());

        ConsoleReporter consoleReporter = Reporter.getConsoleReporter();
        FileReporter fileReporter = Reporter.getFileReporter("Table5" + Enviroment.TXT);
        consoleReporter.setUsedHeader(Reporter.defaultHeader2);
        fileReporter.setUsedHeader(Reporter.defaultHeader2);

        consoleReporter.reportHeader();
        fileReporter.reportHeader();
        Table5.getAllForT(2).forEach(k -> {
            Execution e = Executor.execute(iterations, runs, populationSize, atlbo, k);
            consoleReporter.reportExecutionLikeTable5(e);
            fileReporter.reportExecutionLikeTable5(e);
        });

        consoleReporter.reportHeader();
        fileReporter.reportHeader();
        Table5.getAllForT(3).forEach(k -> {
            Execution e = Executor.execute(iterations, runs, populationSize, atlbo, k);
            consoleReporter.reportExecutionLikeTable5(e);
            fileReporter.reportExecutionLikeTable5(e);
        });

        consoleReporter.reportHeader();
        fileReporter.reportHeader();
        Table5.getAllForT(4).forEach(k -> {
            Execution e = Executor.execute(iterations, runs, populationSize, atlbo, k);
            consoleReporter.reportExecutionLikeTable5(e);
            fileReporter.reportExecutionLikeTable5(e);
        });
    }

}
