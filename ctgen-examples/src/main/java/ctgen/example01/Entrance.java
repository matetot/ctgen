package ctgen.example01;

public class Entrance {

    public static int someMethod(String code, int value) {
        System.out.println("--- SomeMethod ---");
        System.out.println("Code: " + code);
        System.out.println("Value: " + value);

        if (code.equals("one")) {
            if (value == 10) {
                return 0;
            }
            if (value == 20) {
                return 1;
            }
        }

        if (code.equals("two")) {
            if (value == 10) {
                return 2;
            }
            if (value == 20) {
                return 3;
            }
        }

        return 4;
    }

    public boolean canEnter(Person person, int limitedAge, double limitedHeight) {
        System.out.println("--- CanEnter ---");
        System.out.println("Person age: " + person.getAge());
        System.out.println("Person height: " + person.getHeight());
        System.out.println("Person value value: " + person.getPersonValue().getValue());
        System.out.println("Person value gender: " + person.getPersonValue().getGender());

        System.out.println("Limited age: " + limitedAge);
        System.out.println("Limited height: " + limitedHeight);

        if (person.getPersonValue().getGender() == Gender.MALE) {
            if (person.getHeight() < limitedHeight) {
                return false;
            }

            if (person.getAge() < limitedAge) {
                return false;
            }
        } else {
            if (person.getHeight() < limitedHeight) {
                return false;
            }

            if (person.getAge() < limitedAge) {
                return false;
            }
        }

        return true;
    }
}
