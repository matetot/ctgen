package ctgen.example01;

public class Person {

    private int age;

    private double height;

    private PersonValue personValue;

    public PersonValue getPersonValue() {
        return personValue;
    }

    public void setPersonValue(PersonValue personValue) {
        this.personValue = personValue;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

}
