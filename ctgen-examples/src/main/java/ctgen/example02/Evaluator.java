package ctgen.example02;

public class Evaluator {

    public String evaluate(int degree, int children, boolean read, boolean write,
                           boolean speak, boolean understand, boolean newGrad, boolean experience,
                           boolean english, boolean disability, int maritalStatus, int resident) {
        String result = "";

        // t = 6 ============================================================================================
        if (degree == Degree.DOCTOR && children == Children.MORE_THAN_FOUR && maritalStatus == MaritalStatus.MARRIED && disability && !english && !newGrad) {
            result += "SIX_params_interaction";
            // f-2
        }

        if (degree == Degree.DIPLOMA && children == Children.THREE && !experience && !newGrad && resident == Resident.FOREIGNER && !understand) {
            result += "SIX_params_interaction";
            // f-1
        }

        if (degree == Degree.PRIMARY && children == Children.NONE && !read && !write && resident == Resident.FOREIGNER && understand) {
            result += "SIX_params_interaction";
            // f0
        }

        if (degree == Degree.MASTER && children == Children.ONE && read && !write && speak && !understand) {
            result += "SIX_params_interaction";
            // f1
        }

        if (resident == Resident.LOCAL && disability && !read && write && speak && understand) {
            result += "SIX_params_interaction";
            // f2
        }

        if (newGrad && disability && children == Children.FOUR && experience && !speak && english) {
            result += "SIX_params_interaction";
            // f3
        }

        if (newGrad && disability && children == Children.TWO && !write && speak && !experience) {
            result += "SIX_params_interaction";
            // f4
        }
        // ==================================================================================================

        // t = 5 ============================================================================================
        if (degree == Degree.NO_DEGREE && children == Children.MORE_THAN_FOUR && understand && !experience && speak) {
            result += "FIVE_params_interaction";
            // f5
        }

        if (degree == Degree.BACHELOR && read && maritalStatus == MaritalStatus.WIDOW && write && speak) {
            result += "FIVE_params_interaction";
            // f6
        }

        if (experience && read && maritalStatus == MaritalStatus.WIDOW && write && speak) {
            result += "FIVE_params_interaction";
            // f7
        }

        if (children == Children.TWO && resident == Resident.LOCAL && maritalStatus == MaritalStatus.WIDOW && !disability && english) {
            result += "FIVE_params_interaction";
            // f8
        }
        // ==================================================================================================

        // t = 4 ============================================================================================
        if (speak && write && experience && english) {
            result += "FOUR_params_interaction";
            // f9
        }

        if (children == Children.ONE && disability && maritalStatus == MaritalStatus.MARRIED && newGrad) {
            result += "FOUR_params_interaction";
            // f10
        }

        if (!understand && !write && !experience && english) {
            result += "FOUR_params_interaction";
            // f11
        }

        if (!english && !disability && maritalStatus == MaritalStatus.SINGLE && !newGrad) {
            result += "FOUR_params_interaction";
            // f12
        }
        // ==================================================================================================

        // t = 3 ============================================================================================
        if (!newGrad && write && !experience && read) {
            result += "THREE_params_interaction";
            // f13
        }

        if (children == Children.MORE_THAN_FOUR && write && !speak && resident == Resident.OUTSIDER) {
            result += "THREE_params_interaction";
            // f14
        }

        if (degree == Degree.SECONDARY && !write && experience && resident == Resident.LOCAL) {
            result += "THREE_params_interaction";
            // f15
        }

        if (!english && understand && speak && !read) {
            result += "THREE_params_interaction";
            // f16
        }
        // ==================================================================================================

        return result;
    }

}
