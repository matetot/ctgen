package ctgen.example02;

public class Children {

    public static int NONE = 0;

    public static int ONE = 1;

    public static int TWO = 2;

    public static int THREE = 3;

    public static int FOUR = 4;

    public static int MORE_THAN_FOUR = 5;

}
