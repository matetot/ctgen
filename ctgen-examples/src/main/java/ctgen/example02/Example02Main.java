package ctgen.example02;

import atlbo.AbsorbingWalls;
import atlbo.Atlbo;
import codeformat.GoogleJavaFormatter;
import ctgen.CTGen;
import ctgen.UnsupportedFileFormatException;
import fuzzy.FuzzySystemImpl;
import interaction.InteractionsGeneratorImpl;
import population.PopulationGeneratorImpl;
import transformation.JavaTransformer;

public class Example02Main {

    public static void main(String[] args) throws UnsupportedFileFormatException {
        Atlbo atlbo = new Atlbo(new PopulationGeneratorImpl(), new InteractionsGeneratorImpl(),
                new FuzzySystemImpl(), new AbsorbingWalls());

        CTGen.fromFile("example02.xml").
                atlbo(atlbo).
                transformer(new JavaTransformer()).
                codeFormatter(new GoogleJavaFormatter()).
                populationSize(40).
                outputFolder("generated").
                filesExtension("java").
                compileAndRunImmediately(false).
                go();
    }

}
