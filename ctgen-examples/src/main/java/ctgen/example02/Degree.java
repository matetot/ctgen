package ctgen.example02;

public class Degree {

    public static int NO_DEGREE = 0;

    public static int PRIMARY = 1;

    public static int SECONDARY = 2;

    public static int DIPLOMA = 3;

    public static int BACHELOR = 4;

    public static int MASTER = 5;

    public static int DOCTOR = 6;
}
