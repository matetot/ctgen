import ctgen.example01.TestObjectEasy;
import org.junit.jupiter.api.Test;
import ctgen.example01.Person;
import ctgen.example01.PersonValue;
import ctgen.example01.Gender;

public class TestObjectEasyTest {
  @Test
  public void metoda4TestCase1() {
    TestObjectEasy testObjectEasy = new TestObjectEasy();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.MALE);
    personValue.setValue(90);
    person.setPersonValue(personValue);
    person.setAge(11);
    person.setHeight(12.23);
    int limitedAge = 12;
    testObjectEasy.metoda4(person, limitedAge);
  }

  @Test
  public void metoda4TestCase2() {
    TestObjectEasy testObjectEasy = new TestObjectEasy();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.FEMALE);
    personValue.setValue(90);
    person.setPersonValue(personValue);
    person.setAge(10);
    person.setHeight(10.23);
    int limitedAge = 9;
    testObjectEasy.metoda4(person, limitedAge);
  }

  @Test
  public void metoda4TestCase3() {
    TestObjectEasy testObjectEasy = new TestObjectEasy();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.MALE);
    personValue.setValue(100);
    person.setPersonValue(personValue);
    person.setAge(39);
    person.setHeight(10.23);
    int limitedAge = 12;
    testObjectEasy.metoda4(person, limitedAge);
  }

  @Test
  public void metoda4TestCase4() {
    TestObjectEasy testObjectEasy = new TestObjectEasy();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.FEMALE);
    personValue.setValue(100);
    person.setPersonValue(personValue);
    person.setAge(39);
    person.setHeight(12.23);
    int limitedAge = 9;
    testObjectEasy.metoda4(person, limitedAge);
  }

  @Test
  public void metoda4TestCase5() {
    TestObjectEasy testObjectEasy = new TestObjectEasy();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.MALE);
    personValue.setValue(100);
    person.setPersonValue(personValue);
    person.setAge(11);
    person.setHeight(10.23);
    int limitedAge = 9;
    testObjectEasy.metoda4(person, limitedAge);
  }

  @Test
  public void metoda4TestCase6() {
    TestObjectEasy testObjectEasy = new TestObjectEasy();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.MALE);
    personValue.setValue(100);
    person.setPersonValue(personValue);
    person.setAge(10);
    person.setHeight(12.23);
    int limitedAge = 12;
    testObjectEasy.metoda4(person, limitedAge);
  }
}
