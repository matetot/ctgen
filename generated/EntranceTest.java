import ctgen.example01.Entrance;
import org.junit.jupiter.api.Test;
import ctgen.example01.Person;
import ctgen.example01.PersonValue;
import ctgen.example01.Gender;

public class EntranceTest {
  @Test
  public void someMethodTestCase1() {
    String code = "two";
    int value = 20;
    Entrance.someMethod(code, value);
  }

  @Test
  public void someMethodTestCase2() {
    String code = "two";
    int value = 10;
    Entrance.someMethod(code, value);
  }

  @Test
  public void someMethodTestCase3() {
    String code = "one";
    int value = 10;
    Entrance.someMethod(code, value);
  }

  @Test
  public void canEnterTestCase1() {
    Entrance entrance = new Entrance();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.FEMALE);
    personValue.setValue(90);
    person.setPersonValue(personValue);
    person.setAge(11);
    person.setHeight(12.23);
    int limitedAge = 9;
    double limitedHeight = 12.93;
    entrance.canEnter(person, limitedAge, limitedHeight);
  }

  @Test
  public void canEnterTestCase2() {
    Entrance entrance = new Entrance();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.MALE);
    personValue.setValue(100);
    person.setPersonValue(personValue);
    person.setAge(8);
    person.setHeight(12.23);
    int limitedAge = 12;
    double limitedHeight = 9.45;
    entrance.canEnter(person, limitedAge, limitedHeight);
  }

  @Test
  public void canEnterTestCase3() {
    Entrance entrance = new Entrance();

    Person person = new Person();
    PersonValue personValue = new PersonValue();
    personValue.setGender(Gender.FEMALE);
    personValue.setValue(100);
    person.setPersonValue(personValue);
    person.setAge(39);
    person.setHeight(10.23);
    int limitedAge = 12;
    double limitedHeight = 12.93;
    entrance.canEnter(person, limitedAge, limitedHeight);
  }
}
