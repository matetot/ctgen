package analysis;

/**
 * Output from the analysis of outpuf of {@link atlbo.Atlbo} algorithm.
 *
 * @author Tomáš Matějka
 */
public class AnalyzerOutput {

    /** Mean size of the final test suite of analyzed {@link atlbo.AtlboOutput}s. */
    private double meanSize;

    /** Smallest size of the final test suite of analyzed {@link atlbo.AtlboOutput}s. */
    private double bestSize;

    /** Mean exploitation percentage of analyzed {@link atlbo.AtlboOutput}s. */
    private double meanExploitPercentage;

    /** Mean exploration percentage of analyzed {@link atlbo.AtlboOutput}s. */
    private double meanExplorePercentage;

    /** Mean exploitation of analyzed {@link atlbo.AtlboOutput}s. */
    private double meanExploit;

    /** Mean exploration of analyzed {@link atlbo.AtlboOutput}s. */
    private double meanExplore;

    /**
     * Constructor.
     * Creates new instance for given values.
     *
     * @param meanSize mean size of the final test suite of analyzed {@link atlbo.AtlboOutput}s.
     * @param bestSize smallest size of the final test suite of analyzed {@link atlbo.AtlboOutput}s
     * @param meanExploitPercentage mean exploitation percentage of analyzed {@link atlbo.AtlboOutput}s
     * @param meanExplorePercentage mean exploration percentage of analyzed {@link atlbo.AtlboOutput}s
     * @param meanExploit mean exploitation of analyzed {@link atlbo.AtlboOutput}s
     * @param meanExplore mean exploration of analyzed {@link atlbo.AtlboOutput}s
     */
    public AnalyzerOutput(double meanSize, double bestSize, double meanExploitPercentage,
                          double meanExplorePercentage, double meanExploit, double meanExplore) {
        this.meanSize = meanSize;
        this.bestSize = bestSize;
        this.meanExploitPercentage = meanExploitPercentage;
        this.meanExplorePercentage = meanExplorePercentage;
        this.meanExploit = meanExploit;
        this.meanExplore = meanExplore;
    }

    public double getMeanSize() {
        return meanSize;
    }

    public double getBestSize() {
        return bestSize;
    }

    public double getMeanExploitPercentage() {
        return meanExploitPercentage;
    }

    public double getMeanExplorePercentage() {
        return meanExplorePercentage;
    }

    public double getMeanExploit() {
        return meanExploit;
    }

    public double getMeanExplore() {
        return meanExplore;
    }

    /**
     * Creates new instance of {@link Builder} that allows creation of the new AnalyzerOutput instance.
     *
     * @return new instance of builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder that creates new instance of {@link AnalyzerOutput}.
     *
     * @author Tomáš Matějka
     */
    public static class Builder {

        /** Mean size of the final test suite of analyzed {@link atlbo.AtlboOutput}s. */
        private double meanSize;

        /** Smallest size of the final test suite of analyzed {@link atlbo.AtlboOutput}s. */
        private double bestSize;

        /** Mean exploitation percentage of analyzed {@link atlbo.AtlboOutput}s. */
        private double meanExploitPercentage;

        /** Mean exploration percentage of analyzed {@link atlbo.AtlboOutput}s. */
        private double meanExplorePercentage;

        /** Mean exploitation of analyzed {@link atlbo.AtlboOutput}s. */
        private double meanExploit;

        /*** Mean exploration of analyzed {@link atlbo.AtlboOutput}s. */
        private double meanExplore;

        /**
         * Sets the mean size of the final test suite of analyzed {@link atlbo.AtlboOutput}s and returns instance of builder for future use.
         *
         * @param meanSize mean size of the final test suite of analyzed {@link atlbo.AtlboOutput}s
         * @return instance of builder for future use
         */
        public Builder meanSize(double meanSize) {
            this.meanSize = meanSize;
            return this;
        }

        /**
         * Sets the smallest size of the final test suite of analyzed {@link atlbo.AtlboOutput}s and returns instance of builder for future use.
         *
         * @param bestSize smallest size of the final test suite of analyzed {@link atlbo.AtlboOutput}s
         * @return instance of builder for future use
         */
        public Builder bestSize(double bestSize) {
            this.bestSize = bestSize;
            return this;
        }

        /**
         * Sets the mean exploitation percentage of analyzed {@link atlbo.AtlboOutput}s and returns instance of builder for future use.
         *
         * @param meanExploitPercentage mean exploitation percentage of analyzed {@link atlbo.AtlboOutput}s
         * @return instance of builder for future use
         */
        public Builder meanExploitPercentage(double meanExploitPercentage) {
            this.meanExploitPercentage = meanExploitPercentage;
            return this;
        }

        /**
         * Sets the mean exploration percentage of analyzed {@link atlbo.AtlboOutput}s and returns instance of builder for future use.
         *
         * @param meanExplorePercentage mean exploration percentage of analyzed {@link atlbo.AtlboOutput}s
         * @return instance of builder for future use.
         */
        public Builder meanExplorePercentage(double meanExplorePercentage) {
            this.meanExplorePercentage = meanExplorePercentage;
            return this;
        }

        /**
         * Sets the mean exploitation of analyzed {@link atlbo.AtlboOutput}s and returns instance of builder for future use.
         *
         * @param meanExploit mean exploitation of analyzed {@link atlbo.AtlboOutput}s
         * @return instance of builder for future use.
         */
        public Builder meanExploit(double meanExploit) {
            this.meanExploit = meanExploit;
            return this;
        }

        /**
         * Sets the mean exploration of analyzed {@link atlbo.AtlboOutput}s and returns instance of builder for future use.
         *
         * @param meanExplore mean exploration of analyzed {@link atlbo.AtlboOutput}s
         * @return instance of builder for future use.
         */
        public Builder meanExplore(double meanExplore) {
            this.meanExplore = meanExplore;
            return this;
        }

        /**
         * Finishes the building process and creates new instance of {@link AnalyzerOutput}.
         *
         * @return newly created AnalyzerOutput instance
         */
        public AnalyzerOutput build() {
            return new AnalyzerOutput(meanSize, bestSize, meanExploitPercentage, meanExplorePercentage, meanExploit, meanExplore);
        }

    }

}
