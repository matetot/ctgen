package analysis;

import atlbo.AtlboOutput;
import run.Execution;
import run.Run;

import java.util.List;

/**
 * Analyzes output of {@link atlbo.Atlbo} algorithm, output of {@link run.Runner} and output of {@link run.Executor}.
 *
 * @author Tomáš Matějka
 * @see AtlboOutput
 * @see Run
 * @see Execution
 */
public class Analyzer {

    /**
     * Analyzes one {@link Run}.
     *
     * @param run Run with list of {@link AtlboOutput}s for analysis
     * @return information from analyzed data
     */
    public AnalyzerOutput analyze(Run run) {
        return AnalyzerOutput.builder().
                meanSize(meanSize(run)).
                bestSize(bestSize(run)).
                meanExploit(meanExploit(run)).
                meanExplore(meanExplore(run)).
                meanExploitPercentage(meanExploitPercentage(run)).
                meanExplorePercentage(meanExplorePercentage(run)).
                build();
    }

    /**
     * Analyzes one {@link Execution}.
     *
     * @param execution Execution with list of {@link Run}s for analysis
     * @return information from analyzed data
     */
    public AnalyzerOutput analyze(Execution execution) {
        double sampleSize = 0;
        double meanSize = 0;
        double bestSize = Double.MAX_VALUE;
        double meanExploit = 0;
        double meanExplore = 0;
        double meanExploitPercentage = 0;
        double meanExplorePercentage = 0;
        for (Run run : execution.getRuns()) {
            AnalyzerOutput analyzerOutput = analyze(run);

            sampleSize += 1;
            meanSize += analyzerOutput.getMeanSize();
            bestSize = analyzerOutput.getBestSize() < bestSize ? analyzerOutput.getBestSize() : bestSize;
            meanExploit += analyzerOutput.getMeanExploit();
            meanExplore += analyzerOutput.getMeanExplore();
            meanExploitPercentage += analyzerOutput.getMeanExploitPercentage();
            meanExplorePercentage += analyzerOutput.getMeanExplorePercentage();
        }

        return AnalyzerOutput.builder().
                meanSize(meanSize / sampleSize).
                bestSize(bestSize).
                meanExploit(meanExploit / sampleSize).
                meanExplore(meanExplore / sampleSize).
                meanExploitPercentage(meanExploitPercentage / sampleSize).
                meanExplorePercentage(meanExplorePercentage / sampleSize).
                build();
    }

    /**
     * Calculates mean size of the final test suite from all {@link AtlboOutput}s in given Run.
     *
     * @param run Run with list of ATLBO outputs
     * @return mean size of final test suites
     */
    public double meanSize(Run run) {
        return meanSize(run.getAtlboOutputs());
    }

    /**
     * Finds the smallest size of the final test suite from all {@link AtlboOutput}s in given Run.
     *
     * @param run Run with list of ATLBO outputs
     * @return smallest final test suite size
     */
    public double bestSize(Run run) {
        return bestSize(run.getAtlboOutputs());
    }

    /**
     * Calculates mean exploitation percentage from all {@link AtlboOutput}s in given Run.
     *
     * @param run Run with list of ATLBO outputs
     * @return mean exploitation percentage
     */
    public double meanExploitPercentage(Run run) {
        return meanExploitPercentage(run.getAtlboOutputs());
    }

    /**
     * Calculates mean exploration percentage from all {@link AtlboOutput}s in given Run.
     *
     * @param run Run with list of ATLBO outputs
     * @return mean exploration percentage
     */
    public double meanExplorePercentage(Run run) {
        return meanExplorePercentage(run.getAtlboOutputs());
    }

    /**
     * Calculates mean exploration from all {@link AtlboOutput}s in given Run.
     *
     * @param run Run with list of ATLBO outputs
     * @return mean exploitation
     */
    public double meanExploit(Run run) {
        return meanExploit(run.getAtlboOutputs());
    }

    /**
     * Calculates mean exploration from all {@link AtlboOutput}s in given Run.
     *
     * @param run Run with list of ATLBO outputs
     * @return mean exploration
     */
    public double meanExplore(Run run) {
        return meanExplore(run.getAtlboOutputs());
    }

    /**
     * Calculates mean size of the final test suite from given {@link AtlboOutput}s.
     *
     * @param atlboOutputs ATLBO outputs containing final test suites
     * @return mean size of final test suites
     */
    public double meanSize(List<AtlboOutput> atlboOutputs) {
        double sum = 0;
        for (AtlboOutput atlboOutput : atlboOutputs) {
            sum += atlboOutput.getFinalTestSuite().size();
        }
        return sum / atlboOutputs.size();
    }

    /**
     * Finds the smallest size of the final test suite from given {@link AtlboOutput}s.
     *
     * @param atlboOutputs ATLBO outputs containing final test suites
     * @return smallest final test suite size
     */
    public int bestSize(List<AtlboOutput> atlboOutputs) {
        int best = Integer.MAX_VALUE;
        for (AtlboOutput atlboOutput : atlboOutputs) {
            if (atlboOutput.getFinalTestSuite().size() < best) {
                best = atlboOutput.getFinalTestSuite().size();
            }
        }
        return best;
    }

    /**
     * Calculates percentage of local search in given {@link AtlboOutput}.
     *
     * @param atlboOutput ATLBO output with exploitation and exploration values
     * @return local search percentage
     */
    public double exploitPercentage(AtlboOutput atlboOutput) {
        double hundredPercent = atlboOutput.getStatistics().getExploit() + atlboOutput.getStatistics().getExplore();
        return (atlboOutput.getStatistics().getExploit() / hundredPercent) * 100.0;
    }

    /**
     * Calculates percentage of global search in given {@link AtlboOutput}.
     *
     * @param atlboOutput ATLBO output with exploitation and exploration values
     * @return global search percentage
     */
    public double explorePercentage(AtlboOutput atlboOutput) {
        double hundredPercent = atlboOutput.getStatistics().getExploit() + atlboOutput.getStatistics().getExplore();
        return (atlboOutput.getStatistics().getExplore() / hundredPercent) * 100.0;
    }

    /**
     * Calculates mean percentage of local search in given {@link AtlboOutput}s.
     *
     * @param atlboOutputs ATLBO outputs containing exploitation and exploration values
     * @return mean percentage of local search
     */
    public double meanExploitPercentage(List<AtlboOutput> atlboOutputs) {
        double sum = 0;
        for (AtlboOutput atlboOutput : atlboOutputs) {
            sum += exploitPercentage(atlboOutput);
        }

        return sum / atlboOutputs.size();
    }

    /**
     * Calculates mean percentage of global search in given {@link AtlboOutput}s.
     *
     * @param atlboOutputs ATLBO outputs containing exploitation and exploration values
     * @return mean percentage of global search
     */
    public double meanExplorePercentage(List<AtlboOutput> atlboOutputs) {
        double sum = 0;
        for (AtlboOutput atlboOutput : atlboOutputs) {
            sum += explorePercentage(atlboOutput);
        }
        return sum / atlboOutputs.size();
    }

    /**
     * Calculates mean exploitation in given {@link AtlboOutput}s.
     *
     * @param atlboOutputs ATLBO outputs containing exploitation values
     * @return mean exploitation
     */
    public double meanExploit(List<AtlboOutput> atlboOutputs) {
        double sum = 0;
        for (AtlboOutput atlboOutput : atlboOutputs) {
            sum += atlboOutput.getStatistics().getExploit();
        }

        return sum / atlboOutputs.size();
    }

    /**
     * Calculates mean exploration in given {@link AtlboOutput}s.
     *
     * @param atlboOutputs ATLBO outputs containing exploration values
     * @return mean exploration
     */
    public double meanExplore(List<AtlboOutput> atlboOutputs) {
        double sum = 0;
        for (AtlboOutput atlboOutput : atlboOutputs) {
            sum += atlboOutput.getStatistics().getExplore();
        }

        return sum / atlboOutputs.size();
    }

}
