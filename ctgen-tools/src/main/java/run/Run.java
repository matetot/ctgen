package run;

import atlbo.Atlbo;
import atlbo.AtlboOutput;
import atlbo.ClampingRule;
import configuration.MainConfiguration;
import fuzzy.FuzzySystem;
import interaction.Interactions;
import population.Population;

import java.util.ArrayList;
import java.util.List;

/**
 * Class wrapping the outputs of multiple iterations on the same {@link Population} of {@link Atlbo} algorithm
 * and parameters ATLBO was run with.
 * Output of {@link Runner}.
 *
 * @author Tomáš Matějka
 */
public class Run {

    /** How many times {@link Atlbo} was run. */
    private final int iterations;

    /** Concrete {@link Atlbo} that was run. */
    private final Atlbo atlbo;

    /** Configuration used when running {@link Atlbo}. */
    private final MainConfiguration mainConfiguration;

    /** Population used for every {@link Atlbo} iteration of this Run. */
    private final Population population;

    /** Interactions used for every {@link Atlbo} iteration of this Run. */
    private final Interactions interactions;

    /** Fuzzy inference system used when running {@link Atlbo}. */
    private final FuzzySystem fuzzySystem;

    /** Clamping rule used when running {@link Atlbo}. */
    private final ClampingRule clampingRule;

    /** List of {@link AtlboOutput}s gathered from running {@link Atlbo} algorithm {@link #iterations}-times. */
    private final List<AtlboOutput> atlboOutputs;

    /**
     * Constructor.
     * Creates new instance for given values.
     *
     * @param iterations {@link #iterations}
     * @param atlbo {@link #atlbo}
     * @param mainConfiguration {@link #mainConfiguration}
     * @param population {@link #population}
     * @param interactions {@link #interactions}
     * @param fuzzySystem {@link #fuzzySystem}
     * @param clampingRule {@link #clampingRule}
     * @param atlboOutputs {@link #atlboOutputs}
     */
    private Run(int iterations, Atlbo atlbo, MainConfiguration mainConfiguration, Population population,
                Interactions interactions, FuzzySystem fuzzySystem, ClampingRule clampingRule, List<AtlboOutput> atlboOutputs) {
        this.iterations = iterations;
        this.atlbo = atlbo;
        this.mainConfiguration = mainConfiguration;
        this.population = new Population(population);
        this.interactions = new Interactions(interactions);
        this.fuzzySystem = fuzzySystem;
        this.clampingRule = clampingRule;
        this.atlboOutputs = atlboOutputs;
    }

    public int getIterations() {
        return iterations;
    }

    public Atlbo getAtlbo() {
        return atlbo;
    }

    public MainConfiguration getMainConfiguration() {
        return mainConfiguration;
    }

    public Population getPopulation() {
        return population;
    }

    public Interactions getInteractions() {
        return interactions;
    }

    public FuzzySystem getFuzzySystem() {
        return fuzzySystem;
    }

    public ClampingRule getClampingRule() {
        return clampingRule;
    }

    public List<AtlboOutput> getAtlboOutputs() {
        return atlboOutputs;
    }

    /**
     * Creates new instance of {@link Builder} that allows creation of the new Run instance.
     *
     * @return new instance of builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder that creates new instance of {@link Run}.
     *
     * @author Tomáš Matějka
     */
    public static class Builder {

        /** How many times {@link Atlbo} was run. */
        private int iterations;

        /** Concrete {@link Atlbo} that was run. */
        private Atlbo atlbo;

        /** Configuration used when running {@link Atlbo}. */
        private MainConfiguration mainConfiguration;

        /** Population used for every {@link Atlbo} iteration of this Run. */
        private Population population;

        /** Interactions used for every {@link Atlbo} iteration of this Run. */
        private Interactions interactions;

        /*** Fuzzy inference system used when running {@link Atlbo}. */
        private FuzzySystem fuzzySystem;

        /** Clamping rule used when running {@link Atlbo}. */
        private ClampingRule clampingRule;

        /** List of {@link AtlboOutput}s gathered from running {@link Atlbo} algorithm {@link #iterations}-times. */
        private List<AtlboOutput> atlboOutputs = new ArrayList<>();

        /**
         * Sets how many times {@link Atlbo} was run and returns instance of builder for future use.
         *
         * @param iterations how many times {@link Atlbo} was run
         * @return instance of builder for future use
         */
        public Builder iterations(int iterations) {
            this.iterations = iterations;
            return this;
        }

        /**
         * Sets concrete {@link Atlbo} that was run and returns instance of builder for future use.
         *
         * @param atlbo concrete {@link Atlbo} that was run
         * @return instance of builder for future use
         */
        public Builder atlbo(Atlbo atlbo) {
            this.atlbo = atlbo;
            return this;
        }

        /**
         * Sets the Configuration used when running {@link Atlbo} and returns instance of builder for future use.
         *
         * @param mainConfiguration configuration used when running {@link Atlbo}.
         * @return instance of builder for future use
         */
        public Builder mainConfiguration(MainConfiguration mainConfiguration) {
            this.mainConfiguration = mainConfiguration;
            return this;
        }

        /**
         * Sets the population used for every {@link Atlbo} iteration of this Run and returns instance of builder for future use.
         *
         * @param population population used for every {@link Atlbo} iteration of this Run
         * @return instance of builder for future use
         */
        public Builder population(Population population) {
            this.population = population;
            return this;
        }

        /**
         * Sets the interactions used for every {@link Atlbo} iteration of this Run and returns instance of builder for future use.
         *
         * @param interactions interactions used for every {@link Atlbo} iteration of this Run
         * @return instance of builder for future use
         */
        public Builder interactions(Interactions interactions) {
            this.interactions = interactions;
            return this;
        }

        /**
         * Sets the fuzzy inference system used when running {@link Atlbo} and returns instance of builder for future use.
         *
         * @param fuzzySystem fuzzy inference system used when running {@link Atlbo}
         * @return instance of builder for future use
         */
        public Builder fuzzySystem(FuzzySystem fuzzySystem) {
            this.fuzzySystem = fuzzySystem;
            return this;
        }

        /**
         * Sets the clamping rule used when running {@link Atlbo} and returns instance of builder for future use.
         *
         * @param clampingRule clamping rule used when running {@link Atlbo}
         * @return instance of builder for future use
         */
        public Builder clampingRule(ClampingRule clampingRule) {
            this.clampingRule = clampingRule;
            return this;
        }

        /**
         * Adds new ATLBO output into the {@link #atlboOutputs} list.
         *
         * @param atlboOutput newly added ATLBO output
         * @return instance of builder for future use
         */
        public Builder addAtlboOutput(AtlboOutput atlboOutput) {
            this.atlboOutputs.add(atlboOutput);
            return this;
        }

        /**
         * Finishes the building process and creates new instance of {@link Run}.
         *
         * @return newly created Run instance
         */
        public Run build() {
            return new Run(iterations, atlbo, mainConfiguration, population, interactions, fuzzySystem, clampingRule, atlboOutputs);
        }

    }

}
