package run;

import atlbo.Atlbo;
import atlbo.AtlboOutput;
import configuration.MainConfiguration;
import interaction.Interactions;
import population.Population;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to run {@link Atlbo} multiple times on the same {@link Population}.
 * All method produce instance of class {@link Run} containing results of running Atlbo.
 *
 * @author Tomáš Matějka
 */
public final class Runner {

    /**
     * Runs given {@link Atlbo} with given parameters n-times determined by parameter 'iterations'.
     *
     * @param iterations how many times {@link Atlbo} will be run
     * @param atlbo concrete ATLBO to run
     * @param mainConfiguration configuration ATLBO will be run for
     * @param population population ATLBO will be run with
     * @param interactions iterations ATLBO will be run with
     * @return Run containing result data
     */
    public static Run run(int iterations, Atlbo atlbo, MainConfiguration mainConfiguration,
                          Population population, Interactions interactions) {

        Run.Builder builder = Run.builder().
                iterations(iterations).
                atlbo(atlbo).
                mainConfiguration(mainConfiguration).
                population(population).
                interactions(interactions).
                fuzzySystem(atlbo.getFuzzySystem()).
                clampingRule(atlbo.getClampingRule());

        for (int i = 0; i < iterations; i++) {
            System.out.println(mainConfiguration.toString() + " Iteration: " + (i + 1));

            AtlboOutput output = atlbo.generate(
                    mainConfiguration,
                    new Population(population),
                    new Interactions(interactions)
            );
            builder.addAtlboOutput(output);
        }

        return builder.build();
    }

    /**
     * Runs given {@link Atlbo} with given parameters n-times for every input configuration.
     *
     * @param iterations how many times {@link Atlbo} will be run
     * @param populationSize size of generated populations
     * @param atlbo concrete ATLBO to run
     * @param mainConfigurations  configurations ATLBO will be run for
     * @return map, where key is the full name of configuration and value is result {@link Run} for that configuration
     */
    public static Map<String, Run> run(int iterations, int populationSize, Atlbo atlbo,
                                       List<MainConfiguration> mainConfigurations) {

        Map<String, Run> configurationOutputs = new LinkedHashMap<>();
        for (MainConfiguration mainConfiguration : mainConfigurations) {
            Run run = run(
                    iterations,
                    atlbo,
                    mainConfiguration,
                    atlbo.getPopulationGenerator().spawn(mainConfiguration.getParameters(), populationSize),
                    atlbo.getInteractionsGenerator().generate(mainConfiguration)
            );
            configurationOutputs.put(mainConfiguration.getFullName(), run);
        }

        return configurationOutputs;
    }

}
