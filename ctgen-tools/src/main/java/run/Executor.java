package run;

import atlbo.Atlbo;
import configuration.MainConfiguration;
import population.Population;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to run {@link Atlbo} multiple times on different {@link Population}.
 * All method produce instance of class {@link Execution} containing results of running Atlbo.
 *
 * @author Tomáš Matějka
 */
public final class Executor {

    /**
     * Runs given {@link Atlbo} with given parameters n-times determined by parameter 'runs' on different population.
     * For every one of these 'Runs', ATLBO is being run m-times determined by parameter 'iterations', see {@link Runner}.
     *
     * @param iterations how many times ATLBO is run within a {@link Run}
     * @param runs how many {@link Run}s are being run
     * @param populationSize size of generated populations
     * @param atlbo concrete ATLBO to run
     * @param mainConfiguration configuration ATLBO will be run for
     * @return Execution containing result data
     */
    public static Execution execute(int iterations, int runs, int populationSize, Atlbo atlbo, MainConfiguration mainConfiguration) {

        Execution.Builder builder = Execution.builder().
                iterations(iterations).
                runsCount(runs).
                populationSize(populationSize).
                atlbo(atlbo).
                mainConfiguration(mainConfiguration).
                populationGenerator(atlbo.getPopulationGenerator()).
                interactionsGenerator(atlbo.getInteractionsGenerator()).
                fuzzySystem(atlbo.getFuzzySystem()).
                clampingRule(atlbo.getClampingRule());

        for (int i = 0; i < runs; i++) {
            System.out.println("Execution: " + i);
            Run run = Runner.run(iterations, atlbo, mainConfiguration,
                    atlbo.getPopulationGenerator().spawn(mainConfiguration.getParameters(), populationSize),
                    atlbo.getInteractionsGenerator().generate(mainConfiguration));
            builder.addRun(run);
        }

        return builder.build();
    }

    /**
     * Runs given {@link Atlbo} with given parameters n-times determined by parameter 'runs' on different population for every input configuration.
     * For every one of these 'Runs', ATLBO is being run m-times determined by parameter 'iterations', see {@link Runner}.
     *
     * @param iterations how many times ATLBO is run within a {@link Run}
     * @param runs how many {@link Run}s are being run
     * @param populationSize size of generated populations
     * @param atlbo concrete ATLBO to run
     * @param mainConfigurations configurations ATLBO will be run for
     * @return mapa, kde klicem je plne jmeno konfigurace a hodnou je Exectuion naplnena vysledkami z X runu
     */
    public static Map<String, Execution> execute(int iterations, int runs, int populationSize, Atlbo atlbo,
                                                 List<MainConfiguration> mainConfigurations) {

        Map<String, Execution> configurationOutputs = new HashMap<>();
        for (MainConfiguration mainConfiguration : mainConfigurations) {
            Execution execution = execute(
                    iterations,
                    runs,
                    populationSize,
                    atlbo,
                    mainConfiguration
            );
            configurationOutputs.put(mainConfiguration.getFullName(), execution);
        }

        return configurationOutputs;
    }

}
