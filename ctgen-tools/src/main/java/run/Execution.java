package run;

import atlbo.Atlbo;
import atlbo.ClampingRule;
import configuration.MainConfiguration;
import fuzzy.FuzzySystem;
import interaction.InteractionsGenerator;
import population.Population;
import population.PopulationGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Class wrapping multiple {@link Run}s.
 * Each {@link Run} was run for different {@link Population}.
 * Output of {@link Executor}.
 *
 * @author Tomáš Matějka
 */
public class Execution {

    /** Number of iterations of one {@link Run#iterations}. Iterations run on the same {@link Population}. */
    private final int iterations;

    /** How many {@link Run}s were done. Each {@link Run} is done on different {@link Population}. */
    private final int runsCount;

    /** Size of the populations being generated when executing Runs. */
    private final int populationSize;

    /** Concrete {@link Atlbo} used for every {@link Run}. */
    private final Atlbo atlbo;

    /** Configuration used in every {@link Run} when running {@link Atlbo}. */
    private final MainConfiguration mainConfiguration;

    /** Generator used for {@link Population} generation for every {@link Run}. */
    private final PopulationGenerator populationGenerator;

    /** Generator used for {@link interaction.Interactions} generation for every {@link Run}. */
    private final InteractionsGenerator interactionsGenerator;

    /** Fuzzy inference system used when running {@link Atlbo}. */
    private final FuzzySystem fuzzySystem;

    /** Clamping rule used when running {@link Atlbo}. */
    private final ClampingRule clampingRule;

    /** List of created {@link Run}s. */
    private final List<Run> runs;

    /**
     * Constructor.
     * Creates new instance for given values.
     *
     * @param iterations {@link #iterations}
     * @param runsCount {@link #runsCount}
     * @param populationSize {@link #populationSize}
     * @param atlbo {@link #atlbo}
     * @param mainConfiguration {@link #mainConfiguration}
     * @param populationGenerator {@link #populationGenerator}
     * @param interactionsGenerator {@link #interactionsGenerator}
     * @param fuzzySystem {@link #fuzzySystem}
     * @param clampingRule {@link #clampingRule}
     * @param runs {@link #runs}
     */
    private Execution(int iterations, int runsCount, int populationSize, Atlbo atlbo, MainConfiguration mainConfiguration,
                      PopulationGenerator populationGenerator, InteractionsGenerator interactionsGenerator, FuzzySystem fuzzySystem, ClampingRule clampingRule, List<Run> runs) {

        this.iterations = iterations;
        this.runsCount = runsCount;
        this.populationSize = populationSize;
        this.atlbo = atlbo;
        this.mainConfiguration = mainConfiguration;
        this.populationGenerator = populationGenerator;
        this.interactionsGenerator = interactionsGenerator;
        this.fuzzySystem = fuzzySystem;
        this.clampingRule = clampingRule;
        this.runs = runs;
    }

    public int getIterations() {
        return iterations;
    }

    public int getRunsCount() {
        return runsCount;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public Atlbo getAtlbo() {
        return atlbo;
    }

    public MainConfiguration getMainConfiguration() {
        return mainConfiguration;
    }

    public PopulationGenerator getPopulationGenerator() {
        return populationGenerator;
    }

    public InteractionsGenerator getInteractionsGenerator() {
        return interactionsGenerator;
    }

    public FuzzySystem getFuzzySystem() {
        return fuzzySystem;
    }

    public ClampingRule getClampingRule() {
        return clampingRule;
    }

    public List<Run> getRuns() {
        return runs;
    }

    /**
     * Creates new instance of {@link Builder} that allows creation of the new Execution instance.
     *
     * @return new instance of builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * BBuilder that creates new instance of {@link Execution}.
     *
     * @author Tomáš Matějka
     */
    public static class Builder {

        /** Number of iterations of one {@link Run#iterations}. Iterations run on the same {@link Population}. */
        private int iterations;

        /** How many {@link Run}s were done. Each {@link Run} is done on different {@link Population}. */
        private int runsCount;

        /** Size of the populations being generated when executing Runs. */
        private int populationSize;

        /** Concrete {@link Atlbo} used for every {@link Run}. */
        private Atlbo atlbo;

        /** Configuration used in every {@link Run} when running {@link Atlbo}. */
        private MainConfiguration mainConfiguration;

        /** Generator used for {@link Population} generation for every {@link Run}. */
        private PopulationGenerator populationGenerator;

        /** Generator used for {@link interaction.Interactions} generation for every {@link Run}. */
        private InteractionsGenerator interactionsGenerator;

        /** Fuzzy inference system used when running {@link Atlbo}. */
        private FuzzySystem fuzzySystem;

        /** Clamping rule used when running {@link Atlbo}. */
        private ClampingRule clampingRule;

        /** List of created {@link Run}s. */
        private List<Run> runs = new ArrayList<>();

        /**
         * Sets the number of iterations of one {@link Run#iterations}. Iterations run on the same {@link Population}.
         *
         * @param iterations number of iterations of one {@link Run#iterations}. Iterations run on the same {@link Population}
         * @return instance of builder for future use
         */
        public Builder iterations(int iterations) {
            this.iterations = iterations;
            return this;
        }

        /**
         * Sets how many {@link Run}s were done. Each {@link Run} is done on different {@link Population}.
         *
         * @param runsCount how many {@link Run}s were done. Each {@link Run} is done on different {@link Population}
         * @return instance of builder for future use
         */
        public Builder runsCount(int runsCount) {
            this.runsCount = runsCount;
            return this;
        }

        /**
         * Sets the size of the populations being generated when executing Runs.
         *
         * @param populationSize size of the populations being generated when executing Runs
         * @return instance of builder for future use
         */
        public Builder populationSize(int populationSize) {
            this.populationSize = populationSize;
            return this;
        }

        /**
         * Sets the concrete {@link Atlbo} used for every {@link Run}.
         *
         * @param atlbo concrete {@link Atlbo} used for every {@link Run}
         * @return instance of builder for future use
         */
        public Builder atlbo(Atlbo atlbo) {
            this.atlbo = atlbo;
            return this;
        }

        /**
         * Sets the configuration used in every {@link Run} when running {@link Atlbo}.
         *
         * @param mainConfiguration configuration used in every {@link Run} when running {@link Atlbo}
         * @return instance of builder for future use
         */
        public Builder mainConfiguration(MainConfiguration mainConfiguration) {
            this.mainConfiguration = mainConfiguration;
            return this;
        }

        /**
         * Sets the generator used for {@link Population} generation for every {@link Run}.
         *
         * @param populationGenerator generator used for {@link Population} generation for every {@link Run}
         * @return instance of builder for future use
         */
        public Builder populationGenerator(PopulationGenerator populationGenerator) {
            this.populationGenerator = populationGenerator;
            return this;
        }

        /**
         * Sets the generator used for {@link interaction.Interactions} generation for every {@link Run}.
         *
         * @param interactionsGenerator generator used for {@link interaction.Interactions} generation for every {@link Run}
         * @return instance of builder for future use
         */
        public Builder interactionsGenerator(InteractionsGenerator interactionsGenerator) {
            this.interactionsGenerator = interactionsGenerator;
            return this;
        }

        /**
         * Sets the fuzzy inference system used when running {@link Atlbo}.
         *
         * @param fuzzySystem fuzzy inference system used when running {@link Atlbo}
         * @return instance of builder for future use
         */
        public Builder fuzzySystem(FuzzySystem fuzzySystem) {
            this.fuzzySystem = fuzzySystem;
            return this;
        }

        /**
         * Sets the clamping rule used when running {@link Atlbo}.
         *
         * @param clampingRule clamping rule used when running {@link Atlbo}
         * @return instance of builder for future use
         */
        public Builder clampingRule(ClampingRule clampingRule) {
            this.clampingRule = clampingRule;
            return this;
        }

        /**
         * Adds new {@link Run} into the {@link #runs} list.
         *
         * @param run newly added Run
         * @return instance of builder for future use
         */
        public Builder addRun(Run run) {
            this.runs.add(run);
            return this;
        }

        /**
         * Finishes the building process and creates new instance of {@link Execution}.
         *
         * @return new instance of {@link Execution}
         */
        public Execution build() {
            return new Execution(iterations, runsCount, populationSize, atlbo, mainConfiguration, populationGenerator, interactionsGenerator, fuzzySystem, clampingRule, runs);
        }


    }


}
