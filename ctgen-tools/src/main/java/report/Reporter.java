package report;

import analysis.Analyzer;
import analysis.AnalyzerOutput;
import org.apache.commons.lang3.StringUtils;
import run.Execution;
import run.Run;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for reporting outputs of {@link atlbo.Atlbo} algorithm, output of {@link run.Runner} and output of {@link run.Executor}.
 * Abstract class that provides methods for reporting to any output {@link #printStream}.
 * Concrete targets are provided by inheriting classes {@link ConsoleReporter} and {@link FileReporter}.
 *
 * @author Tomáš Matějka
 */
public abstract class Reporter {

    /** Formmater for numeric values. */
    private static final DecimalFormat decimalFormatter = new DecimalFormat("00.00");

    /** Output for reporting outputs. */
    private PrintStream printStream;

    /** Analyzer used to analyze {@link atlbo.AtlboOutput}s, {@link Run}s and {@link Execution}s. */
    private static final Analyzer analyzer = new Analyzer();

    /** Header for reported results. */
    public static final Header defaultHeader1;

    /** Header for reported results. */
    public static final Header defaultHeader2;

    /** Header for reported results. */
    public static final Header defaultHeader3;

    /** Currently used header that will be used when reporting header. */
    private Header usedHeader = defaultHeader1;

    /** Inicializes headers. */
    static {
        defaultHeader1 = new Header();
        defaultHeader1.addColumn("ID", 10);
        defaultHeader1.addColumn("Best", 20);
        defaultHeader1.addColumn("Mean", 30);
        defaultHeader1.addColumn("Mean exploit", 50);
        defaultHeader1.addColumn("Mean explore", 50);

        defaultHeader2 = new Header();
        defaultHeader2.addColumn("t", 5);
        defaultHeader2.addColumn("v", 10);
        defaultHeader2.addColumn("Best", 20);
        defaultHeader2.addColumn("Mean", 30);
        defaultHeader2.addColumn("Mean exploit", 50);
        defaultHeader2.addColumn("Mean explore", 50);

        defaultHeader3 = new Header();
        defaultHeader3.addColumn("t", 5);
        defaultHeader3.addColumn("p", 10);
        defaultHeader3.addColumn("Best", 20);
        defaultHeader3.addColumn("Mean", 30);
        defaultHeader3.addColumn("Mean exploit", 50);
        defaultHeader3.addColumn("Mean explore", 50);
    }

    /**
     * Constructor.
     * Ceates new instance with given stream as target for reporting.
     * @param printStream
     */
    protected Reporter(PrintStream printStream) {
        this.printStream = printStream;
    }

    /**
     * Vytvori novou instanci souboroveho reporteru zapisujiciho do daneho souboru.
     *
     * @param file jmeno souboru
     * @return nova instance
     * @throws FileNotFoundException
     */
    /**
     * Creates new instance of {@link FileReporter} for given file name.
     *
     * @param file name of the target file
     * @return new instance of FileReporter
     * @throws FileNotFoundException thrown if the file is inaccessible for some reason
     */
    public static FileReporter getFileReporter(String file) throws FileNotFoundException {
        return new FileReporter(file);
    }

    /**
     * Creates new instance of {@link ConsoleReporter}.
     *
     * @return new instance of ConsoleReporter
     */
    public static ConsoleReporter getConsoleReporter() {
        return new ConsoleReporter();
    }

    /**
     * Na vystup zapise prave pouzivany header.
     */
    public void reportHeader() {
        reportHeader(usedHeader);
    }

    /**
     * Na vystup zapise dany header.
     *
     * @param header zapisovany header
     */
    public void reportHeader(Header header) {
        String formatted = "";
        for (Header.Column column : header.getColumns()) {
            formatted += column.getName();
            formatted = StringUtils.rightPad(formatted, column.getSize(), " ");
        }
        printStream.println(formatted);
    }

    public void reportExecutions(List<Execution> executions) {
        executions.forEach(this::reportExecution);
    }

    public void reportExecution(Execution execution) {
        AnalyzerOutput analyzerOutput = analyzer.analyze(execution);

        String formatted = execution.getMainConfiguration().getShortName();
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(0).getSize(), " ");
        formatted += analyzerOutput.getBestSize();
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(1).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanSize());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(2).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanExploit()) + " (" + decimalFormatter.format(analyzerOutput.getMeanExploitPercentage()) + "%)";
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(3).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanExplore()) + " (" + decimalFormatter.format(analyzerOutput.getMeanExplorePercentage()) + "%)";

        printStream.println(formatted);
    }

    public void reportExecutionsLikeTable4(List<Execution> executions) {
        executions.forEach(this::reportExecutionLikeTable4);
    }

    public void reportExecutionLikeTable4(Execution execution) {
        AnalyzerOutput analyzerOutput = analyzer.analyze(execution);

        String formatted = String.valueOf(execution.getMainConfiguration().getT());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(0).getSize(), " ");
        formatted += String.valueOf(execution.getMainConfiguration().getParameters().size());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(1).getSize(), " ");
        formatted += analyzerOutput.getBestSize();
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(2).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanSize());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(3).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanExploit()) + " (" + decimalFormatter.format(analyzerOutput.getMeanExploitPercentage()) + "%)";
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(4).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanExplore()) + " (" + decimalFormatter.format(analyzerOutput.getMeanExplorePercentage()) + "%)";

        printStream.println(formatted);
    }

    public void reportExecutionLikeTable5(Execution execution) {
        AnalyzerOutput analyzerOutput = analyzer.analyze(execution);

        String formatted = String.valueOf(execution.getMainConfiguration().getT());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(0).getSize(), " ");
        formatted += String.valueOf(execution.getMainConfiguration().getParameters().get(0).getValues());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(1).getSize(), " ");
        formatted += analyzerOutput.getBestSize();
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(2).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanSize());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(3).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanExploit()) + " (" + decimalFormatter.format(analyzerOutput.getMeanExploitPercentage()) + "%)";
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(4).getSize(), " ");
        formatted += decimalFormatter.format(analyzerOutput.getMeanExplore()) + " (" + decimalFormatter.format(analyzerOutput.getMeanExplorePercentage()) + "%)";

        printStream.println(formatted);
    }

    public void reportRuns(List<Run> runs) {
        runs.forEach(this::reportRun);
    }

    public void reportRun(Run run) {
        String formatted = run.getMainConfiguration().getShortName();
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(0).getSize(), " ");
        formatted += analyzer.bestSize(run);
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(1).getSize(), " ");
        formatted += decimalFormatter.format(analyzer.meanSize(run));
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(2).getSize(), " ");
        formatted += analyzer.meanExploit(run) + " (" + decimalFormatter.format(analyzer.meanExploitPercentage(run)) + "%)";
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(3).getSize(), " ");
        formatted += analyzer.meanExplore(run) + " (" + decimalFormatter.format(analyzer.meanExplorePercentage(run)) + "%)";

        printStream.println(formatted);
    }


    public void report(Run run, Header header) {
        reportHeader(header);
        reportRun(run);
    }

    public void report(List<Run> runs, Header header) {
        reportHeader(header);
        reportRuns(runs);
    }

    public void reportRunsLikeTable4(List<Run> runs) {
        runs.forEach(this::reportLikeTable4);
    }

    public void reportLikeTable4(Run run) {
        String formatted = String.valueOf(run.getMainConfiguration().getT());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(0).getSize(), " ");
        formatted += String.valueOf(run.getMainConfiguration().getParameters().size());
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(1).getSize(), " ");
        formatted += analyzer.bestSize(run);
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(2).getSize(), " ");
        formatted += decimalFormatter.format(analyzer.meanSize(run));
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(3).getSize(), " ");
        formatted += analyzer.meanExploit(run) + " (" + decimalFormatter.format(analyzer.meanExploitPercentage(run)) + "%)";
        formatted = StringUtils.rightPad(formatted, usedHeader.getColumns().get(4).getSize(), " ");
        formatted += analyzer.meanExplore(run) + " (" + decimalFormatter.format(analyzer.meanExplorePercentage(run)) + "%)";

        printStream.println(formatted);
    }

    public void report(String text) {
        printStream.print(text);
    }

    public void reportln(String text) {
        printStream.println(text);
    }

    public Header getUsedHeader() {
        return usedHeader;
    }

    public void setUsedHeader(Header usedHeader) {
        this.usedHeader = usedHeader;
    }

    // =======================================================================

    public static class Header {

        private List<Column> columns = new ArrayList<>();

        public void addColumn(String name, int size) {
            this.columns.add(new Column(name, size));
        }

        public List<Column> getColumns() {
            return columns;
        }

        public static class Column {

            private String name;

            private int size;

            public Column(String name, int size) {
                this.name = name;
                this.size = size;
            }

            public String getName() {
                return name;
            }

            public int getSize() {
                return size;
            }
        }
    }

}