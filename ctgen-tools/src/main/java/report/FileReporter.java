package report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Reports to file.
 *
 * @author Tomáš Matějka
 */
public class FileReporter extends Reporter {

    /**
     * Constructor.
     * Creates new instace of reporter that reports to file with given name.
     *
     * @param file name of the target file
     * @throws FileNotFoundException thrown if the file is inaccessible for some reason
     */
    protected FileReporter(String file) throws FileNotFoundException {
        super(new PrintStream(new FileOutputStream(file, true)));
    }

}
