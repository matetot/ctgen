package report;

/**
 * Reports to console.
 *
 * @author Tomáš Matějka
 */
public class ConsoleReporter extends Reporter {

    /**
     * Constructor.
     * Creates new instance.
     */
    protected ConsoleReporter() {
        super(System.out);
    }

}
