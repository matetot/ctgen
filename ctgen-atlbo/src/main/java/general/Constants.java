package general;

import interaction.InteractionsGeneratorImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * General constants.
 *
 * @author Tomáš Matějka
 */
public final class Constants {

    /** Char constant of literal '0', used when generating interactions with {@link InteractionsGeneratorImpl}. */
    public static final char ZERO = '0';

    /** Char constant of literal '1', used when generating interactions with {@link InteractionsGeneratorImpl}. */
    public static final char ONE = '1';

    /**
     * Char constant of literal 'X',
     * marks that {@link interaction.InteractionElementAtom} in {@link interaction.InteractionElement} is not important.
     */
    public static final String DONT_CARE_CHAR = "X";

    /**
     * Constant used for multiplication in member functions of {@link fuzzy.FuzzySystemImpl}.
     *
     * @see fuzzy.functions.QualityMeasureImpl
     * @see fuzzy.functions.IntensificationMeasureImpl
     * @see fuzzy.functions.DiversificationMeasureImpl
     */
    public static final double MEMBER_FUNCTIONS_MULTIPLICATION_CONSTANT = 100.0;

    /** Map of unicode codes for superscript characters of numbers from '0' to '9'. */
    public static final Map<Integer, String> SUPERSCRIPTS = new HashMap<Integer, String>() {
        {
            put(0, "\u2070");
            put(1, "\u00B9");
            put(2, "\u00B2");
            put(3, "\u00B3");
            put(4, "\u2074");
            put(5, "\u2075");
            put(6, "\u2076");
            put(7, "\u2077");
            put(8, "\u2078");
            put(9, "\u2079");
        }
    };

}
