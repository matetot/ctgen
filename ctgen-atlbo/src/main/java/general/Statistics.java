package general;

/**
 * Statistical data collected during {@link atlbo.Atlbo} run.
 *
 * @author Tomáš Matějka
 */
public class Statistics {

    /** How many iterations of while loop ATLBO took to finish. */
    private int iterations;

    /** How many times global search was done during ATLBO run. */
    private int explore;

    /** How many times local search was done during ATLBO run. */
    private int exploit;

    /**
     * Increments iteration counter.
     */
    public void incrementIterations() {
        iterations += 1;
    }

    /**
     * Increments global search counter.
     */
    public void incrementExplore() {
        explore += 1;
    }

    /**
     * Increments local search counter.
     */
    public void incrementExploit() {
        exploit += 1;
    }

    public int getIterations() {
        return iterations;
    }

    public int getExplore() {
        return explore;
    }

    public int getExploit() {
        return exploit;
    }

    @Override
    public String toString() {
        return "Iterations: " + iterations + "\n" +
                "Explore: " + explore + "\n" +
                "Exploit: " + exploit + "\n";
    }

}
