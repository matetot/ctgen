package general;

/**
 * Provides mathematical operations for vectors (adding, subtracting, multiplying by scalar and Hamming distance).
 *
 * @author Tomáš Matějka
 * @see <a href="<a href="https://en.wikipedia.org/wiki/Hamming_distance">Hamming distance</a>"
 */
public final class VectorUtils {

    /**
     * Calculates Hamming distance of two vectors.
     *
     * @param v1 first operand
     * @param v2 second operand
     * @return calculated hamming distance
     */
    public static int hammingDistance(int[] v1, int[] v2) {
        int hammingDistance = 0;
        for (int i = 0; i < v1.length; i++) {
            hammingDistance += v1[i] != v2[i] ? 1 : 0;
        }

        return hammingDistance;
    }

    /**
     * Adds two vectors.
     *
     * @param v1 first operand
     * @param v2 second operand
     * @return calculated vector sum
     */
    public static int[] add(int[] v1, int[] v2) {
        int[] result = new int[v1.length];
        for (int i = 0; i < v1.length; i++) {
            result[i] = v1[i] + v2[i];
        }

        return result;
    }

    /**
     * Subtracts vector 'v2' from vector 'v1'.
     *
     * @param v1 first operand
     * @param v2 second operand
     * @return calculated vector difference
     */
    public static int[] subtract(int[] v1, int[] v2) {
        int[] result = new int[v1.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = v1[i] - v2[i];
        }

        return result;
    }

    /**
     * Multiplies vector by scalar.
     *
     * @param vector multiplied vector
     * @param scalar scalar used for vector multiplying
     * @return calculated multiplied vector
     */
    public static int[] multiplyByScalar(int[] vector, double scalar) {
        int[] result = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            result[i] = (int) Math.round(vector[i] * scalar);
        }

        return result;
    }

}
