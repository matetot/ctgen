package interaction;

import general.Constants;

import java.util.Objects;

/**
 * Interaction element atoms form {@link InteractionElement}s.
 * Atom can either be interacting or non-interacting {@link #NOT_INTERACTING_ATOM}.
 * When atom is interacting, it has some value. If atom is non-interacting, its value is always '-1' and value should never be used anywhere.
 * To create new interacting atom, use public constructor {@link #InteractionElementAtom(int)},
 * to get non-interacting atom, use static method {@link #notInteractingAtom()}.
 *
 * @author Tomáš Matějka
 */
public class InteractionElementAtom {

    /** Constant for non-interacting interaction element atom. */
    private static final InteractionElementAtom NOT_INTERACTING_ATOM = new InteractionElementAtom(-1, false);

    /** Value of atom. */
    private int value;

    /** Flag indicating if atom is interacting or not. */
    private boolean interacting;

    /**
     * Constructor.
     * Creates new interacting atom for given value.
     * New instances can be created only for interacting atoms.
     *
     * @param value value of atom
     */
    public InteractionElementAtom(int value) {
        this(value, true);
    }

    /**
     * Private constructor.
     * Creates new interacting or non-interacting atom with given value.
     *
     * @param value value of atom
     * @param interacting flag if atom is interacting or not
     */
    private InteractionElementAtom(int value, boolean interacting) {
        this.value = value;
        this.interacting = interacting;
    }

    /**
     * Copy constructor.
     * Creates new instance from already existing InteractionElementAtom instance.
     *
     * @param source source InteractionElementAtom
     */
    public InteractionElementAtom(InteractionElementAtom source) {
        this.value = source.getValue();
        this.interacting = source.isInteracting();
    }

    /**
     * Return instance of non-interacting element atom.
     *
     * @return instance of non-interacting element atom
     */
    public static InteractionElementAtom notInteractingAtom() {
        return NOT_INTERACTING_ATOM;
    }

    public int getValue() {
        return value;
    }

    public boolean isInteracting() {
        return interacting;
    }

    @Override
    public String toString() {
        return interacting ? String.valueOf(value) : Constants.DONT_CARE_CHAR;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InteractionElementAtom that = (InteractionElementAtom) o;
        return value == that.value &&
               interacting == that.interacting;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, interacting);
    }
}
