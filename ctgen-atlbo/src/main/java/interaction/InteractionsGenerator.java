package interaction;

import configuration.MainConfiguration;

/**
 * Interface for {@link Interactions} generators.
 *
 * @author Tomáš Matějka
 */
public interface InteractionsGenerator {

    /**
     * For given configuration generates all possible interactions and their interaction elements
     * and returns them as instance of class {@link Interactions}.
     *
     * @param mainConfiguration configuration that provides needed parameters for interactions generation
     * @return instance of class representing generated interactions
     */
    Interactions generate(MainConfiguration mainConfiguration);

}
