package interaction;

import configuration.MainConfiguration;
import configuration.Parameter;
import configuration.SubConfiguration;
import general.Constants;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link InteractionsGenerator} interface.
 *
 * @author Tomáš Matějka
 */
public class InteractionsGeneratorImpl implements InteractionsGenerator {

    @Override
    public Interactions generate(MainConfiguration mainConfiguration) {
        Interactions.Builder interactionsBuilder = new Interactions.Builder();

        int t = mainConfiguration.getT();
        int m = mainConfiguration.getParameters().size();

        // Math.pow(2, m) - 1 -- aby mohlo byt t = p, tady by se musela odstranit '- 1'
        for (int i = 0; i < Math.pow(2, m); i++) {
            String b = StringUtils.leftPad(Integer.toBinaryString(i), m, Constants.ZERO);
            if (StringUtils.countMatches(b, Constants.ONE) == t) {
                interactionsBuilder.putInteraction(b, createInteractionElements(b, mainConfiguration.getParameters()));
                continue;
            }

            for (SubConfiguration s : mainConfiguration.getSubConfigurations()) {
                if (StringUtils.countMatches(b, Constants.ONE) == s.getT() && interactionMatchesParameters(b, s.getParameters(), s.getT())) {
                    interactionsBuilder.putInteraction(b, createInteractionElements(b, mainConfiguration.getParameters()));
                }
            }
        }

        return interactionsBuilder.build();
    }

    /**
     * For given interaction and list of parameters generates all possible {@link InteractionElement}s.
     *
     * @param interaction defines which {@link InteractionElementAtom}s of generated interaction elements are interacting and non-interacting
     *                    e.g. for interaction '0110' means first and last atoms are non-interacting and second and third are interacting
     * @param parameters list of parameters that provide concrete range of values for generated interaction elements and their atoms
     * @return list of generated interaction elements
     */
    private List<InteractionElement> createInteractionElements(String interaction, List<Parameter> parameters) {
        List<InteractionElement> interactionElements = new ArrayList<>();

        int n = StringUtils.countMatches(interaction, Constants.ONE);
        int[] indices = new int[n];
        for (int i = 0; i < n; i++) {
            indices[i] = 0;
        }

        List<Parameter> interactingParamters = new ArrayList<>();
        for (int i = 0; i < interaction.length(); i++) {
            if (interaction.charAt(i) == Constants.ONE) {
                interactingParamters.add(parameters.get(i));
            }
        }

        // https://www.geeksforgeeks.org/combinations-from-n-arrays-picking-one-element-from-each-array/
        while (true) {
            int interactingParameterIndex = 0;
            InteractionElementAtom[] elementAtoms = new InteractionElementAtom[parameters.size()];

            for (int i = 0; i < parameters.size(); i++) {
                if (interaction.charAt(i) == Constants.ONE) {
                    elementAtoms[i] = new InteractionElementAtom(indices[interactingParameterIndex]);
                    interactingParameterIndex += 1;
                } else {
                    elementAtoms[i] = InteractionElementAtom.notInteractingAtom();
                }
            }
            interactionElements.add(new InteractionElement(elementAtoms));

            int next = n - 1;
            while (next >= 0 && (indices[next] + 1 >= interactingParamters.get(next).getValues())) {
                next--;
            }

            if (next < 0) {
                break;
            }

            indices[next]++;

            for (int i = next + 1; i < n; i++) {
                indices[i] = 0;
            }
        }

        return interactionElements;
    }

    /**
     * Checks if given interaction fits given list of sub-configuration parameters and 't' value.
     * Given interaction must have value '1' in positions determined by parameters in given list.
     * If result is 'true', interaction elements for given interaction will be generated,
     * see {@link #generate(MainConfiguration)} and {@link #createInteractionElements(String, List)}
     *
     * @param interaction defines which {@link InteractionElementAtom}s are interacting
     * @param parameters list of sub-configuration parameters that define important position for this method
     * @param t strength of interaction, here meaning the number of positions, that must contain value '1'
     * @return true/false if given interaction matches to the list of parameters and 't' value
     */
    private boolean interactionMatchesParameters(String interaction, List<Parameter> parameters, int t) {
        for (Parameter p : parameters) {
            if (interaction.charAt(p.getPosition()) == Constants.ONE) {
                t--;
            }
        }
        return t == 0;
    }

}
