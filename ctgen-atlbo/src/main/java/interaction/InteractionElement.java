package interaction;

import population.PopulationMember;

import java.util.Arrays;

/**
 * Interaction element is part of an interaction {@link Interactions#interactions}.
 * Element is formed by interacting and non-interacting {@link InteractionElementAtom}s.
 * <p>
 * E.g. element [X, 1, 2, X] is part of 0110 interaction - first and last atom are non-interacting and two middle ones are interacting.
 *
 * @author Tomáš Matějka
 */
public class InteractionElement {

    /** {@link InteractionElementAtom}s that form this interaction element. */
    private final InteractionElementAtom[] interactionElementAtoms;

    /**
     * Constructor.
     * Creates new instance for given atoms array.
     *
     * @param interactionElementAtoms atoms array forming interaction element
     */
    public InteractionElement(InteractionElementAtom[] interactionElementAtoms) {
        this.interactionElementAtoms = interactionElementAtoms;
    }

    /**
     * Copy constructor.
     * Creates new instance from already existing InteractionElement instance.
     *
     * @param source source InteractionElement
     */
    public InteractionElement(InteractionElement source) {
        interactionElementAtoms = new InteractionElementAtom[source.getInteractionElementAtoms().length];
        for (int i = 0; i < interactionElementAtoms.length; i++) {
            interactionElementAtoms[i] = new InteractionElementAtom(source.getInteractionElementAtoms()[i]);
        }
    }

    /**
     * Checks if given {@link PopulationMember} is covered by this interaction element.
     * Calls {@link #isCoveredBy(int[])} for given population member´s solution.
     *
     * @param populationMember checked population member
     * @return true/false if population member is covered or not
     */
    public boolean isCoveredBy(PopulationMember populationMember) {
        return isCoveredBy(populationMember.getSolution());
    }

    /**
     * Checks if given array is covered by this interaction element.
     *
     * @param solution checked array
     * @return true/false if population member is covered or not
     */
    public boolean isCoveredBy(int [] solution) {
        for (int i = 0; i < interactionElementAtoms.length; i++) {
            if (interactionElementAtoms[i].isInteracting() && interactionElementAtoms[i].getValue() != solution[i]) {
                return false;
            }
        }
        return true;
    }

    public InteractionElementAtom[] getInteractionElementAtoms() {
        return interactionElementAtoms;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();

        for (int i = 0; i < interactionElementAtoms.length; i++) {
            out.append(interactionElementAtoms[i].toString()).append( " ");
        }

        return out.toString().trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InteractionElement that = (InteractionElement) o;
        return Arrays.equals(interactionElementAtoms, that.interactionElementAtoms);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(interactionElementAtoms);
    }
}
