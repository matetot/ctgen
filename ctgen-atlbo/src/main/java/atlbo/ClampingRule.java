package atlbo;

import configuration.Parameter;

import java.util.List;

/**
 * During the {@link Atlbo} algorithm, solutions of {@link population.PopulationMember#solution} are being changed through calculations.
 * These newly calculated solutions can be out of bound of the search space, so they need to be returned back
 * and that is what clamping rule is there for.
 *
 * @author Tomáš Matějka
 */
public interface ClampingRule {

    /**
     * Clamps the given solution back to the search space if needed.
     *
     * @param solution clamped vector
     * @param parameters list of parameters defining the range of each position of vector
     * @return vector that is back in search space
     */
    int[] clamp(int[] solution, List<Parameter> parameters);

}
