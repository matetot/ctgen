package atlbo;

import general.Statistics;
import population.PopulationMember;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Output of the {@link Atlbo}.
 * Contains statistical data of ATLBO run and most importantly, contains final test suite generated during ATLBO run.
 *
 * @author Tomáš Matějka
 * @see Statistics
 */
public class AtlboOutput {

    /** Statistical data gathered during ATLBO run. */
    private Statistics statistics = new Statistics();

    /** ATLBO generated set of tests covering all possible interactions. */
    private List<Integer[]> finalTestSuite = new ArrayList<>();

    /**
     * Adds the solution from the given {@link PopulationMember} into the {@link #finalTestSuite} list.
     *
     * @param populationMember population member containg added solution
     */
    public void addSolution(PopulationMember populationMember) {
        finalTestSuite.add(Arrays.stream(populationMember.getSolution()).boxed().toArray(Integer[]::new));
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public List<Integer[]> getFinalTestSuite() {
        return finalTestSuite;
    }

}
