package atlbo;

import configuration.Parameter;

import java.util.List;

/**
 * Implementation of {@link ClampingRule} interface.
 *
 * Absorbing walls work like this:
 * - if a value overflows its upper bound, the value is clamped to the value of lower bound
 * - if a value underflows its lower bound, the value is clamped to the value of upper bound
 *
 * @author Tomáš Matějka
 */
public class AbsorbingWalls implements ClampingRule {

    @Override
    public int[] clamp(int[] solution, List<Parameter> parameters) {
        int [] result = new int[solution.length];
        for (int i = 0; i < solution.length; i++) {
            if (solution[i] >= parameters.get(i).getValues()) { // if the value is over the upper bound - set it to 0
                result[i] = 0;
            } else if (solution[i] < 0) {                       // if the value is under the lower bound - set it to upper bound
                result[i] = parameters.get(i).getValues() - 1;
            } else {
                result[i] = solution[i];
            }
        }
        return result;
    }

}
