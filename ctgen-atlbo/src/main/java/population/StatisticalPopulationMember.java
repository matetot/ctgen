package population;

import java.util.ArrayList;
import java.util.List;

/**
 * Enhanced {@link PopulationMember} by list of ancestors ({@link #ancestors} that contains all past versions of population member.
 * Each update creates new record in {@link #ancestors} list.
 *
 * @author Tomáš Matějka
 */
public class StatisticalPopulationMember extends PopulationMember {

    /** List of all past versions of population member. */
    private List<PopulationMember> ancestors = new ArrayList<>();

    /**
     * Constructor.
     * Creates new member with given initial solution.
     *
     * @param solution initial solution
     */
    public StatisticalPopulationMember(int[] solution) {
        super(solution);
    }

    /**
     * Constructor
     * Creates new member with given initial solution and initial fitness value.
     *
     * @param solution initial solution
     * @param fitness initial fitness value
     */
    public StatisticalPopulationMember(int[] solution, int fitness) {
        super(solution, fitness);
    }

    /**
     * Copy constructor.
     * Creates new instance from already existing PopulationMember instance.
     *
     * @param populationMember source PopulationMember
     */
    public StatisticalPopulationMember(PopulationMember populationMember) {
        super(populationMember);
    }

    /**
     * First adds new record into the {@link #ancestors} list
     * and then updates the population member with new solution and its fitness value.
     *
     * @param solution new solution
     * @param fitness fitness value of the new solution
     */
    public void update(int[] solution, int fitness) {
        this.ancestors.add(new PopulationMember(this));
        super.update(solution, fitness);
    }

    public List<PopulationMember> getAncestors() {
        return ancestors;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < ancestors.size(); i++) {
            out.append(super.toString()).append(" -> ");
        }
        out.append(super.toString());
        return out.toString();
    }

}
