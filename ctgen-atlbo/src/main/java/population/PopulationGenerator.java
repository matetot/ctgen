package population;

import configuration.Parameter;

import java.util.List;

/**
 * Interface for {@link Population} generators.
 *
 * @author Tomáš Matějka
 */
public interface PopulationGenerator {

    /**
     * From given parameters creates new population of given size.
     *
     * @param parameters list of parameters defining size of population members and defining concrete range of values for each member´s position
     * @param populationSize size of created population
     * @return newly created population
     */
    Population spawn(List<Parameter> parameters, int populationSize);

    /**
     * From given parameters creates new population of given size with population members being of given class.
     * Base class for any member is {@link PopulationMember}.
     *
     * @param parameters list of parameters defining size of population members and defining concrete range of values for each member´s position
     * @param populationSize size of created population
     * @param populationMemberType class of population members
     * @return newly created population
     */
    Population spawn(List<Parameter> parameters, int populationSize, Class<? extends PopulationMember> populationMemberType);

}
