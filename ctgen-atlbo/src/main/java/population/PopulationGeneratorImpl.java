package population;

import configuration.Parameter;
import org.apache.commons.lang3.RandomUtils;

import java.util.List;

/**
 * Implementation of {@link PopulationGenerator} interface.
 *
 * @author Tomáš Matějka
 */
public class PopulationGeneratorImpl implements PopulationGenerator {

    @Override
    public Population spawn(List<Parameter> parameters, int populationSize) {
        return spawn(parameters, populationSize, PopulationMember.class);
    }

    @Override
    public Population spawn(List<Parameter> parameters, int populationSize, Class<? extends PopulationMember> populationMemberType) {
        Population population = new Population(populationSize, parameters.size(), populationMemberType);

        for (int i = 0; i < population.getPopulationSize(); i++) {
            int [] solution = new int[population.getPopulationMemberSize()];
            for (int j = 0; j < population.getPopulationMemberSize(); j++) {
                solution[j] = RandomUtils.nextInt(0, parameters.get(j).getValues());
            }
            population.addMember(solution);
        }

        return population;
    }

}
