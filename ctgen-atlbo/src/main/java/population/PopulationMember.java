package population;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

/**
 * Member of {@link Population}.
 *
 * @author Tomáš Matějka
 */
public class PopulationMember {

    /** Possible solution that is being calculated and used in {@link atlbo.Atlbo} algorithm. */
    private int[] solution;

    /**
     * How many interaction elements this member covers.
     *
     * @see interaction.Interactions#calculateFitness(PopulationMember)
     * @see interaction.InteractionElement#isCoveredBy(PopulationMember)
     */
    private int fitness;

    /**
     * Constructor.
     * Creates new member with given initial solution and zero fitness value.
     *
     * @param solution initial solution
     */
    public PopulationMember(int[] solution) {
        this(solution, 0);
    }

    /**
     * Constructor
     * Creates new member with given initial solution and initial fitness value.
     *
     * @param solution initial solution
     * @param fitness initial fitness value
     */
    public PopulationMember(int[] solution, int fitness) {
        this.solution = solution;
        this.fitness = fitness;
    }

    /**
     * Copy constructor.
     * Creates new instance from already existing PopulationMember instance.
     *
     * @param populationMember source PopulationMember
     */
    public PopulationMember(PopulationMember populationMember) {
        this.solution = ArrayUtils.clone(populationMember.getSolution());
        this.fitness = populationMember.getFitness();
    }

    /**
     * Updates the population member with new solution and its fitness value.
     *
     * @param solution new solution
     * @param fitness fitness value of the new solution
     */
    public void update(int[] solution, int fitness) {
        this.solution = solution;
        this.fitness = fitness;
    }

    /**
     * Updates the population member with new fitness value.
     *
     * @param fitness new fitness value of the population member
     */
    public void update(int fitness) {
        this.fitness = fitness;
    }

    public int[] getSolution() {
        return solution;
    }

    public int getFitness() {
        return fitness;
    }

    @Override
    public String toString() {
        return Arrays.toString(solution) + " : " + fitness;
    }

}
