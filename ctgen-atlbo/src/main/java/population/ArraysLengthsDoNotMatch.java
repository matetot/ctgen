package population;

/**
 * Exception thrown by {@link Population#addMember(int[])} when length of input array
 * does not match defined {@link Population#populationMemberSize}.
 *
 * @author Tomáš Matějka
 */
public class ArraysLengthsDoNotMatch extends RuntimeException {

    /**
     * Constructor.
     * Creates new instance with given error message.
     *
     * @param errorMessage error message
     */
    public ArraysLengthsDoNotMatch(String errorMessage) {
        super(errorMessage);
    }

}
