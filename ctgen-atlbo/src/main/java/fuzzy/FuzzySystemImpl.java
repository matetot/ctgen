package fuzzy;

import fuzzy.functions.*;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;
import population.Population;
import population.PopulationMember;

import java.io.InputStream;

/**
 * Implementation of {@link FuzzySystem} interface.
 * Using member functions {@link QualityMeasure}, {@link IntensificationMeasure}, {@link DiversificationMeasure}.
 * This class uses library for fuzzy logic calculations called '<a href="http://jfuzzylogic.sourceforge.net/html/index.html">jFuzzyLogic</a>'..
 *
 * @author Tomáš Matějka
 */
public class FuzzySystemImpl implements FuzzySystem {

    /** Fuzzy logic inference system provided by 'jFuzzyLogic'. */
    private FIS fis;

    /** Block from fcl file containing description of variables, rules etc. */
    private FunctionBlock functionBlock;

    /** Quality measure member function used for calculations. */
    private QualityMeasure qualityMeasure;

    /** Intensification measure member function used for calculations. */
    private IntensificationMeasure intensificationMeasure;

    /** Diversification measure member function used for calculations. */
    private DiversificationMeasure diversificationMeasure;

    /** Identifier of quality measure block in fcl file. */
    private String qualityMeasureName;

    /** Identifier of intensification measure block in fcl file. */
    private String intensificationMeasureName;

    /** Identifier of diversification measure block in fcl file. */
    private String diversificationMeasureName;

    /** Identifier of selection block in fcl file. */
    private String selectionName;

    /**
     * Constructor. Creates new instance with default member functions.
     */
    public FuzzySystemImpl() {
        this(
                BasicStructure.get(),
                new QualityMeasureImpl(),
                new IntensificationMeasureImpl(),
                new DiversificationMeasureImpl()
        );
    }

    /**
     * Constructor. Creates new instance with given member functions.
     *
     * @param fuzzyStructure class containing names of key words in input fcl file
     * @param qualityMeasure quality measure member function
     * @param intensificationMeasure intensification measure member function
     * @param diversificationMeasure diversification measure member function
     */
    public FuzzySystemImpl(FuzzyStructure fuzzyStructure,
                           QualityMeasure qualityMeasure,
                           IntensificationMeasure intensificationMeasure,
                           DiversificationMeasure diversificationMeasure) {
        this.qualityMeasure = qualityMeasure;
        this.intensificationMeasure = intensificationMeasure;
        this.diversificationMeasure = diversificationMeasure;

        this.qualityMeasureName = fuzzyStructure.getQualityMeasure();
        this.intensificationMeasureName = fuzzyStructure.getIntensificationMeasure();
        this.diversificationMeasureName = fuzzyStructure.getDiversificationMeasure();
        this.selectionName = fuzzyStructure.getSelection();

        InputStream fileStream = FuzzySystemImpl.class.getClassLoader().getResourceAsStream(fuzzyStructure.getFclFile());
        fis = FIS.load(fileStream, true);

        if (fis == null) {
            throw new RuntimeException("Cannot load fcl file '" + fuzzyStructure.getFclFile() + "'");
        }
        functionBlock = fis.getFunctionBlock(fuzzyStructure.getFunctionBlock());
    }

    @Override
    public double selection(int maxFitness, int minFitness, PopulationMember current, Population population) {
        double quality = qualityMeasure.calculate(maxFitness, minFitness, current.getFitness());
        double intensification = intensificationMeasure.calculate(current, population);
        double diversification = diversificationMeasure.calculate(current, population);

        fis.setVariable(qualityMeasureName, quality);
        fis.setVariable(intensificationMeasureName, intensification);
        fis.setVariable(diversificationMeasureName, diversification);
        fis.evaluate();

        return functionBlock.getVariable(selectionName).getLatestDefuzzifiedValue();
    }

}