package fuzzy;

/**
 * Defines the fcl input for {@link FuzzySystemImpl}.
 * Fuzzy structure contains the name of the input fcl file, name of the function block containing variables and functions and
 * names of the variables and functions.
 *
 * @author Tomáš Matějka
 * @see <a href="http://jfuzzylogic.sourceforge.net/html/manual.html#details">jFuzzy logic FCL definitions</a>
 */
public abstract class FuzzyStructure {

    /** Name of the fcl file containing description of fuzzy inference system. */
    protected final String fclFile;

    /** Name of the function block containing variables and functions. */
    protected final String functionBlock;

    /** Name of the quality measure variable. */
    protected final String qualityMeasure;

    /** Name of the intensification measure variable. */
    protected final String intensificationMeasure;

    /** Name of the diversification measure variable. */
    protected final String diversificationMeasure;

    /** Name of the selection variable. */
    protected final String selection;

    /**
     * Constructor.
     * Creates new structure definition for given parameters.
     *
     * @param fclFile name of the fcl file containing description of fuzzy inference system
     * @param functionBlock name of the function block containing variables and functions
     * @param qualityMeasure name of the quality measure variable
     * @param intensificationMeasure name of the intensification measure variable
     * @param diversificationMeasure name of the diversification measure variable
     * @param selection name of the selection variable
     */
    protected FuzzyStructure(String fclFile, String functionBlock, String qualityMeasure, String intensificationMeasure,
                          String diversificationMeasure, String selection) {
        this.fclFile = fclFile;
        this.functionBlock = functionBlock;
        this.qualityMeasure = qualityMeasure;
        this.intensificationMeasure = intensificationMeasure;
        this.diversificationMeasure = diversificationMeasure;
        this.selection = selection;
    }

    public String getFclFile() {
        return fclFile;
    }

    public String getFunctionBlock() {
        return functionBlock;
    }

    public String getQualityMeasure() {
        return qualityMeasure;
    }

    public String getIntensificationMeasure() {
        return intensificationMeasure;
    }

    public String getDiversificationMeasure() {
        return diversificationMeasure;
    }

    public String getSelection() {
        return selection;
    }

}
