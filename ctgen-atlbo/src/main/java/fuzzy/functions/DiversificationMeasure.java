package fuzzy.functions;

import population.Population;
import population.PopulationMember;

/**
 * Interface for 'Diversification measure' which is one of the member functions of fuzzy system.
 * Used in {@link fuzzy.FuzzySystemImpl}.
 *
 * @author Tomáš Matějka
 */
public interface DiversificationMeasure {

    /**
     * Calculates diversification measure of {@link PopulationMember} in {@link Population}.
     *
     * @param current member whose diversification measure is being calculated
     * @param population population containing member whose diversification measure is being calculated
     * @return calculated diversification measure
     */
    double calculate(PopulationMember current, Population population);

}
