package fuzzy.functions;

import general.Constants;
import general.VectorUtils;
import population.Population;
import population.PopulationMember;

/**
 * Implementation of {@link DiversificationMeasure} interface for fuzzy member function 'Diversification measure'.
 *
 * @author Tomáš Matějka
 */
public class DiversificationMeasureImpl implements DiversificationMeasure {

    @Override
    public double calculate(PopulationMember current, Population population) {
        double D = Math.abs(1.0 - (double) population.getPopulationSize()) * (double) population.getPopulationMemberSize();
        double hammingDistanceSum = 0;
        for (int i = 0; i < population.getPopulationSize(); i++) {
            hammingDistanceSum += VectorUtils.hammingDistance(population.getAt(i).getSolution(), current.getSolution());
        }

        return (hammingDistanceSum / D) * Constants.MEMBER_FUNCTIONS_MULTIPLICATION_CONSTANT;
    }

}
