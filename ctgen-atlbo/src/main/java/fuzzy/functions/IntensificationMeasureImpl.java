package fuzzy.functions;

import general.Constants;
import general.VectorUtils;
import population.Population;
import population.PopulationMember;

/**
 * Implementation of {@link IntensificationMeasure} interface for fuzzy member function 'Intensification measure'.
 *
 * @author Tomáš Matějka
 */
public class IntensificationMeasureImpl implements IntensificationMeasure {

    @Override
    public double calculate(PopulationMember current, Population population) {
        double hammingDistance = VectorUtils.hammingDistance(population.getBest().getSolution(), current.getSolution());
        double D = population.getPopulationMemberSize();

        return (hammingDistance / D) * Constants.MEMBER_FUNCTIONS_MULTIPLICATION_CONSTANT;
    }

}
