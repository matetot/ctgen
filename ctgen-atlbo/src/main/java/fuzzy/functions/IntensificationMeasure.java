package fuzzy.functions;

import population.Population;
import population.PopulationMember;

/**
 * Interface for 'Intensification measure' which is one of the member functions of fuzzy system.
 * Used in {@link fuzzy.FuzzySystemImpl}.
 *
 * @author Tomáš Matějka
 */
public interface IntensificationMeasure {

    /**
     * Calculates intensification measure of {@link PopulationMember} in {@link Population}.
     *
     * @param current member whose intensification measure is being calculated
     * @param population population containing member whose intensification measure is being calculated
     * @return calculated intensification measure
     */
    double calculate(PopulationMember current, Population population);

}
