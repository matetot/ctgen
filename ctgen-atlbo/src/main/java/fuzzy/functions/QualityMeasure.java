package fuzzy.functions;

/**
 * Interface for 'Quality measure' which is one of the member functions of fuzzy system.
 * Used in {@link fuzzy.FuzzySystemImpl}.
 *
 * @author Tomáš Matějka
 */
public interface QualityMeasure {

    /**
     * Calculates quality measure of {@link population.PopulationMember}.
     *
     * @param maxFitness best possible fitness population member can have
     * @param minFitness worst possible fitness population member can have
     * @param currentFitness fitness of population member its quality is being calculated
     * @return calculated quality measure
     */
    double calculate(double maxFitness, double minFitness, double currentFitness);

}
