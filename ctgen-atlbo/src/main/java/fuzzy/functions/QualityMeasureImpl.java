package fuzzy.functions;

import general.Constants;

/**
 * Implementation of {@link QualityMeasure} interface for fuzzy member function 'Quality measure'.
 *
 * @author Tomáš Matějka
 */
public class QualityMeasureImpl implements QualityMeasure {

    @Override
    public double calculate(double maxFitness, double minFitness, double currentFitness) {
        return ((currentFitness - minFitness) / (maxFitness - minFitness)) * Constants.MEMBER_FUNCTIONS_MULTIPLICATION_CONSTANT;
    }

}
