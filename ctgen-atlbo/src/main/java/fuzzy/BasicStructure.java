package fuzzy;

/**
 * Implementation of abstract class {@link FuzzyStructure}.
 * Provides concrete values for fuzzy structure.
 * Singleton.
 *
 * @author Tomáš Matějka
 */
public class BasicStructure extends FuzzyStructure {

    /** Single existing structure, */
    private static BasicStructure INSTANCE = null;

    /**
     * Constructor.
     * Creates new instance with default values.
     */
    private BasicStructure() {
        super(
                "fuzzy-default.fcl",
                "fuzzy_atlbo",
                "quality",
                "intensification",
                "diversification",
                "selection"
        );
    }

    /**
     * Return only existing instance of this class or creates it if no instance exists.
     *
     * @return instance of the class
     */
    public static BasicStructure get() {
        if (INSTANCE == null) {
            INSTANCE = new BasicStructure();
        }

        return INSTANCE;
    }
}
