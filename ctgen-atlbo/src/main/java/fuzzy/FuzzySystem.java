package fuzzy;

import population.Population;
import population.PopulationMember;

/**
 * Interface for fuzzy system component of {@link atlbo.Atlbo}.
 * Fuzzy system is used to calculate value 'selection' which determines whether will go into global or local search.
 *
 * @author Tomáš Matějka
 */
public interface FuzzySystem {

    /**
     * For given values, calculates 'selection' value.
     *
     * @param maxFitness best possible fitness population member can have
     * @param minFitness worst possible fitness population member can have
     * @param current member that ATLBO is currently processing
     * @param population population ATLBO is working with
     * @return calculated value
     */
    double selection(int maxFitness, int minFitness, PopulationMember current, Population population);

}
