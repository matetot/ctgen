package configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * One of the input parameters for {@link atlbo.Atlbo} and also used in generating interactions {@link interaction.InteractionsGenerator}.
 * Expands the base class {@link Configuration} by possibility of having {@link SubConfiguration}s.
 *
 * @author Tomáš Matějka
 */
public class MainConfiguration extends Configuration {

    /** Full name of the MainConfiguration. */
    private String fullName;

    /** Short name of the MainConfiguration. */
    private String shortName;

    /** List of {@link SubConfiguration}s of this MainConfiguration. */
    private List<SubConfiguration> subConfigurations = new ArrayList<>();

    /**
     * Constructor.
     * Create new instance with no name and given strength of interaction.
     *
     * @param t strength of interaction
     */
    public MainConfiguration(int t) {
        this("", t);
    }

    /**
     * Constructor.
     * Creates new instance with given full name, no short name and given strength of interaction.
     *
     * @param fullName full name of created MainConfiguration
     * @param t strength of interaction
     */
    public MainConfiguration(String fullName, int t) {
        this(fullName, "", t);
    }

    /**
     * Consstructor.
     * Creates new instance with given full name, short name nad strength of interaction.
     *
     * @param fullName full name of the created MainConfiguration
     * @param shortName short name of the created MainConfiguration
     * @param t strength of interaction
     */
    public MainConfiguration(String fullName, String shortName, int t) {
        super(t);
        this.fullName = fullName;
        this.shortName = shortName;
    }

    /**
     * Adds given {@link SubConfiguration} into the {@link #subConfigurations} list.
     *
     * @param subConfiguration added sub-configuration
     */
    public void addSubconfiguration(SubConfiguration subConfiguration) {
        subConfigurations.add(subConfiguration);
    }

    public List<SubConfiguration> getSubConfigurations() {
        return subConfigurations;
    }

    public String getFullName() {
        return fullName;
    }

    public String getShortName() {
        return shortName;
    }

    @Override
    public String toString() {
        StringBuilder notation = new StringBuilder();

        notation.append(subConfigurations.isEmpty() ?"CA" : "VCA");

        notation.append(" (N: ").append(t).append(",").append(super.toString());
        for (SubConfiguration subConfiguration : subConfigurations) {
            notation.append(", ").append(subConfiguration.toString());
        }
        notation.append(")");

        return notation.toString();
    }

}
