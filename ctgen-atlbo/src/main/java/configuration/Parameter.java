package configuration;

/**
 * Parameter of {@link Configuration}.
 * Does not contain concrete values but the count of values this parameter has.
 *
 * @author Tomáš Matějka
 * @see MainConfiguration
 * @see SubConfiguration
 */
public class Parameter {

    /** Position of the parameter in a {@link Configuration}. */
    private int position;

    /** Number of values the parameter has. */
    private int values;

    /** Identifier by which the parameter can be located. */
    private String identifier;

    /**
     * Constructor.
     * Creates new parameter with given position and number of values.
     *
     * @param position position of the parameter
     * @param values number of the values of the parameter
     */
    public Parameter(int position, int values) {
        this(position, values, "");
    }

    /**
     * Constructor.
     * Creates new parameter with given position, number of values and identifier.
     *
     * @param position position of the parameter
     * @param values number of the values of the parameter
     * @param identifier identifier by which the parameter can be located
     */
    public Parameter(int position, int values, String identifier) {
        this.position = position;
        this.values = values;
        this.identifier = identifier;
    }

    public int getPosition() {
        return position;
    }

    public int getValues() {
        return values;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "position=" + position +
                ", values=" + values +
                ", identifier='" + identifier + '\'' +
                '}';
    }

}
