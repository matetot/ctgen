package configuration;

import general.Constants;

import java.util.*;

/**
 * Base class for {@link MainConfiguration} and {@link SubConfiguration}.
 * Provides interaction strength {@link #t} and list of {@link #parameters} of configuration.
 *
 * @author Tomáš Matějka
 */
public abstract class Configuration {

    /** Strength of interaction - how many parameters interact with each other. */
    protected int t;

    /** List of configuration {@link Parameter}s. */
    protected List<Parameter> parameters = new ArrayList<>();

    /**
     * Constructor.
     * Creates new instance with given strength of interactions.
     *
     * @param t strength of interaction
     */
    protected Configuration(int t) {
        this.t = t;
    }

    /**
     * Creates new {@link Parameter} from given values and adds it into the {@link #parameters} list.
     *
     * @param position position of newly added parameter
     * @param values number of values of newly added parameter
     * @return newly created and added parameter
     */
    public Parameter addParameter(int position, int values) {
        Parameter p = new Parameter(position, values);
        addParameter(p);
        return p;
    }

    /**
     * Adds given {@link Parameter} into the {@link #parameters} list.
     *
     * @param parameter added parameter
     */
    public void addParameter(Parameter parameter) {
        this.parameters.add(parameter);
    }

    /**
     * Adds all given {@link Parameter}s into the {@link #parameters} list.
     *
     * @param parameters added parameters
     */
    public void addParameters(Parameter... parameters) {
        this.parameters.addAll(Arrays.asList(parameters));
    }

    /**
     * Adds all {@link Parameter}s from given list into the {@link #parameters} list.
     *
     * @param parameters list of added parameters
     */
    public void addParameters(List<Parameter> parameters) {
        this.parameters.addAll(parameters);
    }

    /**
     * Creates a map where key is the number of values in a parameter and value is the number of parameters with the same number of values.
     *
     * @return created map <code>Map<number of values in a parameter, number of parameters with the same number of values></></code>
     */
    protected Map<Integer, Integer> parametersForValues() {
        Map<Integer, Integer> paramForValues = new LinkedHashMap<>();

        for (Parameter parameter : parameters) {
            int val = paramForValues.getOrDefault(parameter.getValues(), 0);
            paramForValues.put(parameter.getValues(), ++val);
        }

        return paramForValues;
    }

    public int getT() {
        return t;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        StringBuilder notation = new StringBuilder();

        Map<Integer, Integer> paramValues = parametersForValues();
        for (Map.Entry<Integer, Integer> entry : paramValues.entrySet()) {
            int parameters = entry.getValue();
            int values = entry.getKey();

            StringBuilder superscript = new StringBuilder();
            if (parameters > 9) {
                String params = String.valueOf(parameters);
                for (int i = 0; i < params.length(); i++) {
                    superscript.append(Constants.SUPERSCRIPTS.get(Integer.valueOf(String.valueOf(params.charAt(i)))));
                }
            } else {
                superscript.append(Constants.SUPERSCRIPTS.get(parameters));
            }

            notation.append(" ").append(values).append(superscript);

        }

        return notation.toString();
    }

}
