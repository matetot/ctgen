package configuration;

/***
 * Sub-configuration of the {@link MainConfiguration}.
 * Does not expand the base class {@link Configuration} by anything.
 *
 * @author Tomáš Matějka
 */
public class SubConfiguration extends Configuration {

    /**
     * Constructor.
     * Creates new sub-configuration for given t.
     *
     * @param t strength of interaction
     */
    public SubConfiguration(int t) {
        super(t);
    }

    @Override
    public String toString() {
        return "CA (N: " + t + "," + super.toString() + ")";
    }

}
