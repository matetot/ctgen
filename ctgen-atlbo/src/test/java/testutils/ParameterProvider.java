package testutils;


import configuration.Parameter;

import java.util.ArrayList;
import java.util.List;

public final class ParameterProvider {

    public static List<Parameter> createParamters(int... values) {
        List<Parameter> parameters = new ArrayList<>(values.length);
        for (int i = 0; i < values.length; i++) {
            parameters.add(new Parameter(i, values[i]));
        }
        return parameters;
    }

}
