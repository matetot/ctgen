package junit.general;

import general.VectorUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VectorUtilsTest {

    @Test
    public void hammingDistanceTest() {
        Assertions.assertEquals(0, VectorUtils.hammingDistance(new int[]{0, 1, 2, 3, 4}, new int[]{0, 1, 2, 3, 4}));
        assertEquals(1, VectorUtils.hammingDistance(new int[]{1, 1, 2, 3, 4}, new int[]{0, 1, 2, 3, 4}));
        assertEquals(2, VectorUtils.hammingDistance(new int[]{2, 2, 2, 3, 4}, new int[]{0, 1, 2, 3, 4}));
        assertEquals(3, VectorUtils.hammingDistance(new int[]{3, 3, 3, 3, 4}, new int[]{0, 1, 2, 3, 4}));
        assertEquals(4, VectorUtils.hammingDistance(new int[]{4, 4, 4, 4, 4}, new int[]{0, 1, 2, 3, 4}));
        assertEquals(5, VectorUtils.hammingDistance(new int[]{5, 5, 5, 5, 5}, new int[]{0, 1, 2, 3, 4}));
    }

    @Test
    public void addTest() {
        assertArrayEquals(new int[]{0, 0, 0, 0}, VectorUtils.add(new int[]{0, -1, 2, -3}, new int[]{0, 1, -2, 3}));
        assertArrayEquals(new int[]{2, -4, 10, 153}, VectorUtils.add(new int[]{1, -1, 2, 123}, new int[]{1, -3, 8, 30}));
    }

    @Test
    public void subtractTest() {
        assertArrayEquals(new int[]{0, -2, 4, 0}, VectorUtils.subtract(new int[]{0, -1, 2, 3}, new int[]{0, 1, -2, 3}));
        assertArrayEquals(new int[]{0, 2, -6, 93}, VectorUtils.subtract(new int[]{1, -1, 2, 123}, new int[]{1, -3, 8, 30}));
    }

    @Test
    public void multiplyByScalarTest() {
        assertArrayEquals(new int[]{0, -1, 2, 3}, VectorUtils.multiplyByScalar(new int[]{0, -1, 2, 3}, 1));
        assertArrayEquals(new int[]{-2, 3, -5, -7}, VectorUtils.multiplyByScalar(new int[]{1, -1, 2, 3}, -2.5));
    }

}
