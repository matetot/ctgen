package junit.core.atlbo;

import atlbo.AbsorbingWalls;
import configuration.Parameter;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ClampingRuleTest {

    @Test
    public void absorbingWallsTest() {
        List<Parameter> parameters = Arrays.asList(
                new Parameter(0, 1),
                new Parameter(1, 2),
                new Parameter(2, 3),
                new Parameter(3, 4),
                new Parameter(4, 5)
        );
        AbsorbingWalls absorbingWalls = new AbsorbingWalls();

        assertArrayEquals(new int[]{0, 0, 0, 0, 0}, absorbingWalls.clamp(new int[]{1, 2, 3, 4, 5}, parameters));
        assertArrayEquals(new int[]{0, 1, 2, 3, 4}, absorbingWalls.clamp(new int[]{-1, -2, -3, -4, -5}, parameters));
        assertArrayEquals(new int[]{0, 0, 2, 3, 4}, absorbingWalls.clamp(new int[]{0, 12, 2, -1, 4}, parameters));
    }

}
