package junit.core.interaction;

import configuration.MainConfiguration;
import configuration.Parameter;
import configuration.SubConfiguration;
import interaction.*;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class InteractionGeneratorTest {

    @Test
    public void generatedInteractionsSizeTest() {
        InteractionsGenerator interactionsGenerator = new InteractionsGeneratorImpl();

        MainConfiguration mainConfiguration = new MainConfiguration(2);
        mainConfiguration.addParameter(0, 3);
        Parameter p2 = mainConfiguration.addParameter(1, 2);
        Parameter p3 = mainConfiguration.addParameter(2, 2);
        Parameter p4 = mainConfiguration.addParameter(3, 2);

        SubConfiguration subConfiguration = new SubConfiguration(3);
        subConfiguration.addParameters(p2, p3, p4);
        mainConfiguration.addSubconfiguration(subConfiguration);

        Interactions interactions = interactionsGenerator.generate(mainConfiguration);

        // p! / t! * (P - t)!
        assertEquals(7, factorial(4) / (factorial(2) * factorial(4 - 2)) + //main
                factorial(3) / (factorial(3) * factorial(4 - 3)));    // sub
        assertEquals(7, interactions.remainingInteractions());
        assertEquals(38, interactions.remainingInteractionElements());

        assertNotNull(interactions.getInteractions().get("1100"));
        assertNotNull(interactions.getInteractions().get("1010"));
        assertNotNull(interactions.getInteractions().get("1001"));
        assertNotNull(interactions.getInteractions().get("0110"));
        assertNotNull(interactions.getInteractions().get("0101"));
        assertNotNull(interactions.getInteractions().get("0011"));
        assertNotNull(interactions.getInteractions().get("0111"));
        assertNull(interactions.getInteractions().get("0000"));
        assertNull(interactions.getInteractions().get("1111"));

        assertEquals(6, interactions.getInteractions().get("1100").size());
        assertEquals(6, interactions.getInteractions().get("1010").size());
        assertEquals(6, interactions.getInteractions().get("1001").size());
        assertEquals(4, interactions.getInteractions().get("0110").size());
        assertEquals(4, interactions.getInteractions().get("0101").size());
        assertEquals(4, interactions.getInteractions().get("0011").size());
        assertEquals(8, interactions.getInteractions().get("0111").size());
    }

    @Test
    public void createInteractionElementsTest() {
        InteractionsGenerator interactionsGenerator = new InteractionsGeneratorImpl();
        List<Parameter> parameters = Arrays.asList(new Parameter(0, 3), new Parameter(1, 2),
                                                   new Parameter(2, 2), new Parameter(3, 2));

        List<InteractionElement> interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "1100", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(2),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(2),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));

        interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "1010", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(2),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(2),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));

        interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "1001", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(2),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        new InteractionElementAtom(2),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1)
                }
        ));

        interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "0110", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom()
                }
        ));

        interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "0101", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1)
                }
        ));

        interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "0011", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1)
                }
        ));

        interactionElements = invokeCreateInteractionElemnts(interactionsGenerator, "0111", parameters);
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(0)
                }
        ));
        interactionElements.contains(new InteractionElement(
                new InteractionElementAtom[]{
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1)
                }
        ));
    }

    private List<InteractionElement> invokeCreateInteractionElemnts(InteractionsGenerator interactionsGenerator, String interaction, List<Parameter> parameters) {
        try {
            Method m = InteractionsGeneratorImpl.class.getDeclaredMethod("createInteractionElements", String.class, List.class);
            m.setAccessible(true);

            @SuppressWarnings("unchecked")
            List<InteractionElement> interactionElements = (List<InteractionElement>) m.invoke(interactionsGenerator, interaction, parameters);
            return interactionElements;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void interactionMatchesParameters() {
        try {
            Method m = InteractionsGeneratorImpl.class.getDeclaredMethod("interactionMatchesParameters", String.class, List.class, int.class);
            m.setAccessible(true);

            InteractionsGenerator interactionsGenerator = new InteractionsGeneratorImpl();
            assertTrue((boolean) m.invoke(interactionsGenerator,
                    "01010000100",
                    Arrays.asList(new Parameter(1, 10), new Parameter(3, 10),
                            new Parameter(8, 10), new Parameter(9, 10)),
                    3));
            assertTrue((boolean) m.invoke(interactionsGenerator,
                    "00010011100",
                    Arrays.asList(new Parameter(3, 10), new Parameter(6, 10),
                            new Parameter(7, 10), new Parameter(8, 10)),
                    4));
            assertTrue((boolean) m.invoke(interactionsGenerator,
                    "01010011100",
                    Arrays.asList(new Parameter(1, 10), new Parameter(3, 10),
                            new Parameter(6, 10), new Parameter(7, 10),
                            new Parameter(8, 10), new Parameter(0, 10)),
                    5));


            assertFalse((boolean) m.invoke(interactionsGenerator,
                    "01010000100",
                    Arrays.asList(new Parameter(0, 10), new Parameter(2, 10),
                            new Parameter(4, 10), new Parameter(5, 10),
                            new Parameter(6, 10), new Parameter(10, 10)),
                    3));

            assertFalse((boolean) m.invoke(interactionsGenerator,
                    "00010011100",
                    Arrays.asList(new Parameter(0, 10), new Parameter(3, 10),
                            new Parameter(6, 10), new Parameter(7, 10),
                            new Parameter(9, 10)),
                    4));
            assertFalse((boolean) m.invoke(interactionsGenerator,
                    "01010011100",
                    Arrays.asList(new Parameter(1, 10), new Parameter(2, 10),
                            new Parameter(3, 10), new Parameter(4, 10),
                            new Parameter(6, 10)),
                    5));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private long factorial(long number) {
        if (number <= 1) return 1;
        else return number * factorial(number - 1);
    }

}
