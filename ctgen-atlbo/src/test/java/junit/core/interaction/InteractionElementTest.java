package junit.core.interaction;

import interaction.InteractionElement;
import interaction.InteractionElementAtom;
import org.junit.jupiter.api.Test;
import population.PopulationMember;

import static org.junit.jupiter.api.Assertions.*;

public class InteractionElementTest {

    @Test
    public void interactionElementCloneTest() {
        InteractionElement interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });

        InteractionElement interactionElement2 = new InteractionElement(interactionElement);

        assertEquals(interactionElement, interactionElement2);
    }

    @Test
    public void isCoveredByTest() {
        InteractionElement interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });

        assertTrue(interactionElement.isCoveredBy(new int[]{0, 1, 4, 3}));
        assertTrue(interactionElement.isCoveredBy(new int[]{0, 1, 0, 3}));

        assertFalse(interactionElement.isCoveredBy(new int[]{1, 1, 1, 3}));
        assertFalse(interactionElement.isCoveredBy(new int[]{0, 2, 2, 3}));
        assertFalse(interactionElement.isCoveredBy(new int[]{0, 1, 2, 4}));

        assertTrue(interactionElement.isCoveredBy(new PopulationMember(new int[]{0, 1, 4, 3})));
        assertTrue(interactionElement.isCoveredBy(new PopulationMember(new int[]{0, 1, 0, 3})));

        assertFalse(interactionElement.isCoveredBy(new PopulationMember(new int[]{1, 1, 1, 3})));
        assertFalse(interactionElement.isCoveredBy(new PopulationMember(new int[]{0, 2, 2, 3})));
        assertFalse(interactionElement.isCoveredBy(new PopulationMember(new int[]{0, 1, 2, 4})));


        interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom(),
                        InteractionElementAtom.notInteractingAtom()
                });
        assertTrue(interactionElement.isCoveredBy(new int[]{0, 1, 1, 0}));
        assertTrue(interactionElement.isCoveredBy(new int[]{1, 2, 0, 1}));
        assertTrue(interactionElement.isCoveredBy(new int[]{2, 0, 4, 5}));
        assertTrue(interactionElement.isCoveredBy(new int[]{3, 4, 8, 9}));
    }

}
