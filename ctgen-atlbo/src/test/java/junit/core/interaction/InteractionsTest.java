package junit.core.interaction;

import interaction.InteractionElement;
import interaction.InteractionElementAtom;
import interaction.Interactions;
import org.junit.jupiter.api.Test;
import population.PopulationMember;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class InteractionsTest {

    @Test
    public void calculateFitnessTest() {
        Interactions interactions = new Interactions.Builder().build();

        Map<String, List<InteractionElement>> interactionsMap = interactions.getInteractions();

        List<InteractionElement> interactionElements1 = new ArrayList<>();
        InteractionElement interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement);

        interactionsMap.put("1101", interactionElements1);
        assertEquals(1, interactions.calculateFitness(new int[]{0, 1, 5, 3}));

        InteractionElement interactionElement2 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement2);

        interactionsMap.put("1101", interactionElements1);
        assertEquals(1, interactions.calculateFitness(new int[]{0, 1, 5, 3}));

        List<InteractionElement> interactionElements2 = new ArrayList<>();
        InteractionElement interactionElement3 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(3)
                });
        interactionElements2.add(interactionElement3);

        interactionsMap.put("1011", interactionElements2);
        assertEquals(2, interactions.calculateFitness(new int[]{1, 1, 1, 3}));
    }

    @Test
    public void removeCoveredInteractionsTest() {
        Interactions interactions = new Interactions.Builder().build();

        Map<String, List<InteractionElement>> interactionsMap = interactions.getInteractions();

        List<InteractionElement> interactionElements1 = new ArrayList<>();
        InteractionElement interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement);

        interactionsMap.put("1101", interactionElements1);

        InteractionElement interactionElement2 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement2);

        interactionsMap.put("1101", interactionElements1);

        List<InteractionElement> interactionElements2 = new ArrayList<>();
        InteractionElement interactionElement3 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(3)
                });
        interactionElements2.add(interactionElement3);

        interactionsMap.put("1011", interactionElements2);
        assertEquals(3, interactions.remainingInteractionElements());
        assertEquals(2, interactions.remainingInteractions());

        interactions.removeCoveredInteraction(new PopulationMember(new int[]{0, 1, 4, 3}));
        assertEquals(2, interactions.remainingInteractions());
        assertEquals(2, interactions.remainingInteractionElements());

        interactions.removeCoveredInteraction(new PopulationMember(new int[]{1, 1, 1, 3}));
        assertEquals(0, interactions.remainingInteractions());
        assertEquals(0, interactions.remainingInteractionElements());
    }

    @Test
    public void remainingInteractionElementsTest()  {
        Interactions interactions = new Interactions.Builder().build();

        Map<String, List<InteractionElement>> interactionsMap = interactions.getInteractions();

        List<InteractionElement> interactionElements1 = new ArrayList<>();
        InteractionElement interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement);

        interactionsMap.put("1101", interactionElements1);
        assertEquals(1, interactions.remainingInteractionElements());

        InteractionElement interactionElement2 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement2);

        interactionsMap.put("1101", interactionElements1);
        assertEquals(2, interactions.remainingInteractionElements());

        List<InteractionElement> interactionElements2 = new ArrayList<>();
        InteractionElement interactionElement3 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(3)
                });
        interactionElements2.add(interactionElement3);

        interactionsMap.put("1011", interactionElements2);
        assertEquals(3, interactions.remainingInteractionElements());
    }

    @Test
    public void remainingInteractionsTest() {
        Interactions interactions = new Interactions.Builder().build();

        Map<String, List<InteractionElement>> interactionsMap = interactions.getInteractions();

        List<InteractionElement> interactionElements1 = new ArrayList<>();
        InteractionElement interactionElement = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(0),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement);

        interactionsMap.put("1101", interactionElements1);
        assertEquals(1, interactions.remainingInteractions());

        InteractionElement interactionElement2 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(3)
                });
        interactionElements1.add(interactionElement2);

        interactionsMap.put("1101", interactionElements1);
        assertEquals(1, interactions.remainingInteractions());

        List<InteractionElement> interactionElements2 = new ArrayList<>();
        InteractionElement interactionElement3 = new InteractionElement(new InteractionElementAtom[]
                {
                        new InteractionElementAtom(1),
                        InteractionElementAtom.notInteractingAtom(),
                        new InteractionElementAtom(1),
                        new InteractionElementAtom(3)
                });
        interactionElements2.add(interactionElement3);

        interactionsMap.put("1011", interactionElements2);
        assertEquals(2, interactions.remainingInteractions());
    }

}
