package junit.core.fuzzy;

import fuzzy.FuzzySystem;
import fuzzy.FuzzySystemImpl;
import fuzzy.functions.*;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import population.Population;
import population.PopulationMember;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FuzzySystemTest {

    @Test
    public void rule1Test() {
        InputStream fileStream = FuzzySystemImpl.class.getClassLoader().getResourceAsStream("fuzzy-default.fcl");
        FIS fis = FIS.load(fileStream, true);

        FunctionBlock functionBlock = fis.getFunctionBlock("fuzzy_atlbo");

        for (double Qm = 0.0; Qm <= 60.0; Qm += 0.5) {
            double Im = RandomUtils.nextDouble(0.0, 100.0);
            double Dm = RandomUtils.nextDouble(0.0, 100.0);

            fis.setVariable("quality", Qm);
            fis.setVariable("intensification", Im);
            fis.setVariable("diversification", Dm);

            fis.evaluate();
            double selection = functionBlock.getVariable("selection").getLatestDefuzzifiedValue();

            System.out.println("Qm=" + Qm + ";Im=" + Im + ";Dm=" + Dm);
            System.out.println("Selection=" + selection);

            assertTrue(selection > 50.0);
        }
    }

    @Test
    @Disabled
    public void showCharts() {
        FuzzySystem fuzzySystem = new FuzzySystemImpl();

        try {
            Field field = fuzzySystem.getClass().getDeclaredField("functionBlock");
            field.setAccessible(true);
            FunctionBlock functionBlock = (FunctionBlock) field.get(fuzzySystem);
            JFuzzyChart.get().chart(functionBlock);
            System.in.read();
        } catch (NoSuchFieldException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void qualityMeasureTest() {
        QualityMeasure qualityMeasure = new QualityMeasureImpl();
        double res = qualityMeasure.calculate(10, 0, 5);

        assertEquals(50.0, res);
    }

    @Test
    public void intensificationMeasureTest() {
        Population population = new Population(3, 5, PopulationMember.class);
        population.addMember(new int[]{0, 1, 0, 2, 1});
        population.addMember(new int[]{1, 1, 1, 0, 1});
        population.addMember(new int[]{1, 1, 1, 2, 2});

        population.getAt(2).update(new int[]{1, 1, 1, 2, 2}, 10);
        PopulationMember current = population.getAt(1);

        IntensificationMeasure intensificationMeasure = new IntensificationMeasureImpl();
        double res = intensificationMeasure.calculate(current, population);

        assertEquals(40.0, res);
    }

    @Test
    public void diversificationMeasureTest() {
        Population population = new Population(3, 5, PopulationMember.class);
        population.addMember(new int[]{0, 1, 0, 2, 1});
        population.addMember(new int[]{1, 1, 1, 0, 1});
        population.addMember(new int[]{1, 1, 1, 2, 2});

        population.getAt(2).update(new int[]{1, 1, 1, 2, 2}, 10);
        PopulationMember current = population.getAt(0);

        DiversificationMeasure diversificationMeasure = new DiversificationMeasureImpl();
        double res = diversificationMeasure.calculate(current, population);

        assertEquals(60.0, res);

    }

}
