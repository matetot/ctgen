package junit.core.population;

import configuration.Parameter;
import org.junit.jupiter.api.Test;
import population.Population;
import population.PopulationGenerator;
import population.PopulationGeneratorImpl;
import population.PopulationMember;
import testutils.ParameterProvider;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PopulationGeneratorTest {

    @Test
    public void populationSizeTest() {
        PopulationGenerator populationGenerator = new PopulationGeneratorImpl();

        int populationSize = 15;
        Population population = populationGenerator.spawn(new ArrayList<>(), populationSize);

        assertEquals(populationSize, population.getPopulationSize());
        assertEquals(populationSize, population.getPopulation().size());
    }

    @Test
    public void populationMemberValuesTest() {
        PopulationGenerator populationGenerator = new PopulationGeneratorImpl();

        int populationSize = 15;
        List<Parameter> parameters = ParameterProvider.createParamters(5, 2, 1, 8, 9);
        Population population = populationGenerator.spawn(parameters, populationSize);

        for (int i = 0; i < population.getPopulationSize(); i++) {
            PopulationMember populationMember = population.getAt(i);

            for (int j = 0; j < population.getPopulationMemberSize(); j++) {
                assertFalse(populationMember.getSolution()[j] >= parameters.get(j).getValues());
            }
        }
    }

}
