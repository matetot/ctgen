package junit.core.population;

import org.junit.jupiter.api.Test;
import population.PopulationMember;
import population.StatisticalPopulationMember;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class PopulationMemberTest {

    @Test
    public void populationMemberCloneTest() {
        PopulationMember populationMember1 = new PopulationMember(new int[]{1, 2, 3, 4}, 10);
        PopulationMember populationMember2 = new PopulationMember(populationMember1);

        assertArrayEquals(populationMember1.getSolution(), populationMember2.getSolution());
        assertEquals(populationMember1.getFitness(), populationMember2.getFitness());

        populationMember1.update(new int[]{7, 8, 9, 10}, 15);

        assertFalse(Arrays.equals(populationMember1.getSolution(), populationMember2.getSolution()));
        assertNotEquals(populationMember1.getFitness(), populationMember2.getFitness());
    }

    @Test
    public void statisticalPopulationMemberAncestorsTest() {
        StatisticalPopulationMember populationMember1 = new StatisticalPopulationMember(new int[]{1, 2, 3, 4}, 10);
        assertEquals(0, populationMember1.getAncestors().size());

        populationMember1.update(new int[]{6, 7, 8, 9}, 14);
        assertEquals(1, populationMember1.getAncestors().size());
        assertEquals(10, populationMember1.getAncestors().get(0).getFitness());
        assertTrue(Arrays.equals(new int[]{1, 2, 3, 4}, populationMember1.getAncestors().get(0).getSolution()));

        populationMember1.update(new int[]{0, 0, 0, 0}, 0);
        assertEquals(2, populationMember1.getAncestors().size());
        assertEquals(14, populationMember1.getAncestors().get(1).getFitness());
        assertTrue(Arrays.equals(new int[]{6, 7, 8, 9}, populationMember1.getAncestors().get(1).getSolution()));

        populationMember1.update(new int[]{2, 8, 8, 3}, 14);
        assertEquals(3, populationMember1.getAncestors().size());
        assertEquals(0, populationMember1.getAncestors().get(2).getFitness());
        assertTrue(Arrays.equals(new int[]{0, 0, 0, 0}, populationMember1.getAncestors().get(2).getSolution()));
    }

}
