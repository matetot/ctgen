package junit.core.population;

import org.junit.jupiter.api.Test;
import population.ArraysLengthsDoNotMatch;
import population.Population;
import population.PopulationMember;
import population.StatisticalPopulationMember;

import static org.junit.jupiter.api.Assertions.*;

public class PopulationTest {

    @Test
    public void populationMemberAddingTest() {
        Population population = new Population(10, 4, PopulationMember.class);

        population.addMember(new int[]{0, 1, 2, 3});
        assertArrayEquals(new int[]{0, 1, 2, 3}, population.getAt(0).getSolution());
        assertEquals(PopulationMember.class, population.getAt(0).getClass());

        population.addMember(new int[]{4, 5, 6, 7});
        assertArrayEquals(new int[]{4, 5, 6, 7}, population.getAt(1).getSolution());
        assertEquals(PopulationMember.class, population.getAt(1).getClass());

        assertThrows(IndexOutOfBoundsException.class, () -> population.getAt(3));
    }

    @Test
    public void statisticalPopulationMemberAddingTest() {
        Population population = new Population(10, 4, StatisticalPopulationMember.class);

        population.addMember(new int[]{0, 1, 2, 3});
        assertArrayEquals(new int[]{0, 1, 2, 3}, population.getAt(0).getSolution());
        assertEquals(StatisticalPopulationMember.class, population.getAt(0).getClass());

        population.addMember(new int[]{4, 5, 6, 7});
        assertArrayEquals(new int[]{4, 5, 6, 7}, population.getAt(1).getSolution());
        assertEquals(StatisticalPopulationMember.class, population.getAt(1).getClass());

        assertThrows(IndexOutOfBoundsException.class, () -> population.getAt(3));
    }

    @Test
    public void populationSizeTest() {
        Population population = new Population(1, 2, PopulationMember.class);
        population.addMember(new int[]{0, 1});

        assertThrows(IllegalStateException.class, () -> population.addMember(new int[]{2, 3}));
    }

    @Test
    public void populationMemberSizeTest() {
        Population population = new Population(1, 2, PopulationMember.class);

        assertThrows(ArraysLengthsDoNotMatch.class, () -> population.addMember(new int[]{0}));
        assertThrows(ArraysLengthsDoNotMatch.class, () -> population.addMember(new int[]{0, 1, 2}));
    }

    @Test
    public void populationCloneTest() {
        Population population = new Population(5, 3, PopulationMember.class);

        population.addMember(new int[]{0, 1, 2});
        population.addMember(new int[]{1, 10, 12});
        population.addMember(new int[]{20, 21, 32});
        population.addMember(new int[]{70, 81, 92});
        population.addMember(new int[]{10, 13, 22});

        Population copy = new Population(population);
        assertEquals(population.getPopulationSize(), copy.getPopulationSize());
        assertEquals(population.getPopulationMemberSize(), copy.getPopulationMemberSize());
        assertEquals(population.getPopulationMemberType(), copy.getPopulationMemberType());

        assertEquals(5, population.getPopulation().size());
        assertEquals(population.getPopulation().size(), copy.getPopulation().size());

        for (int i = 0; i < population.getPopulation().size(); i++) {
            assertArrayEquals(population.getAt(i).getSolution(), copy.getAt(i).getSolution());
        }
    }

    @Test
    public void getBestTest() {
        Population population = new Population(5, 3, PopulationMember.class);
        population.addMember(new int[]{6, 7, 8});
        population.addMember(new int[]{0, 1, 2});
        population.addMember(new int[]{12, 13, 14});
        population.addMember(new int[]{3, 4, 5});
        population.addMember(new int[]{9, 10, 11});

        population.getAt(0).update(new int[]{0, 1, 2}, 1);
        population.getAt(1).update(new int[]{3, 4, 5}, 2);
        population.getAt(2).update(new int[]{6, 7, 8}, 3);
        population.getAt(3).update(new int[]{9, 10, 11}, 4);
        population.getAt(4).update(new int[]{12, 13, 14}, 5);

        assertArrayEquals(new int[]{12, 13, 14}, population.getBest().getSolution());
    }

    @Test
    public void getWorstTest() {
        Population population = new Population(5, 3, PopulationMember.class);
        population.addMember(new int[]{0, 1, 2});
        population.addMember(new int[]{3, 4, 5});
        population.addMember(new int[]{6, 7, 8});
        population.addMember(new int[]{9, 10, 11});
        population.addMember(new int[]{12, 13, 14});

        population.getAt(0).update(1);
        population.getAt(1).update(2);
        population.getAt(2).update(3);
        population.getAt(3).update(4);
        population.getAt(4).update(5);

        assertArrayEquals(new int[]{0, 1, 2}, population.getWorst().getSolution());
    }

    @Test
    public void getMeanTest() {
        Population population = new Population(5, 3, PopulationMember.class);
        population.addMember(new int[]{0, 1, 2});
        population.addMember(new int[]{3, 4, 5});
        population.addMember(new int[]{6, 7, 8});
        population.addMember(new int[]{9, 9, 9});
        population.addMember(new int[]{13, 13, 14});

        assertArrayEquals(new int[]{6, 7, 8}, population.getMean());
    }

}
